<?php

namespace App\Repository;

use App\Entity\OperationReportCache;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OperationReportCache>
 *
 * @method OperationReportCache|null find($id, $lockMode = null, $lockVersion = null)
 * @method OperationReportCache|null findOneBy(array $criteria, array $orderBy = null)
 * @method OperationReportCache[]    findAll()
 * @method OperationReportCache[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OperationReportCacheRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OperationReportCache::class);
    }

//    /**
//     * @return OperationReportCache[] Returns an array of OperationReportCache objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('o')
//            ->andWhere('o.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('o.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?OperationReportCache
//    {
//        return $this->createQueryBuilder('o')
//            ->andWhere('o.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
