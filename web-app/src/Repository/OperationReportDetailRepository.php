<?php

namespace App\Repository;

use App\Entity\OperationReportDetail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OperationReportDetail>
 *
 * @method OperationReportDetail|null find($id, $lockMode = null, $lockVersion = null)
 * @method OperationReportDetail|null findOneBy(array $criteria, array $orderBy = null)
 * @method OperationReportDetail[]    findAll()
 * @method OperationReportDetail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OperationReportDetailRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OperationReportDetail::class);
    }

//    /**
//     * @return OperationReportDetail[] Returns an array of OperationReportDetail objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('o')
//            ->andWhere('o.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('o.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?OperationReportDetail
//    {
//        return $this->createQueryBuilder('o')
//            ->andWhere('o.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
