<?php

namespace App\Repository;

use App\Entity\Member;
use App\Service\DataTableService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

/**
 * @extends ServiceEntityRepository<Member>
 *
 * @method Member|null find($id, $lockMode = null, $lockVersion = null)
 * @method Member|null findOneBy(array $criteria, array $orderBy = null)
 * @method Member[]    findAll()
 * @method Member[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MemberRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Member::class);
    }

    /**
     * Find Entries for the Prime VUE DataTable
     * @return Member[] Returns an array of General objects
     * @throws \JsonException
     * @throws QueryException
     */
    public function findByDataTableService(Request $requestUri, DataTableService $dataTableService, array $params, bool $showAllEntries = false): array
    {
        $uri = urldecode($requestUri->getRequestUri());
        $tableShort = "m";
        $joinTableOne = "fS";
        $joinTableTwo = "d";
        $joinTableThree = "dMF";
        $globalFilter = ["$tableShort.surname", "$tableShort.firstname",
            "$joinTableOne.abbreviation", "$joinTableOne.description",
            "$joinTableTwo.abbreviation", "$joinTableTwo.description",
            "$joinTableThree.abbreviation", "$joinTableThree.description",];

        // wurden Parameter mitgegeben?
        if ($uri && str_contains($uri, '?') && !str_contains($uri, '?{}')) {
            $service = $dataTableService;

            $service->explodeParams($uri, $globalFilter);

            // Begrenzung der Ausgabe
            $sql = $this->createQueryBuilder($tableShort)
                ->leftJoin("$tableShort.federalState", $joinTableOne)
                ->leftJoin("$tableShort.department", $joinTableTwo)
                ->leftJoin("$tableShort.defaultMemberFunction", $joinTableThree)
                ->addCriteria($service->generateFilterShowAllEntries($showAllEntries))
                ->addCriteria($service->generateFilterShowOnlyMyDepartmentEntries($params['department'], !$params['isFullAdmin']))
                ->addCriteria($service->generateFilter());

            // Zusammenfügen der Order By Anweisung
            if (count($service->orderBy) > 0) {
                foreach ($service->orderBy as $order) {
                    $sql->addCriteria($order);
                }
            }

//            dd($sql
//                ->setFirstResult($service->offset)
//                ->setMaxResults($service->limit)
//                ->getQuery());

            $filterItems = $sql
                ->setFirstResult($service->offset)
                ->setMaxResults($service->limit)
                ->getQuery()
                ->getResult();

            // Ohne Begrenzung (für die Paginate)
            $maxItems = $this->createQueryBuilder($tableShort)
                ->leftJoin("$tableShort.federalState", $joinTableOne)
                ->leftJoin("$tableShort.department", $joinTableTwo)
                ->leftJoin("$tableShort.defaultMemberFunction", $joinTableThree)
                ->addCriteria($service->generateFilterShowAllEntries($showAllEntries))
                ->addCriteria($service->generateFilterShowOnlyMyDepartmentEntries($params['department'], !$params['isFullAdmin']))
                ->addCriteria($service->generateFilter())
                ->getQuery()
                ->getResult();

            return [
                "items" => $filterItems,
                "filterCount" => count($filterItems),
                "totalCount" => count($maxItems)
            ];
        }

        // Ohne Filter und Begrenzung
        $allItems = $this->createQueryBuilder($tableShort)
            ->leftJoin("$tableShort.federalState", $joinTableOne)
            ->leftJoin("$tableShort.department", $joinTableTwo)
            ->leftJoin("$tableShort.defaultMemberFunction", $joinTableThree)
            ->orderBy("$joinTableOne.abbreviation", Criteria::ASC)
            ->addOrderBy("$joinTableTwo.abbreviation", Criteria::ASC)
            ->addOrderBy("$tableShort.surname", Criteria::ASC)
            ->getQuery()
            ->getResult();

        return [
            "items" => $allItems,
            "filterCount" => count($allItems),
            "totalCount" => count($allItems)
        ];
    }
}
