<?php

namespace App\Repository;

use App\Entity\IncomingAlert;
use App\Service\DataTableService;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\QueryException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

/**
 * @extends ServiceEntityRepository<IncomingAlert>
 *
 * @method IncomingAlert|null find($id, $lockMode = null, $lockVersion = null)
 * @method IncomingAlert|null findOneBy(array $criteria, array $orderBy = null)
 * @method IncomingAlert[]    findAll()
 * @method IncomingAlert[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IncomingAlertRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IncomingAlert::class);
    }

    /**
     * Find Entries for the Prime VUE DataTable
     * @return IncomingAlert[] Returns an array of General objects
     * @throws QueryException
     * @throws \JsonException
     */
    public function findByDataTableService(Request $requestUri, DataTableService $dataTableService, array $params, bool $showAllEntries = false): array
    {
        $uri = urldecode($requestUri->getRequestUri());
        $tableShort = "iA";
        $joinTableOne = "fS";
        $joinTableTwo = "d";
        $joinTableThree = "aA";
        $globalFilter = [
            "$tableShort.text", "$tableShort.comment", "$tableShort.numberControlCenter", "$tableShort.keywordShort", "$tableShort.keywordLong", "$tableShort.location",
            "$tableShort.personPatient", "$tableShort.otherAddressInformation", "$tableShort.text", "$tableShort.messageComplete", "$tableShort.comment",
            "$joinTableOne.description", "$joinTableOne.abbreviation",
            "$joinTableTwo.description", "$joinTableTwo.abbreviation",
            "$joinTableThree.address", "$joinTableTwo.description",
        ];

        // wurden Parameter mitgegeben?
        if ($uri && str_contains($uri, '?') && !str_contains($uri, '?{}')) {
            $service = $dataTableService;

            $service->explodeParams($uri, $globalFilter, true);

            // Begrenzung der Ausgabe
            $sql = $this->createQueryBuilder($tableShort)
                ->leftJoin("$tableShort.federalState", $joinTableOne)
                ->leftJoin("$tableShort.department", $joinTableTwo)
                ->leftJoin("$tableShort.address", $joinTableThree)
                ->addCriteria($service->generateFilterShowAllEntries($showAllEntries))
                ->addCriteria($service->generateFilterShowOnlyMyDepartmentEntries($params['department'], !$params['isFullAdmin']))
                ->addCriteria($service->generateFilter());

            // Zusammenfügen der Order By Anweisung
            if (count($service->orderBy) > 0) {
                foreach ($service->orderBy as $order) {
                    $sql->addCriteria($order);
                }
            }

            $filterItems = $sql
                ->setFirstResult($service->offset)
                ->setMaxResults($service->limit)
                ->getQuery()
                ->getResult();

            // Ohne Begrenzung (für die Paginate)
            $maxItems = $this->createQueryBuilder($tableShort)
                ->leftJoin("$tableShort.federalState", $joinTableOne)
                ->leftJoin("$tableShort.department", $joinTableTwo)
                ->leftJoin("$tableShort.address", $joinTableThree)
                ->addCriteria($service->generateFilterShowAllEntries($showAllEntries))
                ->addCriteria($service->generateFilterShowOnlyMyDepartmentEntries($params['department'], !$params['isFullAdmin']))
                ->addCriteria($service->generateFilter())
                ->getQuery()
                ->getResult();

            return [
                "items" => $filterItems,
                "filterCount" => count($filterItems),
                "totalCount" => count($maxItems)
            ];
        }

        // Ohne Filter und Begrenzung
        $allItems = $this->createQueryBuilder($tableShort)
            ->leftJoin("$tableShort.federalState", $joinTableOne)
            ->leftJoin("$tableShort.department", $joinTableTwo)
            ->leftJoin("$tableShort.address", $joinTableThree)
            ->orderBy("$tableShort.incomingDateTime", Criteria::DESC)
            ->getQuery()
            ->getResult();

        return [
            "items" => $allItems,
            "filterCount" => count($allItems),
            "totalCount" => count($allItems)
        ];
    }

    /**
     * Find Entries for the Prime VUE DataTable
     * @return IncomingAlert[] Returns an array of General objects
     * @throws QueryException
     * @throws \JsonException
     */
    public function findByDataTableServiceUserRics(Request $requestUri, DataTableService $dataTableService, array|null $userRics, bool $showAllEntries = false, bool $readForOperationReport = false): array
    {
        $uri = urldecode($requestUri->getRequestUri());
        $tableShort = "iA";
        $joinTableOne = "fS";
        $joinTableTwo = "d";
        $joinTableThree = "aA";
        $globalFilter = [
            "$tableShort.text", "$tableShort.comment", "$tableShort.numberControlCenter", "$tableShort.keywordShort", "$tableShort.keywordLong", "$tableShort.location",
            "$tableShort.personPatient", "$tableShort.otherAddressInformation", "$tableShort.text", "$tableShort.messageComplete", "$tableShort.comment",
            "$joinTableOne.description", "$joinTableOne.abbreviation",
            "$joinTableTwo.description", "$joinTableTwo.abbreviation",
            "$joinTableThree.address", "$joinTableTwo.description",
        ];
        $myRics = [];

        if ($userRics) {
            foreach ($userRics as $userRic) {
                $myRics[] = preg_replace('/[^0-9]/', '', $userRic);
            }
        }

        $criteria = new Criteria();
        $expressionBuilder = Criteria::expr();

        $criteria->where($expressionBuilder->in("$joinTableThree.address", $myRics));

        if ($readForOperationReport) {
            $criteria->andWhere($expressionBuilder->startsWith("$tableShort.isVisibilityOperationReport", 1));
        }

        // wurden Parameter mitgegeben?
        if ($uri && str_contains($uri, '?') && !str_contains($uri, '?{}')) {
            $service = $dataTableService;

            $service->explodeParams($uri, $globalFilter, true);

            // Begrenzung der Ausgabe
            $sql = $this->createQueryBuilder($tableShort)
                ->leftJoin("$tableShort.federalState", $joinTableOne)
                ->leftJoin("$tableShort.department", $joinTableTwo)
                ->leftJoin("$tableShort.address", $joinTableThree)
                ->addCriteria($service->generateFilterShowAllEntries($showAllEntries))
                ->addCriteria($service->generateFilter())
                ->addCriteria($criteria);

            // Zusammenfügen der Order By Anweisung
            if (count($service->orderBy) > 0) {
                foreach ($service->orderBy as $order) {
                    $sql->addCriteria($order);
                }
            }

            $filterItems = $sql
                ->setFirstResult($service->offset)
                ->setMaxResults($service->limit)
                ->getQuery()
                ->getResult();

            // Ohne Begrenzung (für die Paginate)
            $maxItems = $this->createQueryBuilder($tableShort)
                ->leftJoin("$tableShort.federalState", $joinTableOne)
                ->leftJoin("$tableShort.department", $joinTableTwo)
                ->leftJoin("$tableShort.address", $joinTableThree)
                ->addCriteria($service->generateFilterShowAllEntries($showAllEntries))
                ->addCriteria($service->generateFilter())
                ->addCriteria($criteria)
                ->getQuery()
                ->getResult();

            return [
                "items" => $filterItems,
                "filterCount" => count($filterItems),
                "totalCount" => count($maxItems)
            ];
        }

        // Ohne Filter und Begrenzung
        $allItems = $this->createQueryBuilder($tableShort)
            ->leftJoin("$tableShort.federalState", $joinTableOne)
            ->leftJoin("$tableShort.department", $joinTableTwo)
            ->leftJoin("$tableShort.address", $joinTableThree)
            ->orderBy("$tableShort.incomingDateTime", Criteria::DESC)
            ->addCriteria($criteria)
            ->getQuery()
            ->getResult();

        return [
            "items" => $allItems,
            "filterCount" => count($allItems),
            "totalCount" => count($allItems)
        ];
    }

    /**
     * Find Entries for the Evaluation Dashboard,
     * Year and all Months
     *
     * @return IncomingAlert[]
     */
    public function findByYearAndMonthUserRics(int $requestYear, int $requestMonth, array|null $userRics, bool $showAllEntries = false): array
    {
        $month = sprintf("%'.02d", $requestMonth);
        $day = new DateTime("$requestYear-$month");
        $monthDays = $day->format('t');
//        $address = $para['address'];
//         Filter Datum (1. bis zum letzten Tag)
        $start = "$requestYear-$month-01 00:00:00";
        $end = "$requestYear-$month-$monthDays 23:59:00";

        $myRics = [];
        // hat der Nutzer 'Abonnierte Adressen' ?
        if ($userRics) {
            foreach ($userRics as $userRic) {
                $myRics[] = preg_replace('/[^0-9]/', '', $userRic);
            }
        }

        $criteria = new Criteria();
        $expressionBuilder = Criteria::expr();

        $criteria->where($expressionBuilder->in("aA.address", $myRics));

        return $this->createQueryBuilder('iA')
            ->leftJoin('iA.address', 'aA')
            ->addCriteria($criteria)
            ->andWhere("(iA.incomingDateTime > '$start' and iA.incomingDateTime < '$end')")
            ->getQuery()
            ->getResult();
    }

    /**
     * Prepare for the Import Function
     *
     * @throws QueryException
     */
    public function findForTheImport(array $params): array
    {
        $criteria = new Criteria();
        $expressionBuilder = Criteria::expr();

        $criteria->where($expressionBuilder->eq("iA.address", $params['address']));
        $criteria->andWhere($expressionBuilder->eq("iA.incomingDateTime", $params['incomingDateTime']));
        $criteria->andWhere($expressionBuilder->startsWith("iA.isVisibilityOperationReport", "1"));
        $criteria->orWhere($expressionBuilder->contains("iA.messageComplete", $params['messageComplete']));

        return $this->createQueryBuilder('iA')
            ->addCriteria($criteria)
            ->getQuery()
            ->getResult();

    }
}
