<?php

namespace App\Repository;

use App\Entity\AlertAddress;
use App\Service\DataTableService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\QueryException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

/**
 * @extends ServiceEntityRepository<AlertAddress>
 *
 * @method AlertAddress|null find($id, $lockMode = null, $lockVersion = null)
 * @method AlertAddress|null findOneBy(array $criteria, array $orderBy = null)
 * @method AlertAddress[]    findAll()
 * @method AlertAddress[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AlertAddressRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AlertAddress::class);
    }

    /**
     * Find Entries for the Prime VUE DataTable
     * @return AlertAddress[] Returns an array of General objects
     * @throws QueryException
     * @throws \JsonException
     */
    public function findByDataTableService(Request $requestUri, DataTableService $dataTableService, array $params, bool $showAllEntries = false): array
    {
        $uri = urldecode($requestUri->getRequestUri());
        $tableShort = "v";
        $joinTableOne = "fS";
        $joinTableTwo = "d";
        $globalFilter = [
            "$tableShort.address", "$tableShort.description",
            "$joinTableOne.description", "$joinTableOne.abbreviation",
            "$joinTableTwo.description", "$joinTableTwo.abbreviation",
        ];

        // wurden Parameter mitgegeben?
        if ($uri && str_contains($uri, '?') && !str_contains($uri, '?{}')) {
            $service = $dataTableService;

            $service->explodeParams($uri, $globalFilter, true);

            // Begrenzung der Ausgabe
            $sql = $this->createQueryBuilder($tableShort)
                ->leftJoin("$tableShort.federalState", $joinTableOne)
                ->leftJoin("$tableShort.department", $joinTableTwo)
                ->addCriteria($service->generateFilterShowAllEntries($showAllEntries))
                ->addCriteria($service->generateFilterShowOnlyMyDepartmentEntries($params['department'], !$params['isFullAdmin']))
                ->addCriteria($service->generateFilter());

            // Zusammenfügen der Order By Anweisung
            if (count($service->orderBy) > 0) {
                foreach ($service->orderBy as $order) {
                    $sql->addCriteria($order);
                }
            }

            $filterItems = $sql
                ->setFirstResult($service->offset)
                ->setMaxResults($service->limit)
                ->getQuery()
                ->getResult();

            // Ohne Begrenzung (für die Paginate)
            $maxItems = $this->createQueryBuilder($tableShort)
                ->leftJoin("$tableShort.federalState", $joinTableOne)
                ->leftJoin("$tableShort.department", $joinTableTwo)
                ->addCriteria($service->generateFilterShowAllEntries($showAllEntries))
                ->addCriteria($service->generateFilterShowOnlyMyDepartmentEntries($params['department'], !$params['isFullAdmin']))
                ->addCriteria($service->generateFilter())
                ->getQuery()
                ->getResult();

            return [
                "items" => $filterItems,
                "filterCount" => count($filterItems),
                "totalCount" => count($maxItems)
            ];
        }

        // Ohne Filter und Begrenzung
        $allItems = $this->createQueryBuilder($tableShort)
            ->leftJoin("$tableShort.federalState", $joinTableOne)
            ->leftJoin("$tableShort.department", $joinTableTwo)
            ->orderBy("$joinTableOne.abbreviation", Criteria::ASC)
            ->addOrderBy("$joinTableTwo.abbreviation", Criteria::ASC)
            ->addOrderBy("$tableShort.description", Criteria::ASC)
            ->getQuery()
            ->getResult();

        return [
            "items" => $allItems,
            "filterCount" => count($allItems),
            "totalCount" => count($allItems)
        ];
    }
}
