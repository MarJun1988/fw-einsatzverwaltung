<?php

namespace App\Repository;

use App\Entity\Department;
use App\Entity\OperationReport;
use App\Service\DataTableService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\QueryException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

/**
 * @extends ServiceEntityRepository<OperationReport>
 *
 * @method OperationReport|null find($id, $lockMode = null, $lockVersion = null)
 * @method OperationReport|null findOneBy(array $criteria, array $orderBy = null)
 * @method OperationReport[]    findAll()
 * @method OperationReport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OperationReportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OperationReport::class);
    }

    /**
     * Find Entries for the Prime VUE DataTable
     * @return OperationReport[] Returns an array of General objects
     * @throws QueryException
     * @throws \JsonException
     */
    public function findByDataTableService(Request $requestUri, DataTableService $dataTableService, array $params, bool $showAllEntries = false): array
    {
        $uri = urldecode($requestUri->getRequestUri());
        $tableShort = "oReport"; // OperationReport
        $joinTableOne = "fS"; // FederalState
        $joinTableTwo = "d"; // Department
        $joinTableThree = "oRD"; // OperationReportDetail
        $joinTableFour = "iA"; // IncomingAlert
        $joinTableFive = "oT"; // OperationType

        $criteria = new Criteria();
        $expressionBuilder = Criteria::expr();

        $globalFilter = [
            "$tableShort.ownForces", "$tableShort.otherTaskForces", "$tableShort.tools", "$tableShort.commentPrint", "$tableShort.comment",
            "$joinTableOne.description", "$joinTableOne.abbreviation",
            "$joinTableTwo.description", "$joinTableTwo.abbreviation",
            "$joinTableThree.numberControlCenter", "$joinTableThree.keywordShort", "$joinTableThree.keywordLong",
            "$joinTableThree.location", "$joinTableThree.otherAddressInformation", "$joinTableThree.personPatient",
            "$joinTableThree.messageComplete", "$joinTableThree.postalCode", "$joinTableThree.streetWithNumber",
            "$joinTableThree.numberFireDepartment", "$joinTableThree.numberPolice", "$joinTableThree.streetWithNumber",
            "$joinTableFive.description", "$joinTableFive.abbreviation"
        ];

        // wurden Parameter mitgegeben?
        if ($uri && str_contains($uri, '?') && !str_contains($uri, '?{}')) {
            $service = $dataTableService;

            $service->explodeParams($uri, $globalFilter, true);

            // Begrenzung der Ausgabe
            $sql = $this->createQueryBuilder($tableShort)
                ->leftJoin("$tableShort.federalState", $joinTableOne)
                ->leftJoin("$tableShort.department", $joinTableTwo)
                ->leftJoin("$tableShort.operationReportDetail", $joinTableThree)
                ->leftJoin("$tableShort.incomingAlert", $joinTableFour)
                ->leftJoin("$joinTableThree.operationType", $joinTableFive)
                ->addCriteria($service->generateFilterShowAllEntries($showAllEntries))
                ->addCriteria($service->generateFilterShowOnlyMyDepartmentEntries($params['department'], !$params['isFullAdmin']))
                ->addCriteria($service->generateFilter());
            // Wurde ein Jahr mitgegeben
            if (in_array("year", $params, true) && $params["year"]) {
                $sql->addCriteria((new Criteria())->andWhere($expressionBuilder->gt("oRD.incomingDateTime", $params['year'] . "-01-01"))
                    ->andWhere($expressionBuilder->lt("oRD.incomingDateTime", $params['year'] . "-12-31")));
            }

            // Zusammenfügen der Order By Anweisung
            if (count($service->orderBy) > 0) {
                foreach ($service->orderBy as $order) {
                    $sql->addCriteria($order);
                }
            }

            $filterItems = $sql
                ->setFirstResult($service->offset)
                ->setMaxResults($service->limit)
                ->getQuery()
                ->getResult();

            // Ohne Begrenzung (für die Paginate)
            $maxItems = $this->createQueryBuilder($tableShort)
                ->leftJoin("$tableShort.federalState", $joinTableOne)
                ->leftJoin("$tableShort.department", $joinTableTwo)
                ->leftJoin("$tableShort.operationReportDetail", $joinTableThree)
                ->leftJoin("$tableShort.incomingAlert", $joinTableFour)
                ->leftJoin("$joinTableThree.operationType", $joinTableFive)
                ->addCriteria($service->generateFilterShowAllEntries($showAllEntries))
                ->addCriteria($service->generateFilterShowOnlyMyDepartmentEntries($params['department'], !$params['isFullAdmin']))
                ->addCriteria($service->generateFilter())
                ->getQuery()
                ->getResult();

            return [
                "items" => $filterItems,
                "filterCount" => count($filterItems),
                "totalCount" => count($maxItems)
            ];
        }

        // Ohne Filter und Begrenzung
        $allItems = $this->createQueryBuilder($tableShort)
            ->leftJoin("$tableShort.federalState", $joinTableOne)
            ->leftJoin("$tableShort.department", $joinTableTwo)
            ->leftJoin("$tableShort.operationReportDetail", $joinTableThree)
            ->leftJoin("$tableShort.incomingAlert", $joinTableFour)
            ->leftJoin("$joinTableThree.operationType", $joinTableFive)
            ->orderBy("$tableShort.createdAt", Criteria::DESC)
            ->getQuery()
            ->getResult();

        return [
            "items" => $allItems,
            "filterCount" => count($allItems),
            "totalCount" => count($allItems)
        ];
    }

    /**
     * Find Operation Reports by Year and Department
     *
     * @param int $year
     * @param string $department
     * @return array
     * @throws QueryException
     */
    public function findByYear(int $year, string $department): array
    {
        if ($year && $department) {
            $start = "$year-01-01 00:00:00";
            $end = "$year-12-31 23:59:00";

            $criteria = new Criteria();
            $expressionBuilder = Criteria::expr();

            $criteria->where($expressionBuilder->eq("operation_report.department", $department));

            return $this->createQueryBuilder('operation_report')
                ->leftJoin("operation_report.incomingAlert", "iA")
                ->leftJoin("operation_report.operationReportDetail", "oRD")
                ->addCriteria($criteria)
                ->andWhere("(iA.incomingDateTime > '$start' and iA.incomingDateTime < '$end')")
                ->orderBy("operation_report.createdAt", Criteria::DESC)
                ->getQuery()
                ->getResult();
        }
        return [];
    }

}
