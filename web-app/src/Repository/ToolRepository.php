<?php

namespace App\Repository;

use App\Entity\Tool;
use App\Service\DataTableService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\QueryException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

/**
 * @extends ServiceEntityRepository<Tool>
 *
 * @method Tool|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tool|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tool[]    findAll()
 * @method Tool[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ToolRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tool::class);
    }

    /**
     * Find Entries for the Prime VUE DataTable
     * @return Tool[] Returns an array of General objects
     * @throws QueryException
     * @throws \JsonException
     */
    public function findByDataTableService(Request $requestUri, DataTableService $dataTableService, array $params, bool $showAllEntries = false): array
    {
        $uri = urldecode($requestUri->getRequestUri());
        $tableShort = "t";
        $joinTableOne = "tC";
        $globalFilter = ["$tableShort.abbreviation", "$tableShort.description", "$tableShort.comment"];

        // wurden Parameter mitgegeben?
        if ($uri && str_contains($uri, '?') && !str_contains($uri, '?{}')) {
            $service = $dataTableService;

            $service->explodeParams($uri, $globalFilter);

            // Begrenzung der Ausgabe
            $sql = $this->createQueryBuilder($tableShort)
                ->leftJoin("$tableShort.toolCategory", $joinTableOne)
                ->addCriteria($service->generateFilterShowAllEntries($showAllEntries))
                ->addCriteria($service->generateFilterShowOnlyMyDepartmentEntries($params['department'], !$params['isFullAdmin']))
                ->addCriteria($service->generateFilter());

            // Zusammenfügen der Order By Anweisung
            if (count($service->orderBy) > 0) {
                foreach ($service->orderBy as $order) {
                    $sql->addCriteria($order);
                }
            }

            $filterItems = $sql
                ->setFirstResult($service->offset)
                ->setMaxResults($service->limit)
                ->getQuery()
                ->getResult();

            // Ohne Begrenzung (für die Paginate)
            $maxItems = $this->createQueryBuilder($tableShort)
                ->leftJoin("$tableShort.toolCategory", $joinTableOne)
                ->addCriteria($service->generateFilterShowAllEntries($showAllEntries))
                ->addCriteria($service->generateFilterShowOnlyMyDepartmentEntries($params['department'], !$params['isFullAdmin']))
                ->addCriteria($service->generateFilter())
                ->getQuery()
                ->getResult();

            return [
                "items" => $filterItems,
                "filterCount" => count($filterItems),
                "totalCount" => count($maxItems)
            ];
        }

        // Ohne Filter und Begrenzung
        $allItems = $this->createQueryBuilder($tableShort)
            ->leftJoin("$tableShort.toolCategory", $joinTableOne)
            ->orderBy("$joinTableOne.sorting", Criteria::ASC)
            ->addOrderBy("$tableShort.abbreviation", Criteria::ASC)
            ->getQuery()
            ->getResult();

        return [
            "items" => $allItems,
            "filterCount" => count($allItems),
            "totalCount" => count($allItems)
        ];
    }
}
