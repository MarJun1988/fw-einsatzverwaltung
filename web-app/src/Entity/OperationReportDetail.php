<?php
declare(strict_types=1);
namespace App\Entity;

use App\Repository\OperationReportDetailRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints\NotNull;

#[ORM\Entity(repositoryClass: OperationReportDetailRepository::class)]
#[ORM\Table('`operation_report_details`')]
#[UniqueEntity(fields: ['operationType', 'startetAt'])]
class OperationReportDetail
{
    private const GROUPS = ['operationReportDetails'];
    private const GROUPS_TWO = ['operationReportDetails', 'dataByIdOperationReportDetail'];
    private const GROUPS_THREE = ['operationReportDetails', 'operationReportDetail', 'dataByIdOperationReportDetail'];

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "CUSTOM")]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[ORM\Column(type: "uuid", unique: true)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $id = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    #[NotNull, Groups(self::GROUPS_THREE), MaxDepth(1)]
    private ?OperationType $operationType = null;

    #[ORM\Column]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?\DateTimeImmutable $incomingDateTime = null;

    #[ORM\Column]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?\DateTimeImmutable $startetAt = null;

    #[ORM\Column]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?\DateTimeImmutable $endetAt = null;

    #[ORM\Column(length: 15)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $numberControlCenter = null;

    #[ORM\Column(length: 20)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $keywordShort = null;

    #[ORM\Column(length: 60)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $keywordLong = null;

    #[ORM\Column(length: 100)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $location = null;

    #[ORM\Column(length: 100, nullable: true)]
    #[Groups(self::GROUPS_THREE)]
    private ?string $otherAddressInformation = null;

    #[ORM\Column(length: 100, nullable: true)]
    #[Groups(self::GROUPS_THREE)]
    private ?string $personPatient = null;

    #[ORM\Column(type: Types::TEXT)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $messageComplete = null;

    #[ORM\Column]
    #[NotNull, Groups(self::GROUPS_TWO)]
    private ?bool $isVisibility = null;

    #[ORM\Column(length: 10)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $postalCode = null;

    #[ORM\Column(length: 100)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $place = null;

    #[ORM\Column(length: 100)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $streetWithNumber = null;

    #[ORM\Column(length: 50)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $numberFireDepartment = null;

    #[ORM\Column(length: 50, nullable: true)]
    #[Groups(self::GROUPS_THREE)]
    private ?string $numberPolice = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(self::GROUPS_THREE)]
    private ?string $comment = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(self::GROUPS_THREE)]
    private ?string $text = null;

    #[ORM\Column]
    #[Groups(self::GROUPS)]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(nullable: true)]
    #[Groups(self::GROUPS)]
    private ?\DateTimeImmutable $updatedAt = null;

    #[ORM\Column(length: 255)]
    #[Groups(self::GROUPS)]
    private ?string $createdFrom = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(self::GROUPS)]
    private ?string $updatedFrom = null;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getOperationType(): ?OperationType
    {
        return $this->operationType;
    }

    public function setOperationType(?OperationType $operationType): static
    {
        $this->operationType = $operationType;

        return $this;
    }

    public function getStartetAt(): ?\DateTimeImmutable
    {
        return $this->startetAt;
    }

    public function setStartetAt(\DateTimeImmutable $startetAt): static
    {
        $this->startetAt = $startetAt;

        return $this;
    }

    public function getEndetAt(): ?\DateTimeImmutable
    {
        return $this->endetAt;
    }

    public function setEndetAt(\DateTimeImmutable $endetAt): static
    {
        $this->endetAt = $endetAt;

        return $this;
    }

    public function getNumberControlCenter(): ?string
    {
        return $this->numberControlCenter;
    }

    public function setNumberControlCenter(string $numberControlCenter): static
    {
        $this->numberControlCenter = $numberControlCenter;

        return $this;
    }

    public function getKeywordShort(): ?string
    {
        return $this->keywordShort;
    }

    public function setKeywordShort(string $keywordShort): static
    {
        $this->keywordShort = $keywordShort;

        return $this;
    }

    public function getKeywordLong(): ?string
    {
        return $this->keywordLong;
    }

    public function setKeywordLong(string $keywordLong): static
    {
        $this->keywordLong = $keywordLong;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): static
    {
        $this->location = $location;

        return $this;
    }

    public function getOtherAddressInformation(): ?string
    {
        return $this->otherAddressInformation;
    }

    public function setOtherAddressInformation(?string $otherAddressInformation): static
    {
        $this->otherAddressInformation = $otherAddressInformation;

        return $this;
    }

    public function getPersonPatient(): ?string
    {
        return $this->personPatient;
    }

    public function setPersonPatient(?string $personPatient): static
    {
        $this->personPatient = $personPatient;

        return $this;
    }

    public function getMessageComplete(): ?string
    {
        return $this->messageComplete;
    }

    public function setMessageComplete(string $messageComplete): static
    {
        $this->messageComplete = $messageComplete;

        return $this;
    }

    public function isIsVisibility(): ?bool
    {
        return $this->isVisibility;
    }

    public function setIsVisibility(bool $isVisibility): static
    {
        $this->isVisibility = $isVisibility;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): static
    {
        $this->postalCode = $postalCode;

        return $this;
    }
    public function setPlace(string $place): static
    {
        $this->place = $place;

        return $this;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function getStreetWithNumber(): ?string
    {
        return $this->streetWithNumber;
    }

    public function setStreetWithNumber(string $streetWithNumber): static
    {
        $this->streetWithNumber = $streetWithNumber;

        return $this;
    }

    public function getNumberFireDepartment(): ?string
    {
        return $this->numberFireDepartment;
    }

    public function setNumberFireDepartment(string $numberFireDepartment): static
    {
        $this->numberFireDepartment = $numberFireDepartment;

        return $this;
    }

    public function getNumberPolice(): ?string
    {
        return $this->numberPolice;
    }

    public function setNumberPolice(?string $numberPolice): static
    {
        $this->numberPolice = $numberPolice;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getIncomingDateTime(): ?\DateTimeImmutable
    {
        return $this->incomingDateTime;
    }

    public function setIncomingDateTime(?\DateTimeImmutable $incomingDateTime): static
    {
        $this->incomingDateTime = $incomingDateTime;

        return $this;
    }

    public function getCreatedFrom(): ?string
    {
        return $this->createdFrom;
    }

    public function setCreatedFrom(string $createdFrom): static
    {
        $this->createdFrom = $createdFrom;

        return $this;
    }

    public function getUpdatedFrom(): ?string
    {
        return $this->updatedFrom;
    }

    public function setUpdatedFrom(?string $updatedFrom): static
    {
        $this->updatedFrom = $updatedFrom;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): static
    {
        $this->comment = $comment;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): static
    {
        $this->text = $text;

        return $this;
    }
}
