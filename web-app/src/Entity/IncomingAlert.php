<?php
declare(strict_types=1);
namespace App\Entity;

use App\Repository\IncomingAlertRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints\NotNull;

#[ORM\Entity(repositoryClass: IncomingAlertRepository::class)]
#[ORM\Table('`incoming_alerts`')]
#[UniqueEntity(fields: ['department', 'address', 'messageComplete', 'incomingDateTime'])]
class IncomingAlert
{
    private const GROUPS = ['incomingAlerts'];
    private const GROUPS_TWO = ['incomingAlerts', 'dataByIdIncomingAlert'];
    private const GROUPS_THREE = ['incomingAlerts', 'incomingAlert', 'dataByIdIncomingAlert'];

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "CUSTOM")]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[ORM\Column(type: "uuid", unique: true)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $id = null;

    #[ORM\ManyToOne(inversedBy: 'incomingAlerts')]
    #[ORM\JoinColumn(nullable: false)]
    #[NotNull, Groups(self::GROUPS_THREE), MaxDepth(1)]
    private ?AlertAddress $address = null;

    #[ORM\ManyToOne(inversedBy: 'incomingAlerts')]
    #[ORM\JoinColumn(nullable: false)]
    #[NotNull, Groups(self::GROUPS_TWO), MaxDepth(1)]
    private ?FederalState $federalState = null;

    #[ORM\ManyToOne(inversedBy: 'incomingAlerts')]
    #[ORM\JoinColumn(nullable: false)]
    #[NotNull, Groups(self::GROUPS_TWO), MaxDepth(1)]
    private ?Department $department = null;

    #[ORM\Column(length: 255)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $text = null;

    #[ORM\Column]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?\DateTimeImmutable $incomingDateTime = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(self::GROUPS_TWO)]
    private ?string $comment = null;

    #[ORM\Column]
    #[Groups(self::GROUPS)]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(nullable: true)]
    #[Groups(self::GROUPS)]
    private ?\DateTimeImmutable $updatedAt = null;

    #[ORM\Column(length: 255)]
    #[Groups(self::GROUPS)]
    private ?string $createdFrom = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(self::GROUPS)]
    private ?string $updatedFrom = null;

    #[ORM\Column(options: ["default" => true])]
    #[Groups(self::GROUPS_TWO)]
    private ?bool $isVisibility = null;

    #[ORM\Column(options: ["default" => true])]
    #[Groups(self::GROUPS_TWO)]
    private ?bool $isVisibilityOperationReport = null;

    #[ORM\Column(type: Types::TEXT)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $messageComplete = null;

    #[ORM\Column(length: 50, nullable: true)]
    #[Groups(self::GROUPS_THREE)]
    private ?string $numberControlCenter = null;

    #[ORM\Column(length: 50, nullable: true)]
    #[Groups(self::GROUPS_THREE)]
    private ?string $keywordShort = null;

    #[ORM\Column(length: 100, nullable: true)]
    #[Groups(self::GROUPS_THREE)]
    private ?string $keywordLong = null;

    #[ORM\Column(length: 200, nullable: true)]
    #[Groups(self::GROUPS_THREE)]
    private ?string $location = null;

    #[ORM\Column(length: 200, nullable: true)]
    #[Groups(self::GROUPS_THREE)]
    private ?string $otherAddressInformation = null;

    #[ORM\Column(length: 100, nullable: true)]
    #[Groups(self::GROUPS_THREE)]
    private ?string $personPatient = null;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getAddress(): ?AlertAddress
    {
        return $this->address;
    }

    public function setAddress(?AlertAddress $address): static
    {
        $this->address = $address;

        return $this;
    }

    public function getFederalState(): ?FederalState
    {
        return $this->federalState;
    }

    public function setFederalState(?FederalState $federalState): static
    {
        $this->federalState = $federalState;

        return $this;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(?Department $department): static
    {
        $this->department = $department;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): static
    {
        $this->text = $text;

        return $this;
    }


    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): static
    {
        $this->comment = $comment;

        return $this;
    }

    public function getIncomingDateTime(): ?\DateTimeImmutable
    {
        return $this->incomingDateTime;
    }

    public function setIncomingDateTime(\DateTimeImmutable $incomingDateTime): static
    {
        $this->incomingDateTime = $incomingDateTime;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedFrom(): ?string
    {
        return $this->createdFrom;
    }

    public function setCreatedFrom(string $createdFrom): static
    {
        $this->createdFrom = $createdFrom;

        return $this;
    }

    public function getUpdatedFrom(): ?string
    {
        return $this->updatedFrom;
    }

    public function setUpdatedFrom(?string $updatedFrom): static
    {
        $this->updatedFrom = $updatedFrom;

        return $this;
    }

    public function isIsVisibility(): ?bool
    {
        return $this->isVisibility;
    }

    public function setIsVisibility(bool $isVisibility): static
    {
        $this->isVisibility = $isVisibility;

        return $this;
    }

    public function isVisibilityOperationReport(): ?bool
    {
        return $this->isVisibilityOperationReport;
    }

    public function setIsVisibilityOperationReport(bool $isVisibilityOperationReport): static
    {
        $this->isVisibilityOperationReport = $isVisibilityOperationReport;

        return $this;
    }

    public function getMessageComplete(): ?string
    {
        return $this->messageComplete;
    }

    public function setMessageComplete(string $messageComplete): static
    {
        $this->messageComplete = $messageComplete;

        return $this;
    }

    public function getNumberControlCenter(): ?string
    {
        return $this->numberControlCenter;
    }

    public function setNumberControlCenter(?string $numberControlCenter): static
    {
        $this->numberControlCenter = $numberControlCenter;

        return $this;
    }

    public function getKeywordShort(): ?string
    {
        return $this->keywordShort;
    }

    public function setKeywordShort(?string $keywordShort): static
    {
        $this->keywordShort = $keywordShort;

        return $this;
    }

    public function getKeywordLong(): ?string
    {
        return $this->keywordLong;
    }

    public function setKeywordLong(?string $keywordLong): static
    {
        $this->keywordLong = $keywordLong;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): static
    {
        $this->location = $location;

        return $this;
    }

    public function getOtherAddressInformation(): ?string
    {
        return $this->otherAddressInformation;
    }

    public function setOtherAddressInformation(?string $otherAddressInformation): static
    {
        $this->otherAddressInformation = $otherAddressInformation;

        return $this;
    }

    public function getPersonPatient(): ?string
    {
        return $this->personPatient;
    }

    public function setPersonPatient(?string $personPatient): static
    {
        $this->personPatient = $personPatient;

        return $this;
    }
}
