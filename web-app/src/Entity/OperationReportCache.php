<?php

namespace App\Entity;

use App\Repository\OperationReportCacheRepository;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotNull;

#[ORM\Entity(repositoryClass: OperationReportCacheRepository::class)]
#[ORM\Table(name: '`operation_reports_cache`')]
class OperationReportCache
{
    private const GROUPS = ['operationReportCaches'];
    private const GROUPS_TWO = ['operationReportCaches', 'dataByIdOperationReportCache'];
    private const GROUPS_THREE = ['operationReportCaches', 'operationReportCache', 'dataByIdOperationReportCache'];

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "CUSTOM")]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[ORM\Column(type: "uuid", unique: true)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $id = null;

    #[ORM\Column]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private array $store = [];

    #[ORM\ManyToOne(inversedBy: 'operationReportCaches')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Department $department = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(self::GROUPS_TWO)]
    private ?string $comment = null;

    #[ORM\Column]
    #[Groups(self::GROUPS)]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(nullable: true)]
    #[Groups(self::GROUPS)]
    private ?\DateTimeImmutable $updatedAt = null;

    #[ORM\Column(length: 255)]
    #[Groups(self::GROUPS)]
    private ?string $createdFrom = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(self::GROUPS)]
    private ?string $updatedFrom = null;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getStore(): array
    {
        return $this->store;
    }

    public function setStore(array $store): static
    {
        $this->store = $store;

        return $this;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(?Department $department): static
    {
        $this->department = $department;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): static
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedFrom(): ?string
    {
        return $this->createdFrom;
    }

    public function setCreatedFrom(string $createdFrom): static
    {
        $this->createdFrom = $createdFrom;

        return $this;
    }

    public function getUpdatedFrom(): ?string
    {
        return $this->updatedFrom;
    }

    public function setUpdatedFrom(?string $updatedFrom): static
    {
        $this->updatedFrom = $updatedFrom;

        return $this;
    }
}
