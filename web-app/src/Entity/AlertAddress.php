<?php
declare(strict_types=1);

namespace App\Entity;

use App\Repository\AlertAddressRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints\NotNull;

#[ORM\Entity(repositoryClass: AlertAddressRepository::class)]
#[ORM\Table('`alert_addresses`')]
#[UniqueEntity(fields: ['address', 'description', 'department'])]
class AlertAddress
{
    private const GROUPS = ['alertAddresses'];
    private const GROUPS_TWO = ['alertAddresses', 'dataByIdAlertAddress'];
    private const GROUPS_THREE = ['alertAddresses', 'alertAddress', 'dataByIdAlertAddress'];

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "CUSTOM")]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[ORM\Column(type: "uuid", unique: true)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $id = null;

    #[ORM\Column(length: 20)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $address = null;

    #[ORM\Column(length: 255)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $description = null;

    #[ORM\Column(options: ["default" => true])]
    #[Groups(self::GROUPS_TWO)]
    private ?bool $isVisibility = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(self::GROUPS_TWO)]
    private ?string $comment = null;

    #[ORM\Column]
    #[Groups(self::GROUPS)]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(nullable: true)]
    #[Groups(self::GROUPS)]
    private ?\DateTimeImmutable $updatedAt = null;

    #[ORM\Column(length: 255)]
    #[Groups(self::GROUPS)]
    private ?string $createdFrom = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(self::GROUPS)]
    private ?string $updatedFrom = null;

    #[ORM\ManyToOne(inversedBy: 'alertAddresses')]
    #[ORM\JoinColumn(nullable: false)]
    #[NotNull, Groups(self::GROUPS_TWO), MaxDepth(1)]
    private ?FederalState $federalState = null;

    #[ORM\ManyToOne(inversedBy: 'alertAddresses')]
    #[ORM\JoinColumn(nullable: false)]
    #[NotNull, Groups(self::GROUPS_TWO), MaxDepth(1)]
    private ?Department $department = null;

    #[ORM\OneToMany(mappedBy: 'address', targetEntity: IncomingAlert::class)]
    #[Groups(self::GROUPS), MaxDepth(1)]
    private Collection $incomingAlerts;

    public function __construct()
    {
        $this->incomingAlerts = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): static
    {
        $this->address = $address;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function isIsVisibility(): ?bool
    {
        return $this->isVisibility;
    }

    public function setIsVisibility(bool $isVisibility): static
    {
        $this->isVisibility = $isVisibility;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): static
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedFrom(): ?string
    {
        return $this->createdFrom;
    }

    public function setCreatedFrom(string $createdFrom): static
    {
        $this->createdFrom = $createdFrom;

        return $this;
    }

    public function getUpdatedFrom(): ?string
    {
        return $this->updatedFrom;
    }

    public function setUpdatedFrom(?string $updatedFrom): static
    {
        $this->updatedFrom = $updatedFrom;

        return $this;
    }

    public function getFederalState(): ?FederalState
    {
        return $this->federalState;
    }

    public function setFederalState(?FederalState $federalState): static
    {
        $this->federalState = $federalState;

        return $this;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(?Department $department): static
    {
        $this->department = $department;

        return $this;
    }

    /**
     * @return Collection<int, IncomingAlert>
     */
    public function getIncomingAlerts(): Collection
    {
        return $this->incomingAlerts;
    }

    public function addIncomingAlert(IncomingAlert $incomingAlert): static
    {
        if (!$this->incomingAlerts->contains($incomingAlert)) {
            $this->incomingAlerts->add($incomingAlert);
            $incomingAlert->setAddress($this);
        }

        return $this;
    }

    public function removeIncomingAlert(IncomingAlert $incomingAlert): static
    {
        if ($this->incomingAlerts->removeElement($incomingAlert)) {
            // set the owning side to null (unless already changed)
            if ($incomingAlert->getAddress() === $this) {
                $incomingAlert->setAddress(null);
            }
        }

        return $this;
    }
}
