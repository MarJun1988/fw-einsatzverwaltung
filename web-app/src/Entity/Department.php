<?php
declare(strict_types=1);
namespace App\Entity;

use App\Repository\DepartmentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints\NotNull;

#[ORM\Entity(repositoryClass: DepartmentRepository::class)]
#[ORM\Table('`departments`')]
#[UniqueEntity(fields: ['abbreviation', 'description', 'federalState'])]
class Department
{
    private const GROUPS = ['departments'];
    private const GROUPS_TWO = ['departments', 'dataByIdDepartment'];
    private const GROUPS_THREE = ['departments', 'department', 'dataByIdDepartment'];

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "CUSTOM")]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[ORM\Column(type: "uuid", unique: true)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $id = null;

    #[ORM\Column(length: 50)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $abbreviation = null;

    #[ORM\Column(length: 255)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $description = null;

    #[ORM\ManyToOne(inversedBy: 'departments')]
    #[ORM\JoinColumn(nullable: false)]
    #[NotNull, Groups(self::GROUPS_TWO), MaxDepth(1)]
    private ?FederalState $federalState = null;

    #[ORM\Column(length: 10)]
    #[NotNull, Groups(self::GROUPS_TWO)]
    private ?string $zipCode = null;

    #[ORM\Column(length: 255)]
    #[NotNull, Groups(self::GROUPS_TWO)]
    private ?string $placeName = null;

    #[ORM\Column(length: 255)]
    #[NotNull, Groups(self::GROUPS_TWO)]
    private ?string $street = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(self::GROUPS_TWO)]
    private ?string $comment = null;

    #[ORM\Column]
    #[Groups(self::GROUPS)]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(nullable: true)]
    #[Groups(self::GROUPS)]
    private ?\DateTimeImmutable $updatedAt = null;

    #[ORM\Column(length: 255)]
    #[Groups(self::GROUPS)]
    private ?string $createdFrom = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(self::GROUPS)]
    private ?string $updatedFrom = null;

    #[ORM\Column(options: ["default" => true])]
    #[Groups(self::GROUPS_TWO)]
    private ?bool $isVisibility = null;

    #[ORM\OneToMany(mappedBy: 'department', targetEntity: User::class)]
    #[Groups(self::GROUPS), MaxDepth(1)]
    private Collection $users;

    #[ORM\OneToMany(mappedBy: 'department', targetEntity: Vehicle::class)]
    #[Groups(self::GROUPS), MaxDepth(1)]
    private Collection $vehicles;

    #[ORM\OneToMany(mappedBy: 'department', targetEntity: ToolCategory::class)]
    #[Groups(self::GROUPS), MaxDepth(1)]
    private Collection $toolCategories;

    #[ORM\OneToMany(mappedBy: 'department', targetEntity: AlertAddress::class)]
    #[Groups(self::GROUPS), MaxDepth(1)]
    private Collection $alertAddresses;

    #[ORM\OneToMany(mappedBy: 'department', targetEntity: Member::class)]
    #[Groups(self::GROUPS), MaxDepth(1)]
    private Collection $members;

    #[ORM\OneToMany(mappedBy: 'department', targetEntity: IncomingAlert::class)]
    #[Groups(self::GROUPS), MaxDepth(1)]
    private Collection $incomingAlerts;

    #[ORM\OneToMany(mappedBy: 'department', targetEntity: OperationReport::class)]
    #[Groups(self::GROUPS), MaxDepth(1)]
    private Collection $operationReports;

    #[ORM\OneToMany(mappedBy: 'department', targetEntity: OperationReportCache::class)]
    private Collection $operationReportCaches;

    #[ORM\OneToMany(mappedBy: 'department', targetEntity: Tool::class)]
    private Collection $tools;

    #[ORM\OneToMany(mappedBy: 'department', targetEntity: Feedback::class)]
    private Collection $feedback;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->vehicles = new ArrayCollection();
        $this->toolCategories = new ArrayCollection();
        $this->alertAddresses = new ArrayCollection();
        $this->members = new ArrayCollection();
        $this->incomingAlerts = new ArrayCollection();
        $this->operationReports = new ArrayCollection();
        $this->operationReportCaches = new ArrayCollection();
        $this->tools = new ArrayCollection();
        $this->feedback = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getAbbreviation(): ?string
    {
        return $this->abbreviation;
    }

    public function setAbbreviation(string $abbreviation): static
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getFederalState(): ?FederalState
    {
        return $this->federalState;
    }

    public function setFederalState(?FederalState $federalState): static
    {
        $this->federalState = $federalState;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): static
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getPlaceName(): ?string
    {
        return $this->placeName;
    }

    public function setPlaceName(string $placeName): static
    {
        $this->placeName = $placeName;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): static
    {
        $this->street = $street;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): static
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedFrom(): ?string
    {
        return $this->createdFrom;
    }

    public function setCreatedFrom(string $createdFrom): static
    {
        $this->createdFrom = $createdFrom;

        return $this;
    }

    public function getUpdatedFrom(): ?string
    {
        return $this->updatedFrom;
    }

    public function setUpdatedFrom(?string $updatedFrom): static
    {
        $this->updatedFrom = $updatedFrom;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): static
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->setDepartment($this);
        }

        return $this;
    }

    public function removeUser(User $user): static
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getDepartment() === $this) {
                $user->setDepartment(null);
            }
        }

        return $this;
    }

    public function isIsVisibility(): ?bool
    {
        return $this->isVisibility;
    }

    public function setIsVisibility(bool $isVisibility): static
    {
        $this->isVisibility = $isVisibility;

        return $this;
    }

    /**
     * @return Collection<int, Vehicle>
     */
    public function getVehicles(): Collection
    {
        return $this->vehicles;
    }

    public function addVehicle(Vehicle $vehicle): static
    {
        if (!$this->vehicles->contains($vehicle)) {
            $this->vehicles->add($vehicle);
            $vehicle->setDepartment($this);
        }

        return $this;
    }

    public function removeVehicle(Vehicle $vehicle): static
    {
        if ($this->vehicles->removeElement($vehicle)) {
            // set the owning side to null (unless already changed)
            if ($vehicle->getDepartment() === $this) {
                $vehicle->setDepartment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ToolCategory>
     */
    public function getToolCategories(): Collection
    {
        return $this->toolCategories;
    }

    public function addToolCategory(ToolCategory $toolCategory): static
    {
        if (!$this->toolCategories->contains($toolCategory)) {
            $this->toolCategories->add($toolCategory);
            $toolCategory->setDepartment($this);
        }

        return $this;
    }

    public function removeToolCategory(ToolCategory $toolCategory): static
    {
        if ($this->toolCategories->removeElement($toolCategory)) {
            // set the owning side to null (unless already changed)
            if ($toolCategory->getDepartment() === $this) {
                $toolCategory->setDepartment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, AlertAddress>
     */
    public function getAlertAddresses(): Collection
    {
        return $this->alertAddresses;
    }

    public function addAlertAddress(AlertAddress $alertAddress): static
    {
        if (!$this->alertAddresses->contains($alertAddress)) {
            $this->alertAddresses->add($alertAddress);
            $alertAddress->setDepartment($this);
        }

        return $this;
    }

    public function removeAlertAddress(AlertAddress $alertAddress): static
    {
        if ($this->alertAddresses->removeElement($alertAddress)) {
            // set the owning side to null (unless already changed)
            if ($alertAddress->getDepartment() === $this) {
                $alertAddress->setDepartment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Member>
     */
    public function getMembers(): Collection
    {
        return $this->members;
    }

    public function addMembers(Member $members): static
    {
        if (!$this->members->contains($members)) {
            $this->members->add($members);
            $members->setDepartment($this);
        }

        return $this;
    }

    public function removeMembers(Member $members): static
    {
        if ($this->members->removeElement($members)) {
            // set the owning side to null (unless already changed)
            if ($members->getDepartment() === $this) {
                $members->setDepartment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, IncomingAlert>
     */
    public function getIncomingAlerts(): Collection
    {
        return $this->incomingAlerts;
    }

    public function addIncomingAlert(IncomingAlert $incomingAlert): static
    {
        if (!$this->incomingAlerts->contains($incomingAlert)) {
            $this->incomingAlerts->add($incomingAlert);
            $incomingAlert->setDepartment($this);
        }

        return $this;
    }

    public function removeIncomingAlert(IncomingAlert $incomingAlert): static
    {
        if ($this->incomingAlerts->removeElement($incomingAlert)) {
            // set the owning side to null (unless already changed)
            if ($incomingAlert->getDepartment() === $this) {
                $incomingAlert->setDepartment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, OperationReport>
     */
    public function getOperationReports(): Collection
    {
        return $this->operationReports;
    }

    public function addOperationReport(OperationReport $operationReport): static
    {
        if (!$this->operationReports->contains($operationReport)) {
            $this->operationReports->add($operationReport);
            $operationReport->setDepartment($this);
        }

        return $this;
    }

    public function removeOperationReport(OperationReport $operationReport): static
    {
        if ($this->operationReports->removeElement($operationReport)) {
            // set the owning side to null (unless already changed)
            if ($operationReport->getDepartment() === $this) {
                $operationReport->setDepartment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, OperationReportCache>
     */
    public function getOperationReportCaches(): Collection
    {
        return $this->operationReportCaches;
    }

    public function addOperationReportCache(OperationReportCache $operationReportCache): static
    {
        if (!$this->operationReportCaches->contains($operationReportCache)) {
            $this->operationReportCaches->add($operationReportCache);
            $operationReportCache->setDepartment($this);
        }

        return $this;
    }

    public function removeOperationReportCache(OperationReportCache $operationReportCache): static
    {
        if ($this->operationReportCaches->removeElement($operationReportCache)) {
            // set the owning side to null (unless already changed)
            if ($operationReportCache->getDepartment() === $this) {
                $operationReportCache->setDepartment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Tool>
     */
    public function getTools(): Collection
    {
        return $this->tools;
    }

    public function addTool(Tool $tool): static
    {
        if (!$this->tools->contains($tool)) {
            $this->tools->add($tool);
            $tool->setDepartment($this);
        }

        return $this;
    }

    public function removeTool(Tool $tool): static
    {
        if ($this->tools->removeElement($tool)) {
            // set the owning side to null (unless already changed)
            if ($tool->getDepartment() === $this) {
                $tool->setDepartment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Feedback>
     */
    public function getFeedback(): Collection
    {
        return $this->feedback;
    }

    public function addFeedback(Feedback $feedback): static
    {
        if (!$this->feedback->contains($feedback)) {
            $this->feedback->add($feedback);
            $feedback->setDepartment($this);
        }

        return $this;
    }

    public function removeFeedback(Feedback $feedback): static
    {
        if ($this->feedback->removeElement($feedback)) {
            // set the owning side to null (unless already changed)
            if ($feedback->getDepartment() === $this) {
                $feedback->setDepartment(null);
            }
        }

        return $this;
    }
}
