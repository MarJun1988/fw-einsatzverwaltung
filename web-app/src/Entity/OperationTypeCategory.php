<?php
declare(strict_types=1);
namespace App\Entity;

use App\Repository\OperationTypeCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints\NotNull;

#[ORM\Entity(repositoryClass: OperationTypeCategoryRepository::class)]
#[ORM\Table('`operation_type_categories`')]
#[UniqueEntity(fields: ['department', 'abbreviation', 'description'])]
class OperationTypeCategory
{
    private const GROUPS = ['operationTypeCategories'];
    private const GROUPS_TWO = ['operationTypeCategories', 'dataByIdOperationTypeCategory'];
    private const GROUPS_THREE = ['operationTypeCategories', 'operationTypeCategory', 'dataByIdOperationTypeCategory'];

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "CUSTOM")]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[ORM\Column(type: "uuid", unique: true)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $id = null;

    #[ORM\Column(length: 50)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $abbreviation = null;

    #[ORM\Column(length: 255)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $description = null;

    #[ORM\Column]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?int $sorting = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(self::GROUPS_THREE)]
    private ?string $comment = null;

    #[ORM\Column]
    #[Groups(self::GROUPS)]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(nullable: true)]
    #[Groups(self::GROUPS)]
    private ?\DateTimeImmutable $updatedAt = null;

    #[ORM\Column(length: 255)]
    #[Groups(self::GROUPS)]
    private ?string $createdFrom = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(self::GROUPS)]
    private ?string $updatedFrom = null;

    #[ORM\Column(options: ["default" => true])]
    #[Groups(self::GROUPS_THREE)]
    private ?bool $isVisibility = null;

    #[ORM\OneToMany(mappedBy: 'operationTypeCategory', targetEntity: OperationType::class)]
    #[Groups(self::GROUPS_TWO), MaxDepth(1)]
    private Collection $operationTypes;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    #[NotNull, Groups(self::GROUPS_TWO), MaxDepth(1)]
    private ?Department $department = null;

    public function __construct()
    {
        $this->operationTypes = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getAbbreviation(): ?string
    {
        return $this->abbreviation;
    }

    public function setAbbreviation(string $abbreviation): static
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getSorting(): ?int
    {
        return $this->sorting;
    }

    public function setSorting(int $sorting): static
    {
        $this->sorting = $sorting;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): static
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedFrom(): ?string
    {
        return $this->createdFrom;
    }

    public function setCreatedFrom(string $createdFrom): static
    {
        $this->createdFrom = $createdFrom;

        return $this;
    }

    public function getUpdatedFrom(): ?string
    {
        return $this->updatedFrom;
    }

    public function setUpdatedFrom(?string $updatedFrom): static
    {
        $this->updatedFrom = $updatedFrom;

        return $this;
    }

    public function isIsVisibility(): ?bool
    {
        return $this->isVisibility;
    }

    public function setIsVisibility(bool $isVisibility): static
    {
        $this->isVisibility = $isVisibility;

        return $this;
    }

    /**
     * @return Collection<int, OperationType>
     */
    public function getOperationTypes(): Collection
    {
        return $this->operationTypes;
    }

    public function addOperationType(OperationType $operationType): static
    {
        if (!$this->operationTypes->contains($operationType)) {
            $this->operationTypes->add($operationType);
            $operationType->setOperationTypeCategory($this);
        }

        return $this;
    }

    public function removeOperationType(OperationType $operationType): static
    {
        if ($this->operationTypes->removeElement($operationType)) {
            // set the owning side to null (unless already changed)
            if ($operationType->getOperationTypeCategory() === $this) {
                $operationType->setOperationTypeCategory(null);
            }
        }

        return $this;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(?Department $department): static
    {
        $this->department = $department;

        return $this;
    }
}
