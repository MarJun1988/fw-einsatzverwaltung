<?php
declare(strict_types=1);
namespace App\Entity;

use App\Repository\FederalStateRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints\NotNull;

#[ORM\Entity(repositoryClass: FederalStateRepository::class)]
#[ORM\Table('`federal_states`')]
#[UniqueEntity(fields: ['abbreviation', 'description'])]
class FederalState
{
    private const GROUPS = ['federalStates'];
    private const GROUPS_TWO = ['federalStates', 'dataByIdFederalState'];
    private const GROUPS_THREE = ['federalStates', 'federalState', 'dataByIdFederalState'];

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "CUSTOM")]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[ORM\Column(type: "uuid", unique: true)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $id = null;

    #[ORM\Column(length: 10)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $abbreviation = null;

    #[ORM\Column(length: 255)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $description = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(self::GROUPS_TWO)]
    private ?string $comment = null;

    #[ORM\Column]
    #[Groups(self::GROUPS)]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(nullable: true)]
    #[Groups(self::GROUPS)]
    private ?\DateTimeImmutable $updatedAt = null;

    #[ORM\Column(length: 255)]
    #[Groups(self::GROUPS)]
    private ?string $createdFrom = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(self::GROUPS)]
    private ?string $updatedFrom = null;

    #[ORM\Column(options: ["default" => true])]
    #[Groups(self::GROUPS_TWO)]
    private ?bool $isVisibility = null;

    #[ORM\OneToMany(mappedBy: 'federalState', targetEntity: Department::class)]
    #[Groups(self::GROUPS), MaxDepth(1)]
    private Collection $departments;

    #[ORM\OneToMany(mappedBy: 'federalState', targetEntity: User::class)]
    #[Groups(self::GROUPS), MaxDepth(1)]
    private Collection $users;

    #[ORM\OneToMany(mappedBy: 'federalState', targetEntity: Vehicle::class)]
    #[Groups(self::GROUPS), MaxDepth(1)]
    private Collection $vehicles;

    #[ORM\OneToMany(mappedBy: 'federalState', targetEntity: AlertAddress::class)]
    #[Groups(self::GROUPS), MaxDepth(1)]
    private Collection $alertAddresses;

    #[ORM\OneToMany(mappedBy: 'federalState', targetEntity: Member::class)]
    #[Groups(self::GROUPS), MaxDepth(1)]
    private Collection $members;

    #[ORM\OneToMany(mappedBy: 'federalState', targetEntity: IncomingAlert::class)]
    #[Groups(self::GROUPS), MaxDepth(1)]
    private Collection $incomingAlerts;

    #[ORM\OneToMany(mappedBy: 'federalState', targetEntity: OperationReport::class)]
    private Collection $operationReports;

    #[ORM\OneToMany(mappedBy: 'federalState', targetEntity: Feedback::class)]
    private Collection $feedback;

    public function __construct()
    {
        $this->departments = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->vehicles = new ArrayCollection();
        $this->alertAddresses = new ArrayCollection();
        $this->members = new ArrayCollection();
        $this->incomingAlerts = new ArrayCollection();
        $this->operationReports = new ArrayCollection();
        $this->feedback = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getAbbreviation(): ?string
    {
        return $this->abbreviation;
    }

    public function setAbbreviation(string $abbreviation): static
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): static
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedFrom(): ?string
    {
        return $this->createdFrom;
    }

    public function setCreatedFrom(string $createdFrom): static
    {
        $this->createdFrom = $createdFrom;

        return $this;
    }

    public function getUpdatedFrom(): ?string
    {
        return $this->updatedFrom;
    }

    public function setUpdatedFrom(?string $updatedFrom): static
    {
        $this->updatedFrom = $updatedFrom;

        return $this;
    }

    /**
     * @return Collection<int, Department>
     */
    public function getDepartments(): Collection
    {
        return $this->departments;
    }

    public function addDepartment(Department $department): static
    {
        if (!$this->departments->contains($department)) {
            $this->departments->add($department);
            $department->setFederalState($this);
        }

        return $this;
    }

    public function removeDepartment(Department $department): static
    {
        if ($this->departments->removeElement($department)) {
            // set the owning side to null (unless already changed)
            if ($department->getFederalState() === $this) {
                $department->setFederalState(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): static
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->setFederalState($this);
        }

        return $this;
    }

    public function removeUser(User $user): static
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getFederalState() === $this) {
                $user->setFederalState(null);
            }
        }

        return $this;
    }

    public function isIsVisibility(): ?bool
    {
        return $this->isVisibility;
    }

    public function setIsVisibility(bool $isVisibility): static
    {
        $this->isVisibility = $isVisibility;

        return $this;
    }

    /**
     * @return Collection<int, Vehicle>
     */
    public function getVehicles(): Collection
    {
        return $this->vehicles;
    }

    public function addVehicle(Vehicle $vehicle): static
    {
        if (!$this->vehicles->contains($vehicle)) {
            $this->vehicles->add($vehicle);
            $vehicle->setFederalState($this);
        }

        return $this;
    }

    public function removeVehicle(Vehicle $vehicle): static
    {
        if ($this->vehicles->removeElement($vehicle)) {
            // set the owning side to null (unless already changed)
            if ($vehicle->getFederalState() === $this) {
                $vehicle->setFederalState(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, AlertAddress>
     */
    public function getAlertAddresses(): Collection
    {
        return $this->alertAddresses;
    }

    public function addAlertAddress(AlertAddress $alertAddress): static
    {
        if (!$this->alertAddresses->contains($alertAddress)) {
            $this->alertAddresses->add($alertAddress);
            $alertAddress->setFederalState($this);
        }

        return $this;
    }

    public function removeAlertAddress(AlertAddress $alertAddress): static
    {
        if ($this->alertAddresses->removeElement($alertAddress)) {
            // set the owning side to null (unless already changed)
            if ($alertAddress->getFederalState() === $this) {
                $alertAddress->setFederalState(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Member>
     */
    public function getMembers(): Collection
    {
        return $this->members;
    }

    public function addMember(Member $member): static
    {
        if (!$this->members->contains($member)) {
            $this->members->add($member);
            $member->setFederalState($this);
        }

        return $this;
    }

    public function removeMember(Member $member): static
    {
        if ($this->members->removeElement($member)) {
            // set the owning side to null (unless already changed)
            if ($member->getFederalState() === $this) {
                $member->setFederalState(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, IncomingAlert>
     */
    public function getIncomingAlerts(): Collection
    {
        return $this->incomingAlerts;
    }

    public function addIncomingAlert(IncomingAlert $incomingAlert): static
    {
        if (!$this->incomingAlerts->contains($incomingAlert)) {
            $this->incomingAlerts->add($incomingAlert);
            $incomingAlert->setFederalState($this);
        }

        return $this;
    }

    public function removeIncomingAlert(IncomingAlert $incomingAlert): static
    {
        if ($this->incomingAlerts->removeElement($incomingAlert)) {
            // set the owning side to null (unless already changed)
            if ($incomingAlert->getFederalState() === $this) {
                $incomingAlert->setFederalState(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, OperationReport>
     */
    public function getOperationReports(): Collection
    {
        return $this->operationReports;
    }

    public function addOperationReport(OperationReport $operationReport): static
    {
        if (!$this->operationReports->contains($operationReport)) {
            $this->operationReports->add($operationReport);
            $operationReport->setFederalState($this);
        }

        return $this;
    }

    public function removeOperationReport(OperationReport $operationReport): static
    {
        if ($this->operationReports->removeElement($operationReport)) {
            // set the owning side to null (unless already changed)
            if ($operationReport->getFederalState() === $this) {
                $operationReport->setFederalState(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Feedback>
     */
    public function getFeedback(): Collection
    {
        return $this->feedback;
    }

    public function addFeedback(Feedback $feedback): static
    {
        if (!$this->feedback->contains($feedback)) {
            $this->feedback->add($feedback);
            $feedback->setFederalState($this);
        }

        return $this;
    }

    public function removeFeedback(Feedback $feedback): static
    {
        if ($this->feedback->removeElement($feedback)) {
            // set the owning side to null (unless already changed)
            if ($feedback->getFederalState() === $this) {
                $feedback->setFederalState(null);
            }
        }

        return $this;
    }
}
