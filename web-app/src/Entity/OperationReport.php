<?php
declare(strict_types=1);
namespace App\Entity;

use App\Repository\OperationReportRepository;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints\NotNull;

#[ORM\Entity(repositoryClass: OperationReportRepository::class)]
#[ORM\Table('`operation_reports`')]
#[UniqueEntity(fields: ['id'])]
class OperationReport
{
    private const GROUPS = ['operationReports'];
    private const GROUPS_TWO = ['operationReports', 'dataByIdOperationReport'];
    private const GROUPS_THREE = ['operationReports', 'operationReport', 'dataByIdOperationReport'];

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "CUSTOM")]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[ORM\Column(type: "uuid", unique: true)]
    #[NotNull, Groups(self::GROUPS_THREE)]
    private ?string $id = null;

    #[ORM\ManyToOne(inversedBy: 'operationReports')]
    #[ORM\JoinColumn(nullable: false)]
    #[NotNull, Groups(self::GROUPS_TWO), MaxDepth(1)]
    private ?FederalState $federalState = null;

    #[ORM\ManyToOne(inversedBy: 'operationReports')]
    #[ORM\JoinColumn(nullable: false)]
    #[NotNull, Groups(self::GROUPS_TWO), MaxDepth(1)]
    private ?Department $department = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    #[NotNull, Groups(self::GROUPS_TWO), MaxDepth(1)]
    private ?OperationReportDetail $operationReportDetail = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    #[NotNull, Groups(self::GROUPS_TWO), MaxDepth(1)]
    private ?IncomingAlert $incomingAlert = null;

    #[ORM\Column]
    #[NotNull, Groups(self::GROUPS_TWO)]
    private array $ownForces = [];

    #[ORM\Column(nullable: true)]
    #[Groups(self::GROUPS_TWO)]
    private ?array $otherTaskForces = null;

    #[ORM\Column(nullable: true)]
    #[Groups(self::GROUPS_TWO)]
    private ?array $tools = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(self::GROUPS_TWO)]
    private ?string $exportPdfPath = null;

    #[ORM\Column(nullable: true)]
    #[Groups(self::GROUPS_TWO)]
    private ?array $sessionStore = null;

    #[ORM\Column(type: "text",length: 65535, nullable: true)]
    #[Groups(self::GROUPS_TWO)]
    private ?string $commentPrint = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(self::GROUPS_TWO)]
    private ?string $comment = null;

    #[ORM\Column]
    #[Groups(self::GROUPS)]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(nullable: true)]
    #[Groups(self::GROUPS)]
    private ?\DateTimeImmutable $updatedAt = null;

    #[ORM\Column(length: 255)]
    #[Groups(self::GROUPS)]
    private ?string $createdFrom = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(self::GROUPS)]
    private ?string $updatedFrom = null;

    #[ORM\Column(options: ["default" => true])]
    #[Groups(self::GROUPS_TWO)]
    private ?bool $isVisibility = null;

    public function getId(): ?string
    {
        return $this->id;
    }


    public function getFederalState(): ?FederalState
    {
        return $this->federalState;
    }

    public function setFederalState(?FederalState $federalState): static
    {
        $this->federalState = $federalState;

        return $this;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(?Department $department): static
    {
        $this->department = $department;

        return $this;
    }

    public function getOperationReportDetail(): ?OperationReportDetail
    {
        return $this->operationReportDetail;
    }

    public function setOperationReportDetail(?OperationReportDetail $operationReportDetail): static
    {
        $this->operationReportDetail = $operationReportDetail;

        return $this;
    }

    public function getIncomingAlert(): ?IncomingAlert
    {
        return $this->incomingAlert;
    }

    public function setIncomingAlert(?IncomingAlert $incomingAlert): static
    {
        $this->incomingAlert = $incomingAlert;

        return $this;
    }

    public function getOwnForces(): array
    {
        return $this->ownForces;
    }

    public function setOwnForces(array $ownForces): static
    {
        $this->ownForces = $ownForces;

        return $this;
    }

    public function getOtherTaskForces(): ?array
    {
        return $this->otherTaskForces;
    }

    public function setOtherTaskForces(?array $otherTaskForces): static
    {
        $this->otherTaskForces = $otherTaskForces;

        return $this;
    }

    public function getTools(): ?array
    {
        return $this->tools;
    }

    public function setTools(?array $tools): static
    {
        $this->tools = $tools;

        return $this;
    }

    public function getExportPdfPath(): ?string
    {
        return $this->exportPdfPath;
    }

    public function setExportPdfPath(?string $exportPdfPath): static
    {
        $this->exportPdfPath = $exportPdfPath;

        return $this;
    }

    public function getSessionStore(): ?array
    {
        return $this->sessionStore;
    }

    public function setSessionStore(?array $sessionStore): static
    {
        $this->sessionStore = $sessionStore;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedFrom(): ?string
    {
        return $this->createdFrom;
    }

    public function setCreatedFrom(string $createdFrom): static
    {
        $this->createdFrom = $createdFrom;

        return $this;
    }

    public function getUpdatedFrom(): ?string
    {
        return $this->updatedFrom;
    }

    public function setUpdatedFrom(?string $updatedFrom): static
    {
        $this->updatedFrom = $updatedFrom;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): static
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCommentPrint(): ?string
    {
        return $this->commentPrint;
    }

    public function setCommentPrint(?string $commentPrint): static
    {
        $this->commentPrint = $commentPrint;

        return $this;
    }


    public function isIsVisibility(): ?bool
    {
        return $this->isVisibility;
    }

    public function setIsVisibility(bool $isVisibility): static
    {
        $this->isVisibility = $isVisibility;

        return $this;
    }
}
