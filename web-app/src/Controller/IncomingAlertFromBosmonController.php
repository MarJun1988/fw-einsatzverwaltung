<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\IncomingAlert;
use App\Repository\AlertAddressRepository;
use App\Repository\DepartmentRepository;
use App\Repository\FederalStateRepository;
use App\Repository\IncomingAlertRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IncomingAlertFromBosmonController extends AbstractController
{
    public function __construct(
        public EntityManagerInterface $entityManager,
        public AlertAddressRepository $alertAddressRepository,
        public FederalStateRepository $federalStateRepository,
        public DepartmentRepository   $departmentRepository,
        public IncomingAlertRepository $incomingAlertRepository
    )
    {
    }

    /**
     * @throws Exception
     */
    #[Route('/incoming-alert-from-bosmon', name: 'app_incoming_alert_from_bosmon', methods: ['POST'])]
    public function index(Request $request): Response
    {

//        if ($request->get('address') && $request->get('alertMessage')) {
        $address = $this->alertAddressRepository->findOneBy(['address' => $request->get('address')]);
        $federalState = $this->federalStateRepository->findOneBy(['abbreviation' => 'SN']);

        $department = match ($request->get('address')) {
            1864096 => $this->departmentRepository->findOneBy(['abbreviation' => 'Probealarm Woche']),
            1864097 => $this->departmentRepository->findOneBy(['abbreviation' => 'Probealarm Täglich']),
            1870048, 1870049 => $this->departmentRepository->findOneBy(['abbreviation' => 'FFw Lichtenwalde']),
            default => $this->departmentRepository->findOneBy(['abbreviation' => 'FFw Niederwiesa']),
        };
//
//        $all = $this->incomingAlertRepository->findAll();
//
//        $erros = 0;
//        if ($all) {
//            foreach ($all as $incomingAlert) {
//                // Alarmierung auf die einzelnen Felder aufsplitten
//                $splitMessage = [
//                    'numberControlCenter' => '',
//                    'keywordShort' => '',
//                    'keywordLong' => '',
//                    'location' => '',
//                    'otherAddressInformation' => '',
//                    'personPatient' => '',
//                    'text' => '',
//                    'isVisibility' => true,
//                    'isVisibilityOperationReport' => true,
//                    'messageComplete' => $request->get('alertMessage'),
//                ];
//
//                $defaultCharacters = "\w\s\d\ÄäÖöÜüß!?#.,;:\-\/\\()_<>=+ ";
//
//                // Regex ab 2024.04-30
//                $patternAfter = "/^(#\d*)([$defaultCharacters]*)\/([$defaultCharacters]*)([-])([$defaultCharacters]*)\/([$defaultCharacters]*)\/([$defaultCharacters]*)\/([$defaultCharacters]*)\/([\d. :]*)/";
//                $patternAfterTwo = "/^(\d*)([$defaultCharacters]*)\/([$defaultCharacters]*)([-])([$defaultCharacters]*)\/([$defaultCharacters]*)\/([$defaultCharacters]*)\/([$defaultCharacters]*)\/([\d. :]*)/";
//                // Regex vor dem 2024-04-30
//                $patternBefore = "/^(#\d*)([$defaultCharacters]*)\/([$defaultCharacters]*)\/([$defaultCharacters]*)([-])([$defaultCharacters]*)\/([$defaultCharacters]*)\/([$defaultCharacters]*)\/([\d. :]*)/";
//                $patternBeforeTwo = "/^(\d*)([$defaultCharacters]*)\/([$defaultCharacters]*)\/([$defaultCharacters]*)([-])([$defaultCharacters]*)\/([$defaultCharacters]*)\/([$defaultCharacters]*)\/([\d. :]*)/";
//                // Sonstige
//                $pattern = "/^(#\d*)([$defaultCharacters]*)\/([$defaultCharacters]*)([-])([$defaultCharacters]*)\/([$defaultCharacters]*)\/([$defaultCharacters]*)\/([\d. :]*)/";
//
//                if (preg_match($patternAfter, $incomingAlert->getMessageComplete(), $matches) || preg_match($patternAfterTwo, $incomingAlert->getMessageComplete(), $matches)) {
//                    // Wenn ein Match gefunden wird, durchlaufen Sie alle Matches und entfernen Sie unerwünschte Leerzeichen
//                    for ($i = 0, $iMax = count($matches); $i < $iMax; $i++) {
//                        $matches[$i] = trim($matches[$i]);
//                    }
//                    $splitMessage = [
//                        'numberControlCenter' => $matches[1],
//                        'keywordShort' => $matches[2],
//                        'keywordLong' => $matches[8],
//                        'location' => $matches[5],
//                        'otherAddressInformation' => $matches[6],
//                        'personPatient' => '',
//                        'text' => $matches[7],
//                        'isVisibility' => true,
//                        'isVisibilityOperationReport' => true,
//                        'messageComplete' => $incomingAlert->getMessageComplete(),
//                    ];
//                }
//                else if (preg_match($patternBefore, $incomingAlert->getMessageComplete(), $matches) || preg_match($patternBeforeTwo, $incomingAlert->getMessageComplete(), $matches)) {
//                    // Wenn ein Match gefunden wird, durchlaufen Sie alle Matches$patternBefore und entfernen Sie unerwünschte Leerzeichen
//                    for ($i = 0, $iMax = count($matches); $i < $iMax; $i++) {
//                        $matches[$i] = trim($matches[$i]);
//                    }
//                    $splitMessage = [
//                        'numberControlCenter' => $matches[1],
//                        'keywordShort' => $matches[2],
//                        'keywordLong' => $matches[3],
//                        'location' => $matches[6],
//                        'otherAddressInformation' => $matches[7],
//                        'personPatient' => '',
//                        'text' => $matches[8],
//                        'isVisibility' => true,
//                        'isVisibilityOperationReport' => true,
//                        'messageComplete' => $incomingAlert->getMessageComplete()
//                    ];
//                } else {
//                    if (!str_contains($incomingAlert->getMessageComplete(), "Automatischer")) {
//                        $erros++;
//                        $fileSystem = new Filesystem();
//                        $fileSystem->mkdir("Prod-Fehler");
//                        $fileSystem->touch("Prod-Fehler/IncomingAlert.txt");
//                        $fileSystem->appendToFile("Prod-Fehler/IncomingAlert.txt", "Es wurde kein Passendes RegEx gefunden: " . $incomingAlert->getMessageComplete() . "\n");
//                    }
//                }
//
//
//            }
//        }


        // Alarmierung auf die einzelnen Felder aufsplitten
        $splitMessage = [
            'numberControlCenter' => '',
            'keywordShort' => '',
            'keywordLong' => '',
            'location' => '',
            'otherAddressInformation' => '',
            'personPatient' => '',
            'text' => '',
            'isVisibility' => true,
            'isVisibilityOperationReport' => true,
            'messageComplete' => $request->get('alertMessage'),
        ];

        $defaultCharacters = "\w\s\d\ÄäÖöÜüß!?#.,;:\-\/\\()_<>=+ ";

        // Regex ab 2024.04-30
        $patternAfter = "/^(#\d*)([$defaultCharacters]*)\/([$defaultCharacters]*)([-])([$defaultCharacters]*)\/([$defaultCharacters]*)\/([$defaultCharacters]*)\/([$defaultCharacters]*)\/([\d. :]*)/";
        // Regex vor dem 2024-04-30
        $patternBefore = "/^(#\d*)([$defaultCharacters]*)\/([$defaultCharacters]*)\/([$defaultCharacters]*)([-])([$defaultCharacters]*)\/([$defaultCharacters]*)\/([$defaultCharacters]*)\/([\d. :]*)/";
        // Sonstige
        $pattern = "/^(#\d*)([$defaultCharacters]*)\/([$defaultCharacters]*)([-])([$defaultCharacters]*)\/([$defaultCharacters]*)\/([$defaultCharacters]*)\/([\d. :]*)/";

        if (preg_match($patternAfter, $request->get('alertMessage'), $matches)) {
            // Wenn ein Match gefunden wird, durchlaufen Sie alle Matches und entfernen Sie unerwünschte Leerzeichen
            for ($i = 0, $iMax = count($matches); $i < $iMax; $i++) {
                $matches[$i] = trim($matches[$i]);
            }
            $splitMessage = [
                'numberControlCenter' => $matches[1],
                'keywordShort' => $matches[2],
                'keywordLong' => $matches[8],
                'location' => $matches[5],
                'otherAddressInformation' => $matches[6],
                'personPatient' => '',
                'text' => $matches[7],
                'isVisibility' => true,
                'isVisibilityOperationReport' => true,
                'messageComplete' => $request->get('alertMessage'),
            ];
        } else if (preg_match($patternBefore, $request->get('alertMessage'), $matches)) {
            // Wenn ein Match gefunden wird, durchlaufen Sie alle Matches$patternBefore und entfernen Sie unerwünschte Leerzeichen
            for ($i = 0, $iMax = count($matches); $i < $iMax; $i++) {
                $matches[$i] = trim($matches[$i]);
            }
            $splitMessage = [
                'numberControlCenter' => $matches[1],
                'keywordShort' => $matches[2],
                'keywordLong' => $matches[3],
                'location' => $matches[6],
                'otherAddressInformation' => $matches[7],
                'personPatient' => '',
                'text' => $matches[8],
                'isVisibility' => true,
                'isVisibilityOperationReport' => true,
                'messageComplete' => $request->get('alertMessage'),
            ];
        } else {

            $fileSystem = new Filesystem();
            $fileSystem->mkdir("Prod-Fehler");
            $fileSystem->touch("Prod-Fehler/IncomingAlert.txt");
            $fileSystem->appendToFile("Prod-Fehler/IncomingAlert.txt", "Es wurde kein Passendes RegEx gefunden: " . $request->get('alertMessage') . "\n");
        }


//        $messageExplode = explode("/", $request->get('alertMessage'));
//
//        if (count($messageExplode) > 1) {
//            $explodeSectionOne = explode(' ', $messageExplode[0]);
//
//            // 1ter Abschnitt
//            if (count($explodeSectionOne) > 0) {
//                $splitMessage['numberControlCenter'] = $explodeSectionOne[0];
//                $splitMessage['keywordShort'] = $explodeSectionOne[1];
//            }
//            // 2ter Abschnitt
//            if (count($messageExplode) > 1) {
//                // ab 01.05.2024
//                $splitMessage['location'] = $messageExplode[1];
//
////                    $splitMessage['keywordLong'] = $messageExplode[1];
//            }
//            // 3ter Abschnitt
//            if (count($messageExplode) > 2) {
//                // ab 01.05.2024
//                $splitMessage['otherAddressInformation'] = $messageExplode[2];
//
////                    $splitMessage['location'] = $messageExplode[2];
//            }
//            // 4ter Abschnitt
//            if (count($messageExplode) > 3) {
//                // ab 01.05.2024
//                $splitMessage['personPatient'] = $messageExplode[3];
//
////                    $splitMessage['otherAddressInformation'] = $messageExplode[3];
//            }
//            // 5ter Abschnitt
//            if (count($messageExplode) > 4) {
//                // ab 01.05.2024
//                $splitMessage['keywordLong'] = $messageExplode[4];
//            }
//            // 5ter Abschnitt
//            if (count($messageExplode) > 5) {
//                $splitMessage['text'] = $messageExplode[5];
//            }
//
//        }

        if ($address && $federalState && $department) {
            $newAlert = new IncomingAlert();

            $newAlert
                ->setAddress($address)
                ->setFederalState($federalState)
                ->setDepartment($department)
                ->setAddress($address)
                ->setIncomingDateTime(new DateTimeImmutable())
                ->setNumberControlCenter(trim((string)$splitMessage['numberControlCenter']))
                ->setKeywordShort(trim((string)$splitMessage['keywordShort']))
                ->setKeywordLong(trim((string)$splitMessage['keywordLong']))
                ->setLocation(trim((string)$splitMessage['location']))
                ->setOtherAddressInformation(trim((string)$splitMessage['otherAddressInformation']))
                ->setPersonPatient(trim((string)$splitMessage['personPatient']))
                ->setText(trim((string)$splitMessage['text']) ?? 'BITTE NOCH AUSFÜLLEN')
                ->setIncomingDateTime(new DateTimeImmutable())
                ->setMessageComplete(trim((string)$splitMessage['messageComplete']))
                ->setIsVisibility($splitMessage['isVisibility'])
                ->setIsVisibilityOperationReport($splitMessage['isVisibilityOperationReport'])
                ->setCreatedFrom('BosMon Feuerwehr')
                ->setCreatedAt(new DateTimeImmutable());

            $this->entityManager->persist($newAlert);
            $this->entityManager->flush();

            return new JsonResponse(
                [
                    "message" => "Created Successfully",
                    "item" => $newAlert
                ],
                Response::HTTP_CREATED);


        }

//        }

        return new Response("No Params Found", Response::HTTP_BAD_REQUEST);

    }
}
