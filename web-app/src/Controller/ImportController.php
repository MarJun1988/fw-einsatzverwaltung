<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\AlertAddress;
use App\Entity\Department;
use App\Entity\FederalState;
use App\Entity\IncomingAlert;
use App\Entity\Member;
use App\Entity\MemberFunction;
use App\Entity\OperationReport;
use App\Entity\OperationReportDetail;
use App\Entity\OperationType;
use App\Entity\OperationTypeCategory;
use App\Entity\Tool;
use App\Entity\ToolCategory;
use App\Entity\User;
use App\Entity\UserRole;
use App\Entity\Vehicle;
use App\Entity\Version;
use App\Repository\AlertAddressRepository;
use App\Repository\DepartmentRepository;
use App\Repository\FederalStateRepository;
use App\Repository\IncomingAlertRepository;
use App\Repository\MemberFunctionRepository;
use App\Repository\MemberRepository;
use App\Repository\OperationTypeCategoryRepository;
use App\Repository\OperationTypeRepository;
use App\Repository\ToolCategoryRepository;
use App\Repository\ToolRepository;
use App\Repository\UserRepository;
use App\Repository\UserRoleRepository;
use App\Repository\VehicleRepository;
use App\Repository\VersionRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use JsonException;
use PDO;
use PHPUnit\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ImportController extends AbstractController
{
    /**
     * @throws JsonException
     * @throws \Exception
     */
    #[Route('/import', name: 'app_import')]
    public function index(EntityManagerInterface          $entityManager,
                          ValidatorInterface              $validator,
                          VersionRepository               $versionRepository,
                          FederalStateRepository          $federalStateRepository,
                          DepartmentRepository            $departmentRepository,
                          AlertAddressRepository          $alertAddressRepository,
                          IncomingAlertRepository         $incomingAlertRepository,
                          MemberFunctionRepository        $memberFunctionRepository,
                          MemberRepository                $memberRepository,
                          ToolCategoryRepository          $toolCategoryRepository,
                          ToolRepository                  $toolRepository,
                          VehicleRepository               $vehicleRepository,
                          OperationTypeCategoryRepository $operationTypeCategoryRepository,
                          OperationTypeRepository         $operationTypeRepository,
                          UserRoleRepository              $userRoleRepository,
                          UserRepository                  $userRepository
    ): Response
    {

        $fileSystem = new Filesystem();
        $fileVersions = "../assets/database-basic-data/versions.json";
        $fileFederalStates = "../assets/database-basic-data/federal-states.json";
        $fileDepartments = "../assets/database-basic-data/departments.json";
        $fileAlertAddresses = "../assets/database-basic-data/alert-addresses.json";
        $fileIncomingAlerts = "../assets/database-basic-data/incoming-alerts.json";
        $fileMemberFunctions = "../assets/database-basic-data/member-functions.json";
        $fileMembers = "../assets/database-basic-data/members.json";
        $fileToolCategories = "../assets/database-basic-data/tool-categories.json";
        $fileTools = "../assets/database-basic-data/tools.json";
        $fileVehicles = "../assets/database-basic-data/vehicles.json";
        $fileOperationTypeCategories = "../assets/database-basic-data/operation-type-category.json";
        $fileOperationTypes = "../assets/database-basic-data/operation-type.json";
        $fileUserRoles = "../assets/database-basic-data/user-roles.json";
        $fileUsers = "../assets/database-basic-data/users.json";

        ini_set('max_execution_time', 300);

        // Versionen
        if ($fileSystem->exists($fileVersions) && $versionRepository->count([]) === 0) {
            $jsonVersions = json_decode(file_get_contents($fileVersions), true, 512, JSON_THROW_ON_ERROR);

            if (count($jsonVersions) > 0) {
                foreach ($jsonVersions as $version) {
                    if (array_keys($version)[0] !== "_comment") {
                        $newVersion = (new Version())
                            ->setNumber($version['number'])
                            ->setBuildNumber($version['build_number'])
                            ->setDescription($version['description'])
                            ->setComment($version['comment'] ?? null)
                            ->setIsVisibility(true)
                            ->setCreatedFrom('System Nutzer')
                            ->setCreatedAt(new DateTimeImmutable());

                        $entityManager->persist($newVersion);

                        if (($result = $validator->validate($newVersion))->count() === 0) {
                            $entityManager->flush();
                        } else {
                            // TODO: Get Error Message
                        }
                    }
                }
            }
        }

        // Bundesländer
        if ($fileSystem->exists($fileFederalStates) && $federalStateRepository->count([]) === 0) {
            $jsonFederalStates = json_decode(file_get_contents($fileFederalStates), true, 512, JSON_THROW_ON_ERROR);

            if (count($jsonFederalStates) > 0) {
                foreach ($jsonFederalStates as $federalState) {
                    if (array_keys($federalState)[0] !== "_comment") {
                        $newFederalState = (new FederalState())
                            ->setAbbreviation($federalState['abbreviation'])
                            ->setDescription($federalState['description'])
                            ->setIsVisibility(true)
                            ->setComment($federalState['comment'] ?? null)
                            ->setCreatedFrom('System Nutzer')
                            ->setCreatedAt(new DateTimeImmutable());

                        $entityManager->persist($newFederalState);

                        if (($result = $validator->validate($newFederalState))->count() === 0) {
                            $entityManager->flush();
                        } else {
                            // TODO: Get Error Message
                        }
                    }
                }
            }
        }

        // Dienststellen
        if ($fileSystem->exists($fileDepartments) && $departmentRepository->count([]) === 0) {
            $jsonDepartment = json_decode(file_get_contents($fileDepartments), true, 512, JSON_THROW_ON_ERROR);
            if (count($jsonDepartment) > 0) {
                foreach ($jsonDepartment as $department) {
                    if (array_keys($department)[0] !== "_comment") {

                        $federalState = $department['federal_state_id'] ? $federalStateRepository->findOneBy(['abbreviation' => $department['federal_state_id']]) : $memberFunctionRepository->findOneBy(['abbreviation' => 'SN']);

                        $newDepartment = (new Department())
                            ->setAbbreviation($department['abbreviation'])
                            ->setDescription($department['description'])
                            ->setFederalState($federalState)
                            ->setZipCode((string)$department['zip_code'])
                            ->setPlaceName((string)$department['place_name'])
                            ->setStreet((string)$department['street'])
                            ->setIsVisibility(true)
                            ->setComment($department['comment'] ?? null)
                            ->setCreatedFrom('System Nutzer')
                            ->setCreatedAt(new DateTimeImmutable());

                        $entityManager->persist($newDepartment);

                        if (($result = $validator->validate($newDepartment))->count() === 0) {
                            $entityManager->flush();
                        } else {
                            // TODO: Get Error Message
                            dd($validator->validate($newDepartment));
                        }
                    }
                }
            }
        }

        // Rics
        if ($fileSystem->exists($fileAlertAddresses) && $alertAddressRepository->count([]) === 0) {
            $jsonAlertAddresses = json_decode(file_get_contents($fileAlertAddresses), true, 512, JSON_THROW_ON_ERROR);
            if (count($jsonAlertAddresses) > 0) {
                foreach ($jsonAlertAddresses as $alertAddress) {
                    if (array_keys($alertAddress)[0] !== "_comment") {

                        $federalState = $alertAddress['federal_state_id'] ? $federalStateRepository->findOneBy(['abbreviation' => $alertAddress['federal_state_id']]) : $federalStateRepository->findOneBy(['abbreviation' => 'SN']);
                        $department = $alertAddress['department_id'] ? $departmentRepository->findOneBy(['abbreviation' => $alertAddress['department_id']]) : $departmentRepository->findOneBy(['abbreviation' => 'Fw Niederwiesa']);

                        $newAlertAddress = (new AlertAddress())
                            ->setFederalState($federalState)
                            ->setDepartment($department)
                            ->setAddress($alertAddress['address'])
                            ->setDescription($alertAddress['description'])
                            ->setIsVisibility(true)
                            ->setComment($alertAddress['comment'] ?? null)
                            ->setCreatedFrom('System Nutzer')
                            ->setCreatedAt(new DateTimeImmutable());

                        $entityManager->persist($newAlertAddress);

                        if (($result = $validator->validate($newAlertAddress))->count() === 0) {
                            $entityManager->flush();
                        } else {
                            // TODO: Get Error Message
                        }
                    }
                }
            }
        }

        // Alarmierungen
        if ($fileSystem->exists($fileIncomingAlerts) && $incomingAlertRepository->count([]) === 0) {
            $jsonIncomingAlerts = json_decode(file_get_contents($fileIncomingAlerts), true, 512, JSON_THROW_ON_ERROR);
            if (count($jsonIncomingAlerts) > 0) {
                foreach ($jsonIncomingAlerts as $incomingAlert) {
                    if (array_keys($incomingAlert)[0] !== "_comment") {

                        $messageComplete = (string)$incomingAlert['text'];
                        $numberControlCenter = "";
                        $keywordShort = "";
                        $keywordLong = "";
                        $location = "";
                        $otherAddressInformation = "";
                        $personPatient = "";
                        $text = "";
                        $alertAddress = null;

                        if ($incomingAlert['id'] && $incomingAlert['id'] < 56) {

                            $textSplit = explode('/', $messageComplete);

                            if ($textSplit > 0) {
                                $keywordShort = count($textSplit) > 0 ? $textSplit[0] : '';
                                $keywordLong = count($textSplit) > 1 ? $textSplit[1] : '';
                                $text = count($textSplit) > 2 ? $textSplit[2] : '';
                                $location = count($textSplit) > 3 ? $textSplit[3] : '';
                                $location .= count($textSplit) > 4 ? $textSplit[4] : '';
                                $otherAddressInformation = count($textSplit) > 5 ? $textSplit[5] : '';
                                $numberControlCenter = count($textSplit) > 7 ? $textSplit[7] : '';
                            }
                        }

                        // Aktueller Standard für Alarmierungen
                        if ($incomingAlert['id'] && $incomingAlert['id'] > 56) {

                            $textSplit = explode('/', $messageComplete);

                            if ($textSplit > 0) {
                                $textSplitOne = explode(' ', $textSplit[0]);
                                $numberControlCenter = $textSplitOne[0];
                                $keywordShort = count($textSplitOne) > 1 ? $textSplitOne[1] : '';
                                $keywordLong = count($textSplit) > 1 ? $textSplit[1] : '';

                                $location = count($textSplit) > 2 ? $textSplit[2] : '';
                                $otherAddressInformation = count($textSplit) > 3 ? $textSplit[3] : '';
                                $personPatient = count($textSplit) > 4 ? $textSplit[4] : '';
                                $text = count($textSplit) > 5 ? $textSplit[5] : '';
                            }
                        }
                        $federalState = $federalStateRepository->findOneBy(['abbreviation' => 'SN']);

                        $department = match ($incomingAlert['address_id']) {
                            1864096 => $departmentRepository->findOneBy(['abbreviation' => 'Probealarm Woche']),
                            1864097 => $departmentRepository->findOneBy(['abbreviation' => 'Probealarm Täglich']),
                            1870048, 1870049 => $departmentRepository->findOneBy(['abbreviation' => 'Fw Lichtenwalde']),
                            default => $departmentRepository->findOneBy(['abbreviation' => 'Fw Niederwiesa']),
                        };

                        if ($incomingAlert['address_id']) {
                            $addressNumber = null;
                            match ($incomingAlert['address_id']) {
                                1 => $addressNumber = 1864097,
                                2 => $addressNumber = 1864096,
                                3 => $addressNumber = 1870064,
                                4 => $addressNumber = 1870071,
                                5 => $addressNumber = 1870048,
                                6 => $addressNumber = 1870075,
                                7 => $addressNumber = 1870066,
                                8 => $addressNumber = 1890745,
                                9 => $addressNumber = 1873065,
                                10 => $addressNumber = 1873066,
                                11 => $addressNumber = 1873064,
                                12 => $addressNumber = 1870049,
                                13 => $addressNumber = 1870065,
                                14 => $addressNumber = 1234567,
                            };

                            $alertAddress = $alertAddressRepository->findOneBy(['address' => $addressNumber]);
                        }

                        $criteria = [
                            'federalState' => $federalState,
                            'department' => $department,
                            'incomingDateTime' => new DateTimeImmutable((string)$incomingAlert['incoming_date_time']),
                            'messageComplete' => $messageComplete
                        ];

                        $existAlert = $incomingAlertRepository->findOneBy($criteria);

                        if (!$existAlert && $alertAddress) {
                            $newIncomingAlert = (new IncomingAlert())
                                ->setAddress($alertAddress)
                                ->setFederalState($federalState)
                                ->setDepartment($department)
                                ->setIncomingDateTime(new DateTimeImmutable((string)$incomingAlert['incoming_date_time']))
                                ->setComment('Import - ' . $incomingAlert['id'])
                                ->setMessageComplete($messageComplete)
                                ->setNumberControlCenter($numberControlCenter)
                                ->setKeywordShort(strlen($keywordShort) < 50 ? $keywordShort : "")
                                ->setKeywordLong($keywordLong)
                                ->setLocation($location)
                                ->setOtherAddressInformation($otherAddressInformation)
                                ->setPersonPatient($personPatient)
                                ->setText($text)
                                ->setIsVisibility(true)
                                ->setIsVisibilityOperationReport(true)
                                ->setComment($incomingAlert['comment'] ?? null)
                                ->setCreatedFrom('System Nutzer')
                                ->setCreatedAt(new DateTimeImmutable((string)$incomingAlert['incoming_date_time']));

                            $entityManager->persist($newIncomingAlert);

                            if (($result = $validator->validate($newIncomingAlert))->count() === 0) {
//                            if (  $incomingAlert['id'] == 85) {
//                                dd($newIncomingAlert);
//                            }

                                $entityManager->flush();
                            } else {
                                dd($incomingAlert);
                                dd($validator->validate($newIncomingAlert));
                                // TODO: Get Error Message
                            }
                        }
                    }
                }
            }
        }

        // Mitglieder Funktion
        if ($fileSystem->exists($fileMemberFunctions) && $memberFunctionRepository->count([]) === 0) {
            $jsonMemberFunctions = json_decode(file_get_contents($fileMemberFunctions), true, 512, JSON_THROW_ON_ERROR);
            if (count($jsonMemberFunctions) > 0) {
                foreach ($jsonMemberFunctions as $memberFunction) {
                    if (array_keys($memberFunction)[0] !== "_comment") {
//                        $department = $memberFunction && $memberFunction['department_id'] ? $departmentRepository->findOneBy(['abbreviation' => $memberFunction['department_id']]) : $departmentRepository->findOneBy(['abbreviation' => 'Fw Niederwiesa']);
                        $department = $departmentRepository->findOneBy(['abbreviation' => 'Fw Niederwiesa']);
                        $newMemberFunction = (new MemberFunction())
                            ->setDepartment($department)
                            ->setAbbreviation($memberFunction['abbreviation'])
                            ->setDescription($memberFunction['description'])
                            ->setSorting((int)$memberFunction['sorting'])
                            ->setIsVisibility((bool)$memberFunction['is_visibility'])
                            ->setIsVisibilityOperationReport((bool)$memberFunction['is_visibility_operation_report'])
                            ->setComment($memberFunction['comment'] ?? null)
                            ->setCreatedFrom('System Nutzer')
                            ->setCreatedAt(new DateTimeImmutable());

                        $entityManager->persist($newMemberFunction);

                        if (($result = $validator->validate($newMemberFunction))->count() === 0) {
                            $entityManager->flush();
                        } else {
                            // TODO: Get Error Message
                        }
                    }
                }
            }
        }

        // Mitglieder
        if ($fileSystem->exists($fileMembers) && $memberRepository->count([]) === 0) {
            $jsonMember = json_decode(file_get_contents($fileMembers), true, 512, JSON_THROW_ON_ERROR);
            if (count($jsonMember) > 0) {
                foreach ($jsonMember as $member) {
                    if (array_keys($member)[0] !== "_comment") {

                        $federalState = $member['federalstate_id'] ? $federalStateRepository->findOneBy(['abbreviation' => $member['federalstate_id']]) : $memberFunctionRepository->findOneBy(['abbreviation' => 'SN']);
                        $department = $member['department_id'] ? $departmentRepository->findOneBy(['abbreviation' => $member['department_id']]) : $memberFunctionRepository->findOneBy(['abbreviation' => 'Fw Niederwiesa']);
                        $defaultMemberFunction = $member['default_function_id'] ? $memberFunctionRepository->findOneBy(['abbreviation' => $member['default_function_id']]) : $memberFunctionRepository->findOneBy(['abbreviation' => 'FM']);

                        if ($federalState && $department && $defaultMemberFunction) {

                            $newMember = (new Member())
                                ->setFederalState($federalState)
                                ->setDepartment($department)
                                ->setSurname((string)$member['surname'])
                                ->setFirstname((string)$member['firstname'])
                                ->setDefaultMemberFunction($defaultMemberFunction)
                                ->setIsVisibility(true)
                                ->setIsVisibilityOperationReport(true)
                                ->setComment($member['comment'] ?? null)
                                ->setCreatedFrom('System Nutzer')
                                ->setCreatedAt(new DateTimeImmutable());

                            $entityManager->persist($newMember);

                            if (($result = $validator->validate($newMember))->count() === 0) {
                                $entityManager->flush();
                            } else {
                                dd($member);
                                // TODO: Get Error Message
                            }
                        }
                    }
                }
            }
        }

        // Hilfsmittel Kategorien
        if ($fileSystem->exists($fileToolCategories) && $toolCategoryRepository->count([]) === 0) {
            $jsonToolCategories = json_decode(file_get_contents($fileToolCategories), true, 512, JSON_THROW_ON_ERROR);
            if (count($jsonToolCategories) > 0) {
                foreach ($jsonToolCategories as $toolCategory) {
                    if (array_keys($toolCategory)[0] !== "_comment") {

                        $department = $toolCategory['department_id'] ? $departmentRepository->findOneBy(['abbreviation' => $toolCategory['department_id']]) : $departmentRepository->findOneBy(['abbreviation' => 'Fw Niederwiesa']);

                        $newToolCategory = (new ToolCategory())
                            ->setDepartment($department)
                            ->setAbbreviation((string)$toolCategory['abbreviation'])
                            ->setDescription((string)$toolCategory['description'])
                            ->setSorting((int)$toolCategory['sorting'])
                            ->setIsVisibility(true)
                            ->setIsVisibilityOperationReport(true)
                            ->setComment($toolCategory['comment'] ?? null)
                            ->setCreatedFrom('System Nutzer')
                            ->setCreatedAt(new DateTimeImmutable());


                        $entityManager->persist($newToolCategory);

                        if (($result = $validator->validate($newToolCategory))->count() === 0) {
                            $entityManager->flush();
                        } else {
                            // TODO: Get Error Message
                            dd($validator->validate($newToolCategory));
                        }
                    }
                }
            }
        }

        // Hilfsmittel
        if ($fileSystem->exists($fileTools) && $toolRepository->count([]) === 0) {
            $jsonTools = json_decode(file_get_contents($fileTools), true, 512, JSON_THROW_ON_ERROR);
            if (count($jsonTools) > 0) {
                foreach ($jsonTools as $tool) {
                    if (array_keys($tool)[0] !== "_comment") {

                        $toolCategory = $tool['tool_category_id'] ? $toolCategoryRepository->findOneBy(['abbreviation' => $tool['tool_category_id']]) : $toolCategoryRepository->findOneBy(['abbreviation' => 'Sonstiges']);
                        $department = $tool['department_id'] ? $departmentRepository->findOneBy(['abbreviation' => $tool['department_id']]) : $departmentRepository->findOneBy(['abbreviation' => 'Fw Niederwiesa']);

                        $newTool = (new Tool())
                            ->setDepartment($department)
                            ->setToolCategory($toolCategory)
                            ->setAbbreviation((string)$tool['abbreviation'])
                            ->setDescription((string)$tool['description'])
                            ->setIsVisibility(true)
                            ->setIsVisibilityOperationReport(true)
                            ->setComment($tool['comment'] ?? null)
                            ->setCreatedFrom('System Nutzer')
                            ->setCreatedAt(new DateTimeImmutable());

                        $entityManager->persist($newTool);

                        if (($result = $validator->validate($newTool))->count() === 0) {
                            $entityManager->flush();
                        } else {
                            // TODO: Get Error Message
                        }
                    }
                }
            }
        }

        // Fahrzeuge
        if ($fileSystem->exists($fileVehicles) && $vehicleRepository->count([]) === 0) {
            $jsonVehicles = json_decode(file_get_contents($fileVehicles), true, 512, JSON_THROW_ON_ERROR);
            if (count($jsonVehicles) > 0) {
                foreach ($jsonVehicles as $vehicle) {
                    if (array_keys($vehicle)[0] !== "_comment") {

                        $federalState = $vehicle['federal_state_id'] ? $federalStateRepository->findOneBy(['abbreviation' => $vehicle['federal_state_id']]) : $federalStateRepository->findOneBy(['abbreviation' => 'SN']);
                        $department = $vehicle['department_id'] ? $departmentRepository->findOneBy(['abbreviation' => $vehicle['department_id']]) : $departmentRepository->findOneBy(['abbreviation' => 'Fw Niederwiesa']);

                        $newVehicle = (new Vehicle())
                            ->setFederalState($federalState)
                            ->setDepartment($department)
                            ->setAbbreviation((string)$vehicle['abbreviation'])
                            ->setDescription((string)$vehicle['description'])
                            ->setMaxSeats((int)$vehicle['max_seats'])
                            ->setNumberplate((string)$vehicle['numberplate'])
                            ->setCost((float)$vehicle['cost'])
                            ->setFurtherInformation((string)$vehicle['further_information'])
                            ->setIsVisibility(true)
                            ->setIsVisibilityOperationReport(true)
                            ->setComment($vehicle['comment'] ?? null)
                            ->setCreatedFrom('System Nutzer')
                            ->setCreatedAt(new DateTimeImmutable());

                        $entityManager->persist($newVehicle);

                        if (($result = $validator->validate($newVehicle))->count() === 0) {
                            $entityManager->flush();
                        } else {
                            // TODO: Get Error Message
                        }
                    }
                }
            }
        }

        // Einsatzkategorien Arten
        if ($fileSystem->exists($fileOperationTypeCategories) && $operationTypeCategoryRepository->count([]) === 0) {
            $jsonOperationTypeCategories = json_decode(file_get_contents($fileOperationTypeCategories), true, 512, JSON_THROW_ON_ERROR);
            if (count($jsonOperationTypeCategories) > 0) {
                foreach ($jsonOperationTypeCategories as $operationTypeCategory) {
                    if (array_keys($operationTypeCategory)[0] !== "_comment") {

                        $department = $operationTypeCategory['department'] ? $departmentRepository->findOneBy(['abbreviation' => $operationTypeCategory['department']]) : $departmentRepository->findOneBy(['abbreviation' => 'Fw Niederwiesa']);

                        $newOperationTypeCategory = (new OperationTypeCategory())
                            ->setDepartment($department)
                            ->setAbbreviation((string)$operationTypeCategory['abbreviation'])
                            ->setDescription((string)$operationTypeCategory['description'])
                            ->setSorting((int)$operationTypeCategory['sorting'])
                            ->setIsVisibility(true)
                            ->setComment($operationTypeCategory['comment'] ?? null)
                            ->setCreatedFrom('System Nutzer')
                            ->setCreatedAt(new DateTimeImmutable());

                        $entityManager->persist($newOperationTypeCategory);

                        if (($result = $validator->validate($newOperationTypeCategory))->count() === 0) {
                            $entityManager->flush();
                        } else {
                            // TODO: Get Error Message
                        }
                    }
                }
            }
        }

        // Einsatz Arten
        if ($fileSystem->exists($fileOperationTypes) && $operationTypeRepository->count([]) === 0) {
            $jsonOperationTypes = json_decode(file_get_contents($fileOperationTypes), true, 512, JSON_THROW_ON_ERROR);
            if (count($jsonOperationTypes) > 0) {
                foreach ($jsonOperationTypes as $operationType) {
                    if (array_keys($operationType)[0] !== "_comment") {

                        $department = $operationType['department'] ? $departmentRepository->findOneBy(['abbreviation' => $operationType['department']]) : $departmentRepository->findOneBy(['abbreviation' => 'Fw Niederwiesa']);
                        $category = $operationType['operationTypeCategory'] ? $operationTypeCategoryRepository->findOneBy(['abbreviation' => $operationType['operationTypeCategory']]) : null;

                        $newOperationType = (new OperationType())
                            ->setDepartment($department)
                            ->setOperationTypeCategory($category)
                            ->setAbbreviation((string)$operationType['abbreviation'])
                            ->setDescription((string)$operationType['description'])
                            ->setSorting((int)$operationType['sorting'])
                            ->setIsVisibility(true)
                            ->setComment($operationType['comment'] ?? null)
                            ->setCreatedFrom('System Nutzer')
                            ->setCreatedAt(new DateTimeImmutable());

                        $entityManager->persist($newOperationType);

                        if (($result = $validator->validate($newOperationType))->count() === 0) {
                            $entityManager->flush();
                        } else {
                            dd($operationType);
                            // TODO: Get Error Message
                        }
                    }
                }
            }
        }

        // Nutzerrollen
        if ($fileSystem->exists($fileUserRoles) && $userRoleRepository->count([]) === 0) {
            $jsonUserRoles = json_decode(file_get_contents($fileUserRoles), true, 512, JSON_THROW_ON_ERROR);
            if (count($jsonUserRoles) > 0) {
                foreach ($jsonUserRoles as $userRole) {
                    if (array_keys($userRole)[0] !== "_comment") {

                        $newUserRole = (new UserRole())
                            ->setDisplayName((string)$userRole['displayName'])
                            ->setRoleName((string)$userRole['roleName'])
                            ->setDescription((string)$userRole['description'])
                            ->setSorting((int)$userRole['sorting'])
                            ->setIsVisibility(true)
                            ->setComment($userRole['comment'] ?? null)
                            ->setCreatedFrom('System Nutzer')
                            ->setCreatedAt(new DateTimeImmutable());

                        $entityManager->persist($newUserRole);

                        if (($result = $validator->validate($newUserRole))->count() === 0) {
                            $entityManager->flush();
                        } else {
                            // TODO: Get Error Message
                        }
                    }
                }
            }
        }

        // Nutzer
        if ($fileSystem->exists($fileUsers) && $userRepository->count([]) === 0) {
            $jsonUsers = json_decode(file_get_contents($fileUsers), true, 512, JSON_THROW_ON_ERROR);
            if (count($jsonUsers) > 0) {
                foreach ($jsonUsers as $user) {
                    if (array_keys($user)[0] !== "_comment") {

                        $federalState = $federalStateRepository->findOneBy(['abbreviation' => $user['federal_state_id']]);
                        $department = $departmentRepository->findOneBy(['abbreviation' => $user['department_id']]);

                        $newUser = (new User())
                            ->setFederalState($federalState)
                            ->setDepartment($department)
                            ->setSurname((string)$user['surname'])
                            ->setFirstname((string)$user['firstname'])
                            ->setEmail((string)$user['email'])
                            ->setPassword((string)$user['password'])
                            ->setRoles((array)$user['roles'])
                            ->setIsVisibility(true)
                            ->setComment($user['comment'] ?? null)
                            ->setCreatedFrom('System Nutzer')
                            ->setCreatedAt(new DateTimeImmutable());

                        $entityManager->persist($newUser);

                        if (($result = $validator->validate($newUser))->count() === 0) {
                            $entityManager->flush();
                        } else {
                            // TODO: Get Error Message
                        }
                    }
                }
            }
        }

        return $this->render('import/index.html.twig', [
            'controller_name' => 'ImportController',
        ]);
    }

    /**
     * @throws JsonException
     * @throws \Exception
     */
    #[Route('/import-old', name: 'app_import_old')]
    public function import(
        EntityManagerInterface   $entityManager,
        FederalStateRepository   $federalStateRepository,
        DepartmentRepository     $departmentRepository,
        AlertAddressRepository   $alertAddressRepository,
        IncomingAlertRepository  $incomingAlertRepository,
        MemberFunctionRepository $memberFunctionRepository,
        MemberRepository         $memberRepository,
        ToolRepository           $toolRepository,
        VehicleRepository        $vehicleRepository,
        OperationTypeRepository  $operationTypeRepository
    ): Response
    {
        ini_set('max_execution_time', '600'); //600 seconds = 10 minutes
        ini_set('memory_limit', '1024M');

        $fileSystem = new Filesystem();

        // Optionen für SQL
        $options = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        );

        try {
            $dbHost = '127.0.0.1';
            $dbUser = 'root';
            $dbPass = '1i6ra5rskTBVN#t8&?#4';
            $dbh = new PDO("mysql:host=$dbHost;dbname=fw-operation-management-old", $dbUser, $dbPass, $options);
            $stmt = $dbh->query(
                "select dR.*, dRD.*, aI.* , aA.*, dRT.* from `fw-operation-management-old`.deployment_reports dR 
                    left join `fw-operation-management-old`.deployment_report_details dRD on dR.detail_id = dRD.id
                    left join `fw-operation-management-old`.alert_incomings aI on aI.id = dRD.alert_incoming_id
                    left join `fw-operation-management-old`.alert_addresses aA on aI.address_id = aA.id
                    left join `fw-operation-management-old`.deployment_report_types dRT on dRD.deployment_type_id = dRT.id
                    order by dR.id asc;
"
            );

            $rows = $stmt->fetchAll();

            foreach ($rows as $row) {

                $newOperationReport = new OperationReport();
                $newOwnForces = [
                    "members" => [],
                    "vehicles" => [],
                    "vehiclesWithMemberAndFunction" => []
                ];
                $newOtherTaskForces = [];
                $newTools = [
                    "allTools" => [],
                    "toolsWithNumbers" => [],
                ];
                $newOperationReportDetail = new OperationReportDetail();

                // Alle Spalten
                $item = [
                    // Report
//                    "idReport" => $row['id'],
//                    "federalState" => $row['federalstate_id'],
//                    "department" => $row['department_id'],
//                    "detailId" => $row['detail_id'],
                    "ownForces" => $row['own_forces'],
                    "otherTaskForces" => $row['other_task_forces'],
                    "tools" => $row['tools'],
//                    "comment" => $row['comment'],
                    "createdAt" => $row['created_at'],
                    "updatedAt" => $row['updated_at'],
//                    "sessionStore" => $row['session_store'],
                    // Report Details
//                    "idReportDetail" => $row[12],
//                    "alertIncomingId" => $row['alert_incoming_id'],
//                    "alertAddressId" => $row['alert_address_id'],
                    "deploymentStart" => $row['deployment_start'],
                    "deploymentEnd" => $row['deployment_end'],
                    "keywordShort" => $row['keyword_short'],
                    "keywordLong" => $row['keyword_long'],
                    "text" => $row['text'],
                    "location" => $row['location'],
                    "postalCode" => $row['postal_code'],
                    "streetWithNumber" => $row['street_with_number'],
//                    "furtherInformation" => $row['further_information'],
                    "numberFireDepartment" => $row['number_fire_department'],
                    "numberPolice" => $row['number_police'],
                    "numberControlCenter" => $row['number_control_center'],
                    "commentDetail" => $row[28],
//                    "createdAtDetail" => $row[29],
//                    "updatedAtDetail" => $row[30],
                    // Incoming Alert
//                    "idAlert" => $row[31],
//                    "idAddressAlert" => $row['address_id'],
                    "textAlert" => $row[33],
                    "incomingDateTime" => $row['incoming_date_time'],
//                    "commentAlert" => $row[35],
//                    "createdAtAlert" => $row[36],
//                    "updatedAtAlert" => $row[37],
//                    "existOperationReport" => $row['exists_operation_report'],
                    // Address
                    "address" => $row['address'],
//                    "description" => $row['description'],
                    // Einsatzart
//                    "operationTypeName" => $row[47],
                    "operationTypeDescription" => $row[48],
                ];

                // Alarmierung aus der Datenbank finden
//                $newAlert = $incomingAlertRepository->findOneBy([
//                    'incomingDateTime' => new DateTimeImmutable($item['incomingDateTime']),
////                    'messageComplete' => $item['textAlert'],
//                    'address' => $alertAddressRepository->findOneBy([
//                        'address' => $item['address']])
//                ]);

                $params = [
                    "address" => $alertAddressRepository->findOneBy(['address' => $item['address']]),
                    "incomingDateTime" => $item['incomingDateTime'],
                    "messageComplete" => $item['textAlert']
                ];

                $newAlert = $incomingAlertRepository->findForTheImport($params);

//                dd($newAlert);
                // Wurde eine Alarmierung gefunden?
                if ($newAlert) {
                    // Einsatzbericht Anlegen, mit dem Grunddaten
                    $newOperationReport
                        ->setFederalState($federalStateRepository->findOneBy(['abbreviation' => "SN"]))
                        ->setDepartment($departmentRepository->findOneBy(['abbreviation' => "Fw Niederwiesa"]))
                        ->setOperationReportDetail($newOperationReportDetail)
                        ->setIncomingAlert($newAlert[0]);

                    // eigene Kräfte
                    $newIndex = 0;
                    if ($item['ownForces'] && count(json_decode($item['ownForces'], true, 512, JSON_THROW_ON_ERROR)) > 0) {
                        foreach (json_decode($item['ownForces'], true, 512, JSON_THROW_ON_ERROR) as $index => $ownForce) {
                            // Fahrzeug
                            $vehicle = $ownForce['vehicle'];
                            // Kameraden Bezeichnung
                            if (!array_key_exists("member", $ownForce)) {
                                $member = $ownForce['members'];
                            } else {
                                $member = $ownForce['member'];
                            }
                            // Fahrzeug Finden
                            $newVehicle = $vehicleRepository->findOneBy(['abbreviation' => $vehicle]);
                            // Wurde ein Fahrzeug gefunden?
                            if (!$newVehicle) {
                                dd("Keine Fahrzeug gefunden. " . $vehicle);
                            }
                            // Hinzufügen eines Fahrzeuges
                            if (!in_array($newVehicle->getId(), $newOwnForces['vehicles'], true)) {
                                if (count($newOwnForces['vehicles']) > 0) {
                                    $newIndex++;
                                }

                                // Neues Element
                                $newOwnForces['members'][] = [
                                    // ID des Fahrzeuges
                                    "id" => $newVehicle->getId(),
                                    "members" => [],
                                    "maxSeats" => $newVehicle->getMaxSeats(),
                                    "description" => $newVehicle->getDescription(),
                                    "abbreviation" => $newVehicle->getAbbreviation(),
                                ];

                                // Fahrzeuge Alleine
                                $newOwnForces['vehicles'][] = $newVehicle->getId();

                                // Neues Element, Fahrzeuge mit Kameraden und Funktionen
                                $newOwnForces['vehiclesWithMemberAndFunction'][] = [
                                    // ID des Fahrzeuges
                                    "id" => $newVehicle->getId(),
                                    "members" => [],
                                    "maxSeats" => $newVehicle->getMaxSeats(),
                                    "description" => $newVehicle->getDescription(),
                                    "abbreviation" => $newVehicle->getAbbreviation(),
                                ];
                            }

                            // Durchlaufen aller Kameraden, die im Einsatz dabei waren
                            if ($member && array_key_exists("displayName", $member)) {
                                // Stimmt noch nicht, mit dem INDEX

                                // NEU
                                // Name der Person
                                $displayName = $member['displayName'];
                                $nameExplode = explode(",", $displayName);
                                // Beschreibung der ausgewählten Funktion
                                $selectedFunctionDescription = $member['selectedFunction']['description'];

                                // Funktion finden
                                $findFunction = $memberFunctionRepository->findOneBy(['description' => $selectedFunctionDescription]);
                                // Wurde ein Funktion gefunden?
                                if (!$findFunction) {
                                    dd("Keine Funktion des Kamerades gefunden. " . $selectedFunctionDescription);
                                }

                                // Name finden in der Datenbank
                                $findMember = $memberRepository->findOneBy(['firstname' => trim($nameExplode[1]), 'surname' => trim($nameExplode[0])]);
                                // Wurde ein Kamerad gefunden?
                                if (!$findMember) {
                                    dd("Foglenden Kamaraden nicht gefunden: " . $displayName);
                                }

                                // Hinzufügen zum neuen Einsatz
                                if (!in_array($findMember->getId(), $newOwnForces['members'][$newIndex]['members'])) {
                                    $newOwnForces['members'][$newIndex]['members'][] = $findMember->getId();
                                    $newOwnForces['vehiclesWithMemberAndFunction'][$newIndex]['members'][] = [
                                        "id" => $findMember->getId(),
                                        "surname" => $findMember->getSurname(),
                                        "firstname" => $findMember->getFirstname(),
                                        "operationFunction" => $findFunction->getId(),
                                        "operationFunctionDetails" => [
                                            "id" => $findFunction->getId(),
                                            "sorting" => $findFunction->getSorting(),
                                            "description" => $findFunction->getDescription(),
                                            "abbreviation" => $findFunction->getAbbreviation(),
                                        ]
                                    ];
                                }

                            } else {
                                // Einsatz mit der Version 2023
                                foreach ($member as $itemMember) {
                                    // Name der Person
                                    $displayName = $itemMember['displayName'];
                                    $nameExplode = explode(",", $displayName);
                                    // Ausgewählte Funktion
//                                $selectedFunction = $member['selectedFunction'];
                                    // Beschreibung der ausgewählten Funktion
                                    $selectedFunctionDescription = $itemMember['selectedFunction']['description'];

                                    // Funktion finden
                                    $findFunction = $memberFunctionRepository->findOneBy(['description' => $selectedFunctionDescription]);

                                    if (!$findFunction) {
                                        dd("Keine Funktion des Kamerades gefunden. " . $selectedFunctionDescription);
                                    }

                                    // Neuer Kamerad
                                    // Name finden
                                    $findMember = $memberRepository->findOneBy(['firstname' => trim($nameExplode[1]), 'surname' => trim($nameExplode[0])]);

                                    if (!$findMember) {
                                        dd("Foglenden Kamaraden nicht gefunden: " . $displayName);
                                    }

                                    if (!in_array($findMember->getId(), $newOwnForces['members'][$newIndex]['members'])) {

                                        $newOwnForces['members'][$index]['members'][] = $findMember->getId();
                                        $newOwnForces['vehiclesWithMemberAndFunction'][$index]['members'][] = [
                                            "id" => $findMember->getId(),
                                            "surname" => $findMember->getSurname(),
                                            "firstname" => $findMember->getFirstname(),
                                            "operationFunction" => $findFunction->getId(),
                                            "operationFunctionDetails" => [
                                                "id" => $findFunction->getId(),
                                                "sorting" => $findFunction->getSorting(),
                                                "description" => $findFunction->getDescription(),
                                                "abbreviation" => $findFunction->getAbbreviation(),
                                            ]
                                        ];
                                    }
                                }
                            }
                        }
                    }
                    // Weitere Informationen zum Einsatzbericht hinzufügen
                    $newOperationReport->setOwnForces($newOwnForces);

                    // Andere Rettungsorganisationen
                    if ($item['otherTaskForces'] && count(json_decode($item['otherTaskForces'], true, 512, JSON_THROW_ON_ERROR)) > 0) {
                        foreach (json_decode($item['otherTaskForces'], true, 512, JSON_THROW_ON_ERROR) as $index => $otherTaskForce) {
                            // Fahrzeuge
                            $vehicles = $otherTaskForce['vehicles'];
                            if ($vehicles) {
                                $vehicles = str_replace(",", ", ", $vehicles);
                            }
                            // Anzahl der Kameraden
                            $emergencyServices = $otherTaskForce['emergencyServices'];
                            // Name der Organisation
                            $fireDepartment = $otherTaskForce['fireDepartment'];

                            if ($fireDepartment && str_contains($fireDepartment, "Niederwiesa")) {
                                $fireDepartment = "Freiwillige Feuerwehr Niederwiesa";
                            } else if ($fireDepartment && str_contains($fireDepartment, "Flöha")) {
                                $fireDepartment = "Freiwillige Feuerwehr Flöha";
                            } else if ($fireDepartment && str_contains($fireDepartment, "Lichtenwalde")) {
                                $fireDepartment = "Freiwillige Feuerwehr Lichtenwalde";
                            } else {
                                $fireDepartment = "Freiwillige Feuerwehr Niederwiesa";
                            }

                            if ($index === 0) {
                                $newOtherTaskForces[] = [
                                    "id" => $index + 1,
                                    "comment" => "Die Daten wurden aus dem Schritt 3 Übernommen.",
                                    "vehicles" => $vehicles,
                                    "numberOfForces" => $emergencyServices,
                                    "rescueOrganizations" => $fireDepartment,
                                ];
                            } else {
                                $newOtherTaskForces[] = [
                                    "id" => $index + 1,
                                    "comment" => "",
                                    "vehicles" => $vehicles,
                                    "numberOfForces" => $emergencyServices,
                                    "rescueOrganizations" => $fireDepartment,
                                ];
                            }
                        }
                    }
                    // Weitere Informationen zum Einsatzbericht hinzufügen
                    $newOperationReport->setOtherTaskForces($newOtherTaskForces);

                    // eingesetzte Hilfsmittel
                    if ($item['tools'] && count(json_decode($item['tools'], true, 512, JSON_THROW_ON_ERROR)) > 0) {
                        foreach (json_decode($item['tools'], true, 512, JSON_THROW_ON_ERROR) as $tool) {
                            // Hilfsmittelname
                            $name = $tool['name'];
                            // Anzahl des Hilfsmittels
                            $numbers = $tool['number'];
                            // Finden des Hilfsmittels
                            $findTool = $toolRepository->findOneBy(['description' => $name]);
                            // Wurde ein Hilfsmittel gefunden?
                            if (!$findTool) {
                                dd("Folgendes Hilfsmittel wurde nicht gefunden: " . $name);
                            }
                            // Hinzufügen das Hilfsmittel
                            $newTools['allTools'][] = $findTool->getId();
                            // Hinzufügen das Hilfsmittel mit Anzahl
                            $newTools['toolsWithNumbers'][] = [
                                "id" => $findTool->getId(),
                                "numbers" => $numbers,
                                "description" => $findTool->getDescription(),
                                "abbreviation" => $findTool->getAbbreviation(),
                                "toolCategory" => [
                                    "id" => $findTool->getToolCategory() ? $findTool->getToolCategory()->getId() : "",
                                    "description" => $findTool->getToolCategory() ? $findTool->getToolCategory()->getDescription() : "",
                                    "abbreviation" => $findTool->getToolCategory() ? $findTool->getToolCategory()->getAbbreviation() : "",
                                ]
                            ];
                        }
                    }
                    // Weitere Informationen zum Einsatzbericht hinzufügen
                    $newOperationReport
                        ->setTools($newTools)
                        ->setCommentPrint($item['commentDetail'])
                        ->setIsVisibility(true)
                        ->setCreatedFrom("Import System")
                        ->setCreatedAt(new DateTimeImmutable($item['createdAt']))
                        ->setUpdatedAt(new DateTimeImmutable($item['updatedAt']));

                    $operationTypeDescription = "";
                    // Die Einsatzkategorie Zurechtfinden
                    match ($item['operationTypeDescription']) {
                        "Kleinbrand A" => $operationTypeDescription = "v2 - Kleinbrand Alpha",
                        "Kleinbrand B" => $operationTypeDescription = "v2 - Kleinbrand Bravo",
                        "VKU (Sonstige)" => $operationTypeDescription = "v2 - VKU (Sonstige)",
                        "Mittelbrand" => $operationTypeDescription = "v2 - Mittelbrand",
                        "TH_Öl Land/ Gewässer" => $operationTypeDescription = "v2 - TH_Öl Land/ Gewässer",
                        "Sonstige Technische Hilfeleistungen" => $operationTypeDescription = "v2 - Sonstige Technische Hilfeleistungen",
                        "VKU (Personenrettung)" => $operationTypeDescription = "v2 - VKU (Personenrettung)",
                        "Sturm/ Baum/ Hochwasser/ Eisgefahren" => $operationTypeDescription = "v2 - Sturm/ Baum/ Hochwasser/ Eisgefahren",
                        "Brandmeldeanlage" => $operationTypeDescription = "v2 - Brandmeldeanlage",
                        "Großbrand" => $operationTypeDescription = "v2 - Großbrand",
                        "ABC-Gefahren (CBRN)" => $operationTypeDescription = "v2 - ABC-Gefahren (CBRN)",
                        "Einsatz außerhalb der Gemeinde" => $operationTypeDescription = "v2 - Einsatz außerhalb der Gemeinde",
                        "Katastrophenschutz" => $operationTypeDescription = "v2 - Katastrophenschutz",
                        "Tierrettung" => $operationTypeDescription = "v2 - Tierrettung",
                        "Sonstige Einsätze" => $operationTypeDescription = "v2 - Sonstige Einsätze",
                        default => dd("Einsatzart nicht gefunden: " . $item['operationTypeDescription']),
                    };
                    // Suchen der Einsatzart in der Datenbank
                    $operationType = $operationTypeRepository->findOneBy(['description' => $operationTypeDescription]);
                    // Wurde eine Einsatzart gefunden?
                    if (!$operationType) {
                        dd("Es wurde keine Einsatzart. " . $item['operationTypeDescription']);
                    }
                    // Einsatzbericht Details erstellen
                    $newOperationReportDetail
                        ->setOperationType($operationType)
                        ->setIncomingDateTime(new DateTimeImmutable($item['incomingDateTime']))
                        ->setStartetAt(new DateTimeImmutable($item['deploymentStart']))
                        ->setEndetAt(new DateTimeImmutable($item['deploymentEnd']))
                        ->setNumberControlCenter($item['numberControlCenter'])
                        ->setKeywordShort($item['keywordShort'])
                        ->setKeywordLong($item['keywordLong'])
                        ->setLocation($item['location'])
                        ->setOtherAddressInformation($newAlert[0]->getOtherAddressInformation())
                        ->setPersonPatient($newAlert[0]->getPersonPatient())
                        ->setMessageComplete($newAlert[0]->getMessageComplete())
                        ->setPostalCode($item['postalCode'])
                        ->setPlace($item['location'])
                        ->setStreetWithNumber($item['streetWithNumber'])
                        ->setNumberControlCenter($item['numberControlCenter'])
                        ->setNumberPolice($item['numberPolice'])
                        ->setNumberFireDepartment($item['numberFireDepartment'])
                        ->setText($item['text'])
                        ->setCreatedAt(new DateTimeImmutable($item['createdAt']))
                        ->setCreatedFrom("Import System")
                        ->setUpdatedFrom("Import System")
                        ->setIsVisibility(true)
                        ->setUpdatedAt(new DateTimeImmutable($item['updatedAt']));

                    // Alarm nicht mehr sichtbar machen
                    /* @var IncomingAlert[] $newAlert */
                    $newAlert[0]->setIsVisibilityOperationReport(false);

                    // In den Zwischenspeichern hinzufügen
                    $entityManager->persist($newOperationReport);
                    $entityManager->persist($newOperationReportDetail);
                    $entityManager->persist($newAlert[0]);

                    if (!str_contains($newAlert[0]->getNumberControlCenter(), $item['numberControlCenter'])) {
                        $fileSystem->mkdir("Fehler");
                        $fileSystem->touch("Fehler/Einsatz.txt");
                        $fileSystem->appendToFile("Fehler/Einsatz.txt", "Kein richtiger Einsatz gefunden: " . $item['numberControlCenter'] . "(" . $newAlert[0]->getNumberControlCenter() . ")  - " . $item['text'] . "\n");
                    }
                } else {
                    $fileSystem->mkdir("Fehler");
                    $fileSystem->touch("Fehler/Fehler.txt");
                    $fileSystem->appendToFile("Fehler/Fehler.txt", "Kein Alarm gefunden: " . $item['text'] . "\n");
//                    dd("Kein Alarm gefunden: " . $item['text']);
                }
                // Speichern in der Datenbank
                $entityManager->flush();
            }

        } catch (Exception $e) {
            echo "Error!: " . $e->getMessage() . "<br/>";
            die();
        }

        return $this->render('import/index.html.twig', [
            'controller_name' => 'ImportController',
        ]);
    }
}
