<?php
declare(strict_types=1);
namespace App\Controller;

use App\Repository\UserRepository;
use LogicException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;

class AuthenticationController extends AbstractController
{

    #[Route(path: '/login-check', name: 'app_login')]
    public function login(AuthenticationUtils $authenticationUtils, LoggerInterface $logger, UserRepository $userRepository, TranslatorInterface $translator): Response
    {
        $logger->info('Login in the Web-Application');

        // Ist der Nutzer bereits angemeldet, dann leite ihn weiter
        if ($this->getUser()) {
            return $this->redirectToRoute('app-frontend', ['route' => 'dashboard']);
        }

        // get the login error if there is one
        $authError = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $error = null;
        $status = 200;

        // Sind Fehler bei der Authentisierung aufgetreten?
        if ($authError !== null) {
            // Prüfen, ob der Nutzer in der Web-Anwendung vorhanden ist.
            $user = $userRepository->findOneBy(['email' => $lastUsername]);

            if (!$user) {
                $error = $translator->trans('security.noUserFound');
                $status = 204;
                $logger->warning('No User found in the Database!');
            } else {
                $error = $translator->trans("security." . $authError->getMessage());
                $status = 409;
                $logger->error('User Passwort Wrong!');
            }
        }

        return new JsonResponse(['last_username' => $lastUsername, 'error' => $error], (int)$status);
    }

    #[Route(path: '/logout', name: 'app_logout')]
    public function logout(): void
    {
        throw new LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
