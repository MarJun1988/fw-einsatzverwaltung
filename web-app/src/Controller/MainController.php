<?php
declare(strict_types=1);

namespace App\Controller;

use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;


class MainController extends AbstractController
{
    #[Route('/', name: 'default-app')]
    public function loginRedirectToRoute(LoggerInterface $logger): RedirectResponse
    {
        $logger->info('Redirect to Web-Anwendung');

        return new RedirectResponse('/dashboard');
    }

    #[Route('/{route}', name: 'app-frontend', requirements: ['route' => '^(?!.*_wdt|_profiler|basic|api|incoming-alert-from-bosmon).+'])]
    public function index(TranslatorInterface $translator, UserRepository $userRepository, EntityManagerInterface $entityManager, LoggerInterface $logger): Response
    {

//        $pdf->generate("https://127.0.0.1:8000/operation-area/operation-report/details/bc03da3e-d2f9-409f-9817-066bd97fd865", "google.pdf");


        // Ist der Nutzer erfolgreich angemeldet, dann das letzte Anmelde DateTime setzen
        if ($this->getUser()) {
            $user = $userRepository->findOneBy(['email' => $this->getUser()->getUserIdentifier()]);
            if ($user) {
                $user->setLastLogin(new DateTime());
                $entityManager->persist($user);
                $entityManager->flush();
            }


            $logger->info('Update the Lastlogin from the User (' . $user->getUserIdentifier() . ') in der Database.');
        }

        // Weiterleitung zur Hauptseite (VUE JS)
        return $this->render('main/index.html.twig');
    }


    #[Route('/login', name: 'app-frontend-login')]
    public function toLogin(LoggerInterface $logger): Response
    {

        $logger->info('Login in the Web-Application');

        // Ist der Nutzer bereits angemeldet, dann leite ihn weiter
        if ($this->getUser()) {
            return $this->redirectToRoute('app-frontend', ['route' => 'dashboard']);
        }

        // Weiterleitung zur Hauptseite (VUE JS)
        return $this->render('main/index.html.twig');
    }

}
