<?php
declare(strict_types=1);

namespace App\Controller\API;

use App\Entity\User;
use App\Repository\DepartmentRepository;
use App\Repository\FederalStateRepository;
use App\Repository\UserRepository;
use App\Service\CheckEntityService;
use App\Service\DataTableService;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\QueryException;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/api/user', name: 'api_user')]
#[IsGranted('ROLE_USER')]
class UserController extends AbstractController
{

    private const LOGGER_ALL_ENTRIES = "Users";
    private const LOGGER_ONE_ENTRY = "User";

    public function __construct(
        public LoggerInterface        $logger,
        public CheckEntityService     $checkEntity,
        public EntityManagerInterface $entityManager,
        public TranslatorInterface    $translator,
        public FederalStateRepository $federalStateRepository,
        public DepartmentRepository   $departmentRepository,
        public UserRepository         $userRepository,
        public DataTableService       $dataTableService)
    {
    }

    /**
     * Read all Entries from the Database
     *
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     * @throws JsonException
     * @throws QueryException
     */
    #[Route('s', name: 's', methods: ['GET'])]
    #[IsGranted('ROLE_USER_READ')]
    public function readAll(SerializerInterface $serializer, Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Read all " . self::LOGGER_ALL_ENTRIES . " from the Database!");
        // Auslesen aller Einträge
        $param = [
            "department" => $this->getUser()->getDepartment()->getId(),
            "isFullAdmin" => $this->isGranted('ROLE_FULL_ADMINISTRATOR')
        ];

        $items = $this->userRepository->findByDataTableService($request, $this->dataTableService, $param, $this->isGranted("ROLE_FULL_ADMINISTRATOR"));
        // Objekte Konvertieren
        $serialize = $serializer->serialize(
            $items['items'],
            'json',
            ['groups' => ['users', 'federalState', 'department'], AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT => 2]
        );
        // Ausgabe als JSON
        return new JsonResponse([
            'items' => json_decode($serialize, false, 512, JSON_THROW_ON_ERROR),
            'filterCount' => $items['filterCount'],
            'totalCount' => $items['totalCount']
        ]);
    }

    /**
     * Read the Entry with ID from the Database
     *
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     * @throws JsonException
     */
    #[Route('/get-by-id/{id}', name: '_get_by_id', methods: ['GET'])]
    #[IsGranted('ROLE_USER_READ')]
    public function getById(SerializerInterface $serializer, Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Read the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id') . " from the Database!");
        // Auslesen des Einträges
        $items = $this->userRepository->find($request->get('id'));
        // Objekte Konvertieren
        $serialize = $serializer->serialize(
            $items,
            'json',
            ['groups' => ['dataByIdUser', 'federalState', 'department'], AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT => 2]
        );

        // Ausgabe als JSON
        return new JsonResponse(
            ['item' => json_decode($serialize, false, 512, JSON_THROW_ON_ERROR)]
        );
    }

    /**
     * Create a new Entry in the Database
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/new', name: '_new', methods: ['POST'])]
    #[IsGranted('ROLE_USER_NEW')]
    public function new(Request $request, UserPasswordHasherInterface $passwordHasher): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Create a new " . self::LOGGER_ONE_ENTRY);
        // Übermittelte Daten
        $requestEntry = $request->toArray();

        // Anlegen eines neuen Eintrages
        $item = (new User())
            ->setFederalState($this->federalStateRepository->find($requestEntry['federalState']['id']))
            ->setDepartment($this->departmentRepository->find($requestEntry['department']['id']))
            ->setSurname(trim((string)$requestEntry['surname']))
            ->setFirstname(trim((string)$requestEntry['firstname']))
            ->setEmail(trim((string)$requestEntry['email']))
            ->setRoles((array)$requestEntry['roles'])
            ->setMyAlertAddresses((array)$requestEntry['myAlertAddresses'])
            ->setIsVisibility((bool)$requestEntry['isVisibility'])
            ->setComment(trim((string)$requestEntry['comment']))
            ->setCreatedFrom($this->getUser()->getSurname() . ', ' . $this->getUser()->getFirstname())
            ->setCreatedAt(new DateTimeImmutable());

        // Ist ein Password gesetzt?
        if ((string)$requestEntry['password'] && (string)$requestEntry['passwordRepeat'] &&
            (string)$requestEntry['password'] === (string)$requestEntry['passwordRepeat']
        ) {
            $hashedPassword = $passwordHasher->hashPassword(
                $item,
                (string)$requestEntry['password']
            );
            $item->setPassword($hashedPassword);
        }

        // Prüfen von doppelten Einträgen sowie leeren Feldern, ...
        return $this->checkEntity->validation($item);
    }

    /**
     * Update the Entry with ID in the Database
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/edit/{id}', name: '_edit', methods: ['POST'])]
    #[IsGranted('ROLE_USER_EDIT')]
    public function edit(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Update the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id'));
        // Suchen des Eintrages in der Datenbank
        $item = $this->userRepository->find($request->get('id'));
        // Übermittelte Daten
        $requestEntry = $request->toArray();

        if ($item) {
            $item
                ->setFederalState($this->federalStateRepository->find($requestEntry['federalState']['id']))
                ->setDepartment($this->departmentRepository->find($requestEntry['department']['id']))
                ->setSurname(trim((string)$requestEntry['surname']))
                ->setFirstname(trim((string)$requestEntry['firstname']))
                ->setEmail(trim((string)$requestEntry['email']))
                ->setRoles((array)$requestEntry['roles'])
                ->setMyAlertAddresses((array)$requestEntry['myAlertAddresses'])
                ->setIsVisibility((bool)$requestEntry['isVisibility'])
                ->setComment(trim((string)$requestEntry['comment']))
                ->setUpdatedFrom($this->getUser()->getSurname() . ', ' . $this->getUser()->getFirstname())
                ->setUpdatedAt(new DateTimeImmutable());
        }

        // Prüfen von doppelten Einträgen sowie leeren Feldern, ...
        return $this->checkEntity->validation($item);
    }

    /**
     * Update the Entry with ID in the Database,
     * only the Password
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/edit-password/{id}', name: '_edit_password', methods: ['POST'])]
    #[IsGranted('ROLE_USER_EDIT')]
    public function editPassword(Request $request, UserPasswordHasherInterface $passwordHasher): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Update the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id'));
        // Suchen des Eintrages in der Datenbank
        $item = $this->userRepository->find($request->get('id'));
        // Übermittelte Daten
        $requestEntry = $request->toArray();

        if ($item) {

            $hashedPasswordOld = $passwordHasher->isPasswordValid($item, (string)$requestEntry['passwordOld']);

            if ($hashedPasswordOld) {
                $hashedPassword = $passwordHasher->hashPassword(
                    $item,
                    (string)$requestEntry['password']
                );
                $item
                    ->setPassword($hashedPassword)
                    ->setUpdatedFrom($this->getUser()->getSurname() . ', ' . $this->getUser()->getFirstname())
                    ->setUpdatedAt(new DateTimeImmutable());
            } else {

                $errors[] = [
                    'field' => "passwordOld",
                    'message' => "Altes Password, ist nicht Korrekt!"
                ];

                // Ausgabe der Fehler Meldung
                return new JsonResponse([
                    'status' => 'error',
                    'message' => $this->translator->trans('entityMessage.failed'),
                    'errors' => $errors,
                    'item' => null
                ], Response::HTTP_BAD_REQUEST);
            }
        }

        // Prüfen von doppelten Einträgen sowie leeren Feldern, ...
        return $this->checkEntity->validation($item);
    }


    /**
     * Delete the Entry with ID in the Database
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/delete/{id}', name: '_delete', methods: ['POST'])]
    #[IsGranted('ROLE_USER_DELETE')]
    public function delete(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Delete the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id'));
        // Suchen des Eintrages in der Datenbank
        $item = $this->userRepository->find($request->get('id'));
        // Prüfen, ob der Eintrag vorhanden ist, ...
        return $this->checkEntity->validationDelete($item);
    }
}
