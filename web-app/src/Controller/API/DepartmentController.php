<?php
declare(strict_types=1);

namespace App\Controller\API;

use App\Entity\Department;
use App\Repository\DepartmentRepository;
use App\Repository\FederalStateRepository;
use App\Service\CheckEntityService;
use App\Service\DataTableService;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\QueryException;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/api/department', name: 'api_department')]
#[IsGranted('ROLE_USER')]
class DepartmentController extends AbstractController
{

    private const LOGGER_ALL_ENTRIES = "Departments";
    private const LOGGER_ONE_ENTRY = "Department";

    public function __construct(
        public LoggerInterface        $logger,
        public CheckEntityService     $checkEntity,
        public EntityManagerInterface $entityManager,
        public TranslatorInterface    $translator,
        public DepartmentRepository   $departmentRepository,
        public FederalStateRepository $federalStateRepository,
        public DataTableService       $dataTableService)
    {
    }

    /**
     * Read all Entries from the Database
     *
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     * @throws JsonException
     * @throws QueryException
     */
    #[Route('s', name: 's', methods: ['GET'])]
    #[IsGranted('ROLE_DEPARTMENT_READ')]
    public function readAll(SerializerInterface $serializer, Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Read all " . self::LOGGER_ALL_ENTRIES . " from the Database!");
        // Auslesen aller Einträge
        $items = $this->departmentRepository->findByDataTableService($request, $this->dataTableService,  $this->isGranted("ROLE_FULL_ADMINISTRATOR"));
        // Objekte Konvertieren
        $serialize = $serializer->serialize(
            $items['items'],
            'json',
            ['groups' => ['departments', 'federalState']
                , AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true, AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT => 2]
        );
        // Ausgabe als JSON
        return new JsonResponse([
            'items' => json_decode($serialize, false, 512, JSON_THROW_ON_ERROR),
            'filterCount' => $items['filterCount'],
            'totalCount' => $items['totalCount']
        ]);
    }

    /**
     * Read all Entries from the Database for Dropdown
     *
     * @param SerializerInterface $serializer
     * @return Response
     * @throws JsonException
     */
    #[Route('/get-for-dropdown', name: '_get_for_dropdown', methods: ['GET'])]
    #[IsGranted('ROLE_DEPARTMENT_READ')]
    public function getForDropdown(SerializerInterface $serializer): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Read the all " . self::LOGGER_ALL_ENTRIES . " from the Database!");
        // Auslesen des Einträges
        $items = $this->departmentRepository->findBy([], ['description' => 'ASC']);
        // Objekte Konvertieren
        $serialize = $serializer->serialize(
            $items,
            'json',
            ['groups' => ['department'], AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT => 2]
        );
        // Ausgabe als JSON
        return new JsonResponse(
            ['items' => json_decode($serialize, false, 512, JSON_THROW_ON_ERROR)]
        );
    }

    /**
     * Read the Entry with ID from the Database
     *
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     * @throws JsonException
     */
    #[Route('/get-by-id/{id}', name: '_get_by_id', methods: ['GET'])]
    #[IsGranted('ROLE_DEPARTMENT_READ')]
    public function getById(SerializerInterface $serializer, Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Read the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id') . " from the Database!");
        // Auslesen des Einträges
        $items = $this->departmentRepository->find($request->get('id'));
        // Objekte Konvertieren
        $serialize = $serializer->serialize(
            $items,
            'json',
            ['groups' => ['dataByIdDepartment', 'federalState'], AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT => 2]
        );
        // Ausgabe als JSON
        return new JsonResponse(
            ['item' => json_decode($serialize, false, 512, JSON_THROW_ON_ERROR)]
        );
    }

    /**
     * Create a new Entry in the Database
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/new', name: '_new', methods: ['POST'])]
    #[IsGranted('ROLE_DEPARTMENT_NEW')]
    public function new(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Create a new " . self::LOGGER_ONE_ENTRY);
        // Übermittelte Daten
        $requestEntry = $request->toArray();

        // Anlegen eines neuen Eintrages
        $item = (new Department())
            ->setAbbreviation(trim((string)$requestEntry['abbreviation']))
            ->setDescription(trim((string)$requestEntry['description']))
            ->setFederalState($this->federalStateRepository->find($requestEntry['federalState']['id']))
            ->setZipCode(trim((string)$requestEntry['zipCode']))
            ->setPlaceName(trim((string)$requestEntry['placeName']))
            ->setStreet(trim((string)$requestEntry['street']))
            ->setComment(trim((string)$requestEntry['comment']))
            ->setIsVisibility((bool)$requestEntry['isVisibility'])
            ->setCreatedFrom($this->getUser()->getSurname() . ', ' . $this->getUser()->getFirstname())
            ->setCreatedAt(new DateTimeImmutable());

        // Prüfen von doppelten Einträgen sowie leeren Feldern, ...
        return $this->checkEntity->validation($item);
    }

    /**
     * Update the Entry with ID in the Database
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/edit/{id}', name: '_edit', methods: ['POST'])]
    #[IsGranted('ROLE_DEPARTMENT_EDIT')]
    public function edit(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Update the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id'));
        // Suchen des Eintrages in der Datenbank
        $item = $this->departmentRepository->find($request->get('id'));
        // Übermittelte Daten
        $requestEntry = $request->toArray();

        if ($item) {
            $item
                ->setAbbreviation(trim((string)$requestEntry['abbreviation']))
                ->setDescription(trim((string)$requestEntry['description']))
                ->setFederalState($this->federalStateRepository->find($requestEntry['federalState']['id']))
                ->setZipCode(trim((string)$requestEntry['zipCode']))
                ->setPlaceName(trim((string)$requestEntry['placeName']))
                ->setStreet(trim((string)$requestEntry['street']))
                ->setComment(trim((string)$requestEntry['comment']))
                ->setIsVisibility((bool)$requestEntry['isVisibility'])
                ->setUpdatedFrom($this->getUser()->getSurname() . ', ' . $this->getUser()->getFirstname())
                ->setUpdatedAt(new DateTimeImmutable());
        }

        // Prüfen von doppelten Einträgen sowie leeren Feldern, ...
        return $this->checkEntity->validation($item);
    }

    /**
     * Delete the Entry with ID in the Database
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/delete/{id}', name: '_delete', methods: ['POST'])]
    #[IsGranted('ROLE_DEPARTMENT_DELETE')]
    public function delete(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Delete the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id'));
        // Suchen des Eintrages in der Datenbank
        $item = $this->departmentRepository->find($request->get('id'));
        // Prüfen, ob der Eintrag vorhanden ist, ...
        return $this->checkEntity->validationDelete($item);
    }
}
