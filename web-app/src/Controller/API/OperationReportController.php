<?php
declare(strict_types=1);

namespace App\Controller\API;

use App\Entity\OperationReport;
use App\Entity\OperationReportDetail;
use App\Repository\AlertAddressRepository;
use App\Repository\DepartmentRepository;
use App\Repository\FederalStateRepository;
use App\Repository\IncomingAlertRepository;
use App\Repository\OperationReportDetailRepository;
use App\Repository\OperationReportRepository;
use App\Repository\OperationTypeRepository;
use App\Service\CheckEntityService;
use App\Service\DataTableService;
use DateTime;
use DateTimeImmutable;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\QueryException;
use JsonException;
use Knp\Snappy\Pdf;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Path;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use function Symfony\Component\Clock\now;

#[Route('/api/operation-report', name: 'api_operation_report')]
#[IsGranted('ROLE_USER')]
class OperationReportController extends AbstractController
{

    private const LOGGER_ALL_ENTRIES = "Operation Reports";
    private const LOGGER_ONE_ENTRY = "Operation Report";

    public function __construct(
        public LoggerInterface                 $logger,
        public CheckEntityService              $checkEntity,
        public EntityManagerInterface          $entityManager,
        public TranslatorInterface             $translator,
        public FederalStateRepository          $federalStateRepository,
        public DepartmentRepository            $departmentRepository,
        public AlertAddressRepository          $alertAddressRepository,
        public IncomingAlertRepository         $incomingAlertRepository,
        public OperationTypeRepository         $operationTypeRepository,
        public OperationReportDetailRepository $operationReportDetailRepository,
        public OperationReportRepository       $operationReportRepository,
        public DataTableService                $dataTableService)
    {
    }

    /**
     * Read all Entries from the Database
     *
     * @param Pdf $pdf
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     * @throws JsonException
     * @throws QueryException
     */
    #[Route('s', name: 's', methods: ['GET'])]
    #[IsGranted('ROLE_OPERATION_REPORT_READ')]
    public function readAll(Pdf $pdf, SerializerInterface $serializer, Request $request): Response
    {

//        $html = $this->render("x##")


//        $pdf->generate("https://127.0.0.1:8000/dashboard", (new DateTime())->getTimestamp() . '.pdf');


        // Ausgabe in der Logdatei
        $this->logger->info("Read all " . self::LOGGER_ALL_ENTRIES . " from the Database!");
        // Auslesen aller Einträge
        $param = [
            "department" => $this->getUser()->getDepartment()->getId(),
            "isFullAdmin" => $this->isGranted('ROLE_FULL_ADMINISTRATOR'),
            "year" => null
        ];
        $items = $this->operationReportRepository->findByDataTableService($request, $this->dataTableService, $param, $this->isGranted("ROLE_FULL_ADMINISTRATOR"));
        // Objekte Konvertieren
        $serialize = $serializer->serialize(
            $items['items'],
            'json',
            ['groups' => ['operationReports', 'operationReportDetail', 'incomingAlert', 'operationType', 'operationTypeCategory', 'alertAddress', 'federalState', 'department'], AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT => 2]
        );
        // Ausgabe als JSON
        return new JsonResponse([
            'items' => json_decode($serialize, false, 512, JSON_THROW_ON_ERROR),
            'filterCount' => $items['filterCount'],
            'totalCount' => $items['totalCount']
        ]);
    }

    /**
     * Read all Entries from the Database
     *
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     * @throws JsonException
     * @throws QueryException
     */
    #[Route('s/evaluation', name: 's_evaluation', methods: ['GET'])]
    #[IsGranted('ROLE_OPERATION_REPORT_READ')]
    public function readEvaluationAll(SerializerInterface $serializer, Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Read all " . self::LOGGER_ALL_ENTRIES . " from the Database!");
        // Auslesen aller Einträge
        $param = [
            "department" => $this->getUser()->getDepartment()->getId(),
            "isFullAdmin" => $this->isGranted('ROLE_FULL_ADMINISTRATOR'),
//            "year" => $request->get('year')
        ];
        $items = $this->operationReportRepository->findByDataTableService($request, $this->dataTableService, $param, $this->isGranted("ROLE_FULL_ADMINISTRATOR"));
        // Objekte Konvertieren
        $serialize = $serializer->serialize(
            $items['items'],
            'json',
            ['groups' => ['operationReports', 'operationReportDetail', 'incomingAlert', 'operationType', 'alertAddress', 'federalState', 'department'], AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT => 2]
        );
        // Ausgabe als JSON
        return new JsonResponse([
            'items' => json_decode($serialize, false, 512, JSON_THROW_ON_ERROR),
            'filterCount' => $items['filterCount'],
            'totalCount' => $items['totalCount']
        ]);
    }

    /**
     * Read the Entry with ID from the Database
     *
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     * @throws JsonException
     */
    #[Route('/get-by-id/{id}', name: '_get_by_id', methods: ['GET'])]
    #[IsGranted('ROLE_OPERATION_REPORT_READ')]
    public function getById(SerializerInterface $serializer, Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Read the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id') . " from the Database!");
        // Auslesen des Einträges
        $items = $this->operationReportRepository->find($request->get('id'));
        // Objekte Konvertieren
        $serialize = $serializer->serialize(
            $items,
            'json',
            ['groups' => ['dataByIdOperationReport', 'operationReportDetail', 'incomingAlert', 'operationType', 'operationTypeCategory', 'alertAddress', 'federalState', 'department'], AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT => 2]
        );
        // Ausgabe als JSON
        return new JsonResponse(
            ['item' => json_decode($serialize, false, 512, JSON_THROW_ON_ERROR)]
        );
    }

    /**
     * Create a new Entry in the Database
     *
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    #[Route('/new', name: '_new', methods: ['POST'])]
    #[IsGranted('ROLE_OPERATION_REPORT_NEW')]
    public function new(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Create a new " . self::LOGGER_ONE_ENTRY);
        // Übermittelte Daten
        $requestEntry = $request->toArray();

        // Alarmierung
        $incomingAlert = $this->incomingAlertRepository->find($requestEntry['incomingAlert']['id']);

        // Anlegen eines neuen Eintrages
        $operationReportDetail = (new OperationReportDetail())
            ->setOperationType($this->operationTypeRepository->find($requestEntry['operationReportDetail']['operationType']['id']))
            ->setStartetAt(new DateTimeImmutable($requestEntry['operationReportDetail']['startetAt']))
            ->setEndetAt(new DateTimeImmutable($requestEntry['operationReportDetail']['endetAt']))
            ->setNumberControlCenter(trim((string)$requestEntry['operationReportDetail']['numberControlCenter']))
            ->setKeywordShort(trim((string)$requestEntry['operationReportDetail']['keywordShort']))
            ->setKeywordLong(trim((string)$requestEntry['operationReportDetail']['keywordLong']))
            ->setLocation(trim((string)$requestEntry['operationReportDetail']['location']))
            ->setOtherAddressInformation(trim((string)$requestEntry['operationReportDetail']['otherAddressInformation']))
            ->setPersonPatient(trim((string)$requestEntry['operationReportDetail']['personPatient']))
            ->setMessageComplete(trim((string)$requestEntry['operationReportDetail']['messageComplete']))
            ->setText(trim((string)$requestEntry['operationReportDetail']['text']))
            ->setPostalCode(trim((string)$requestEntry['operationReportDetail']['postalCode']))
            ->setPlace(trim((string)$requestEntry['operationReportDetail']['place']))
            ->setStreetWithNumber(trim((string)$requestEntry['operationReportDetail']['streetWithNumber']))
            ->setNumberFireDepartment(trim((string)$requestEntry['operationReportDetail']['numberFireDepartment']))
            ->setNumberPolice(trim((string)$requestEntry['operationReportDetail']['numberPolice']))
            ->setIncomingDateTime($this->incomingAlertRepository->find($requestEntry['incomingAlert']['id'])->getIncomingDateTime())
            ->setIsVisibility(true)
            ->setCreatedFrom($this->getUser()->getSurname() . ', ' . $this->getUser()->getFirstname())
            ->setCreatedAt(new DateTimeImmutable());

        $item = (new OperationReport())
            ->setFederalState($this->getUser()->getFederalState())
            ->setDepartment($this->getUser()->getDepartment())
            ->setOperationReportDetail($operationReportDetail)
            ->setIncomingAlert($incomingAlert)
            ->setIsVisibility((bool)$requestEntry['isVisibility'])
            ->setOwnForces((array)$requestEntry['ownForces'])
            ->setOtherTaskForces((array)$requestEntry['otherTaskForces'])
            ->setTools((array)$requestEntry['tools'])
            ->setSessionStore((array)$requestEntry['sessionStore'])
            ->setCommentPrint((string)$requestEntry['commentPrint'])
            ->setComment(trim((string)$requestEntry['comment']))
            ->setCreatedFrom($this->getUser()->getSurname() . ', ' . $this->getUser()->getFirstname())
            ->setCreatedAt(new DateTimeImmutable());

        // Prüfen von doppelten Einträgen sowie leeren Feldern, ...
        $this->entityManager->persist($operationReportDetail);
//        $this->entityManager->flush();

        // Alarmierung, bekannt geben das es einen Einsatzbericht gibt
        $incomingAlert->setIsVisibilityOperationReport(false);
        $this->entityManager->persist($incomingAlert);

        return $this->checkEntity->validation($item);
    }

    /**
     * Update the Entry with ID in the Database
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/edit/{id}', name: '_edit', methods: ['POST'])]
    #[IsGranted('ROLE_OPERATION_REPORT_EDIT')]
    public function edit(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Update the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id'));
        // Suchen des Eintrages in der Datenbank
        $item = $this->operationReportRepository->find($request->get('id'));
        // Übermittelte Daten
        $requestEntry = $request->toArray();
        $operationReportDetail = $this->operationReportDetailRepository->find($requestEntry['operationReportDetail']['id']);

        if ($item) {
            $item
                ->setFederalState($this->getUser()->getFederalState())
                ->setDepartment($this->getUser()->getDepartment())
                ->setIsVisibility((bool)$requestEntry['isVisibility'])
                ->setOwnForces((array)$requestEntry['ownForces'])
                ->setOtherTaskForces((array)$requestEntry['otherTaskForces'])
                ->setTools((array)$requestEntry['tools'])
                ->setSessionStore((array)$requestEntry['sessionStore'])
                ->setCommentPrint((string)$requestEntry['commentPrint'])
                ->setComment(trim((string)$requestEntry['comment']))
                ->setUpdatedFrom($this->getUser()->getSurname() . ', ' . $this->getUser()->getFirstname())
                ->setUpdatedAt(new DateTimeImmutable());

            $operationReportDetail
                ->setOperationType($this->operationTypeRepository->find($requestEntry['operationReportDetail']['operationType']['id']))
                ->setStartetAt(new DateTimeImmutable($requestEntry['operationReportDetail']['startetAt']))
                ->setEndetAt(new DateTimeImmutable($requestEntry['operationReportDetail']['endetAt']))
                ->setNumberControlCenter(trim((string)$requestEntry['operationReportDetail']['numberControlCenter']))
                ->setKeywordShort(trim((string)$requestEntry['operationReportDetail']['keywordShort']))
                ->setKeywordLong(trim((string)$requestEntry['operationReportDetail']['keywordLong']))
                ->setLocation(trim((string)$requestEntry['operationReportDetail']['location']))
                ->setOtherAddressInformation(trim((string)$requestEntry['operationReportDetail']['otherAddressInformation']))
                ->setPersonPatient(trim((string)$requestEntry['operationReportDetail']['personPatient']))
                ->setMessageComplete(trim((string)$requestEntry['operationReportDetail']['messageComplete']))
                ->setText(trim((string)$requestEntry['operationReportDetail']['text']))
                ->setPostalCode(trim((string)$requestEntry['operationReportDetail']['postalCode']))
                ->setPlace(trim((string)$requestEntry['operationReportDetail']['place']))
                ->setStreetWithNumber(trim((string)$requestEntry['operationReportDetail']['streetWithNumber']))
                ->setNumberFireDepartment(trim((string)$requestEntry['operationReportDetail']['numberFireDepartment']))
                ->setNumberPolice(trim((string)$requestEntry['operationReportDetail']['numberPolice']))
                ->setIncomingDateTime($this->incomingAlertRepository->find($requestEntry['incomingAlert']['id'])->getIncomingDateTime())
                ->setUpdatedFrom($this->getUser()->getSurname() . ', ' . $this->getUser()->getFirstname())
                ->setUpdatedAt(new DateTimeImmutable());

            $this->entityManager->persist($operationReportDetail);
        }

//        dd($item);

        // Prüfen von doppelten Einträgen sowie leeren Feldern, ...
        return $this->checkEntity->validation($item);
    }

    /**
     * Delete the Entry with ID in the Database
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/delete/{id}', name: '_delete', methods: ['POST'])]
    #[IsGranted('ROLE_OPERATION_REPORT_DELETE')]
    public function delete(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Delete the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id'));
        // Suchen des Eintrages in der Datenbank
        $item = $this->operationReportRepository->find($request->get('id'));

        // Alarmierung
        $incomingAlert = $this->incomingAlertRepository->findOneBy(['id' => $item->getIncomingAlert()->getId()]);
        $incomingAlert->setIsVisibilityOperationReport(true);
        $this->entityManager->persist($incomingAlert);

        // Prüfen, ob der Eintrag vorhanden ist, ...
        return $this->checkEntity->validationDelete($item);
    }

    #[Route('/get-last-number-fire-department', name: '_get_last_number_fire_department', methods: ['GET'])]
    #[IsGranted('ROLE_OPERATION_REPORT_READ')]
    public function getLastNumberFireDepartment(): Response
    {
        $numberFireDepartment = null;
        $currentYear = new DateTime();
        $currentYear = $currentYear->format("Y");
        $lastOperationReport = $this->operationReportRepository->findByYear((int)$currentYear, $this->getUser()->getDepartment()->getId());

        if ($lastOperationReport) {
            $numberFireDepartment = $lastOperationReport[0]->getOperationReportDetail()->getNumberFireDepartment();
        }

        $count = 0;
        if ($lastOperationReport) {
            $count = count($lastOperationReport);
        }

        return new JsonResponse([
            'lastNumber' => $numberFireDepartment,
            'next' => $currentYear . "/" . sprintf("%'.03d", $count + 1),
            'count' => count($lastOperationReport)
        ], 200
        );
    }

    #[Route('/save-pdf/{id}', name: '_save_pdf', methods: ['POST'])]
    public function savePdf(Request $request): Response
    {
        $path = "Export-Operation-Reports";

        // Ausgabe in der Logdatei
        $this->logger->info("Export the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id'));
        // Suchen des Eintrages in der Datenbank
        $item = $this->operationReportRepository->find($request->get('id'));

        if ($item) {
            $filename = str_replace('/', '-', $item->getOperationReportDetail()->getNumberFireDepartment());
            $filenameExplode = explode('-', $filename);

            $filesystem = new Filesystem();

            // Lies den Inhalt des ArrayBuffer aus dem POST-Request
            $pdfData = $request->getContent();

            $filenameComplete = $path . '/' . $filenameExplode[0] . '/' . $filename . '.pdf';

//            if (!$item->getExportPdfPath()) {

            try {
                // Hauptverzeichnis
                $filesystem->mkdir(
                    Path::normalize($path),
                );
                // Unterordner Jahr
                $filesystem->mkdir(
                    Path::normalize($path . '/' . $filenameExplode[0]),
                );

                $filesystem->dumpFile($filenameComplete, $pdfData);

                $item->setExportPdfPath('/' . $filenameComplete);
                $this->entityManager->persist($item);
                $this->entityManager->flush();

                return new JsonResponse([
                    'status' => 'success',
                    'exportFolder' => $item->getExportPdfPath()
                ]);

            } catch (IOExceptionInterface $exception) {
                return new JsonResponse([
                    'status' => 'error',
                    'exportFolder' => $item->getExportPdfPath(),
                    'message' => $exception->getPath()
                ]);
            }
//            }
//            return new JsonResponse([
//                'status' => 'success',
//                'exportFolder' => $item->getExportPdfPath()
//            ]);
        }

        return new Response();

    }

}