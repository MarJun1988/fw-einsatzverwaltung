<?php
declare(strict_types=1);

namespace App\Controller\API;

use App\Entity\IncomingAlert;
use App\Repository\AlertAddressRepository;
use App\Repository\DepartmentRepository;
use App\Repository\FederalStateRepository;
use App\Repository\IncomingAlertRepository;
use App\Service\CheckEntityService;
use App\Service\DataTableService;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\QueryException;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\ExpressionLanguage\Expression;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/api/incoming-alert', name: 'api_incoming_alert')]
#[IsGranted('ROLE_USER')]
class IncomingAlertController extends AbstractController
{

    private const LOGGER_ALL_ENTRIES = "Incoming Alerts";
    private const LOGGER_ONE_ENTRY = "Incoming Alert";

    public function __construct(
        public LoggerInterface         $logger,
        public CheckEntityService      $checkEntity,
        public EntityManagerInterface  $entityManager,
        public TranslatorInterface     $translator,
        public FederalStateRepository  $federalStateRepository,
        public DepartmentRepository    $departmentRepository,
        public AlertAddressRepository  $alertAddressRepository,
        public IncomingAlertRepository $incomingAlertRepository,
        public DataTableService        $dataTableService)
    {
    }

    /**
     * Read all Entries from the Database
     *
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     * @throws JsonException
     * @throws QueryException
     *
     */
    #[IsGranted('ROLE_ALL_INCOMING_ALERT_READ')]
    #[Route('s', name: 's', methods: ['GET'])]
    public function readAll(SerializerInterface $serializer, Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Read all " . self::LOGGER_ALL_ENTRIES . " from the Database!");

        $param = [
            "department" => $this->getUser()->getDepartment()->getId(),
            "isFullAdmin" => $this->isGranted('ROLE_FULL_ADMINISTRATOR')
        ];

        // Auslesen aller Einträge
        $items = $this->incomingAlertRepository->findByDataTableService($request, $this->dataTableService, $param, $this->isGranted("ROLE_FULL_ADMINISTRATOR"));
        // Objekte Konvertieren
        $serialize = $serializer->serialize(
            $items['items'],
            'json',
            ['groups' => ['incomingAlerts', 'alertAddress', 'federalState', 'department'], AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT => 2]
        );
        // Ausgabe als JSON
        return new JsonResponse([
            'items' => json_decode($serialize, false, 512, JSON_THROW_ON_ERROR),
            'filterCount' => $items['filterCount'],
            'totalCount' => $items['totalCount']
        ]);
    }

    /**
     * Read only Entries from the Database, what the User RICS Show
     *
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     * @throws JsonException
     * @throws QueryException
     *
     */
    #[IsGranted('ROLE_MY_INCOMING_ALERT_READ')]
    #[Route('s/user-rics', name: 's_user_rics', methods: ['GET'])]
    public function readUserRics(SerializerInterface $serializer, Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Read User Rics " . self::LOGGER_ALL_ENTRIES . " from the Database!");

        // Auslesen aller Einträge
        $items = $this->incomingAlertRepository->findByDataTableServiceUserRics($request, $this->dataTableService, $this->getUser()->getMyAlertAddresses(), $this->isGranted("ROLE_FULL_ADMINISTRATOR"));
        // Objekte Konvertieren
        $serialize = $serializer->serialize(
            $items['items'],
            'json',
            ['groups' => ['incomingAlerts', 'alertAddress', 'federalState', 'department'], AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT => 2]
        );
        // Ausgabe als JSON
        return new JsonResponse([
            'items' => json_decode($serialize, false, 512, JSON_THROW_ON_ERROR),
            'filterCount' => $items['filterCount'],
            'totalCount' => $items['totalCount']
        ]);
    }

    /**
     * Read only Entries from the Database, what the User RICS Show
     *
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     * @throws JsonException
     * @throws QueryException
     *
     */
    #[IsGranted('ROLE_MY_INCOMING_ALERT_READ')]
    #[Route('s/user-rics-for-operation-report', name: 's_user_rics_for_operation_report', methods: ['GET'])]
    public function readUserRicsForOperationReport(SerializerInterface $serializer, Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Read User Rics " . self::LOGGER_ALL_ENTRIES . " from the Database!");

        // Auslesen aller Einträge
        $items = $this->incomingAlertRepository->findByDataTableServiceUserRics($request, $this->dataTableService, $this->getUser()->getMyAlertAddresses(), $this->isGranted("ROLE_FULL_ADMINISTRATOR"), true);
        // Objekte Konvertieren
        $serialize = $serializer->serialize(
            $items['items'],
            'json',
            ['groups' => ['incomingAlerts', 'alertAddress', 'federalState', 'department'], AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT => 2]
        );
        // Ausgabe als JSON
        return new JsonResponse([
            'items' => json_decode($serialize, false, 512, JSON_THROW_ON_ERROR),
            'filterCount' => $items['filterCount'],
            'totalCount' => $items['totalCount']
        ]);
    }

    /**
     * Read only Entries from the Database for the Chart Evaluation, from Year and User Rics
     *
     * @param Request $request
     * @param TranslatorInterface $translator
     * @return Response
     */
    #[IsGranted('ROLE_MY_INCOMING_ALERT_READ')]
    #[Route('s/get-chart-by-year/{year}', name: 's_get_chart_by_year', methods: ['GET'])]
    public function getByChartYear(Request $request, TranslatorInterface $translator): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Read Alerts from Year from the Database!");

        $year = (int)$request->get('year');
        $labels = [$translator->trans("generals.months.january"), $translator->trans("generals.months.february"), $translator->trans("generals.months.march"),
            $translator->trans("generals.months.april"), $translator->trans("generals.months.may"), $translator->trans("generals.months.june"),
            $translator->trans("generals.months.july"), $translator->trans("generals.months.august"), $translator->trans("generals.months.september"),
            $translator->trans("generals.months.october"), $translator->trans("generals.months.november"), $translator->trans("generals.months.december"),
            $translator->trans("generals.months.total")];
        $monthNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        $data = [];
        $totalCount = 0;

        foreach ($monthNumbers as $month) {
            // Auslesen aller Einträge
            $items = $this->incomingAlertRepository->findByYearAndMonthUserRics($year, $month, $this->getUser()->getMyAlertAddresses(), $this->isGranted("ROLE_FULL_ADMINISTRATOR"));
            // Werte der Monate
            $data[] = count($items);
            // Gesamtanzahl
            $totalCount += count($items);
        }
        // Gesamtanzahl hinzufügen
        $data[] = $totalCount;

        // Ausgabe als JSON
        return new JsonResponse([
            'message' => 'Value from the Year: ' . $year,
            'state' => 200,
            'item' => [
                'labels' => $labels,
                'datasets' => [
                    'label' => $this->translator->trans('generals.months.label') . $year,
                    'data' => $data,
                    'backgroundColor' => ["#D8D8D8", "#A9E2F3", "#0040FF", "#F781F3", "#9FF781", "#04B431", "#F7FE2E", "#FF0000", "#8000FF", "#FF8000", "#8A4B08", "#000000", "#5F4C0B"]
                ]
            ]
        ]);
    }

    /**
     * Read the Entry with ID from the Database
     *
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     * @throws JsonException
     */
    #[IsGranted('ROLE_MY_INCOMING_ALERT_READ')]
    #[Route('/get-by-id/{id}', name: '_get_by_id', methods: ['GET'])]
    public function getById(SerializerInterface $serializer, Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Read the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id') . " from the Database!");
        // Auslesen des Einträges
        $items = $this->incomingAlertRepository->find($request->get('id'));
        // Objekte Konvertieren
        $serialize = $serializer->serialize(
            $items,
            'json',
            ['groups' => ['dataByIdIncomingAlert', 'alertAddress', 'federalState', 'department'], AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT => 2]
        );
        // Ausgabe als JSON
        return new JsonResponse(
            ['item' => json_decode($serialize, false, 512, JSON_THROW_ON_ERROR)]
        );
    }

    /**
     * Create a new Entry in the Database
     *
     * @param Request $request
     * @return Response
     */
    #[IsGranted(new Expression('is_granted("ROLE_MY_INCOMING_ALERT_NEW") or is_granted("ROLE_ALL_INCOMING_ALERT_NEW")'))]
    #[Route('/new', name: '_new', methods: ['POST'])]
    public function new(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Create a new " . self::LOGGER_ONE_ENTRY);
        // Übermittelte Daten
        $requestEntry = $request->toArray();

        // Anlegen eines neuen Eintrages
        $item = (new IncomingAlert())
            ->setFederalState($this->federalStateRepository->find($requestEntry['federalState']['id']))
            ->setDepartment($this->departmentRepository->find($requestEntry['department']['id']))
            ->setAddress($this->alertAddressRepository->find($requestEntry['address']['id']))
            ->setNumberControlCenter(trim((string)$requestEntry['numberControlCenter']))
            ->setKeywordShort(trim((string)$requestEntry['keywordShort']))
            ->setKeywordLong(trim((string)$requestEntry['keywordLong']))
            ->setLocation(trim((string)$requestEntry['location']))
            ->setOtherAddressInformation(trim((string)$requestEntry['otherAddressInformation']))
            ->setPersonPatient(trim((string)$requestEntry['personPatient']))
            ->setText(trim((string)$requestEntry['text']))
            ->setIncomingDateTime(new DateTimeImmutable())
            ->setMessageComplete(trim((string)$requestEntry['messageComplete']))
            ->setIsVisibility((bool)$requestEntry['isVisibility'])
            ->setIsVisibilityOperationReport((bool)$requestEntry['isVisibilityOperationReport'])
            ->setComment(trim((string)$requestEntry['comment']))
            ->setCreatedFrom($this->getUser()->getSurname() . ', ' . $this->getUser()->getFirstname())
            ->setCreatedAt(new DateTimeImmutable());

        // Prüfen von doppelten Einträgen sowie leeren Feldern, ...
        return $this->checkEntity->validation($item);
    }

    /**
     * Update the Entry with ID in the Database
     *
     * @param Request $request
     * @return Response
     */
    #[IsGranted('ROLE_MY_INCOMING_ALERT_EDIT')]
    #[Route('/edit/{id}', name: '_edit', methods: ['POST'])]
    public function edit(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Update the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id'));
        // Suchen des Eintrages in der Datenbank
        $item = $this->incomingAlertRepository->find($request->get('id'));
        // Übermittelte Daten
        $requestEntry = $request->toArray();

        if ($item) {
            $item
                ->setFederalState($this->federalStateRepository->find($requestEntry['federalState']['id']))
                ->setDepartment($this->departmentRepository->find($requestEntry['department']['id']))
                ->setAddress($this->alertAddressRepository->find($requestEntry['address']['id']))
                ->setNumberControlCenter(trim((string)$requestEntry['numberControlCenter']))
                ->setKeywordShort(trim((string)$requestEntry['keywordShort']))
                ->setKeywordLong(trim((string)$requestEntry['keywordLong']))
                ->setLocation(trim((string)$requestEntry['location']))
                ->setOtherAddressInformation(trim((string)$requestEntry['otherAddressInformation']))
                ->setPersonPatient(trim((string)$requestEntry['personPatient']))
                ->setText(trim((string)$requestEntry['text']))
                ->setIncomingDateTime(new DateTimeImmutable())
                ->setMessageComplete(trim((string)$requestEntry['messageComplete']))
                ->setIsVisibility((bool)$requestEntry['isVisibility'])
                ->setIsVisibilityOperationReport((bool)$requestEntry['isVisibilityOperationReport'])
                ->setComment(trim((string)$requestEntry['comment']))
                ->setUpdatedFrom($this->getUser()->getSurname() . ', ' . $this->getUser()->getFirstname())
                ->setUpdatedAt(new DateTimeImmutable());
        }

        // Prüfen von doppelten Einträgen sowie leeren Feldern, ...
        return $this->checkEntity->validation($item);
    }

    /**
     * Delete the Entry with ID in the Database
     *
     * @param Request $request
     * @return Response
     */
    #[IsGranted('ROLE_MY_INCOMING_ALERT_DELETE')]
    #[Route('/delete/{id}', name: '_delete', methods: ['POST'])]
    public function delete(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Delete the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id'));
        // Suchen des Eintrages in der Datenbank
        $item = $this->incomingAlertRepository->find($request->get('id'));
        // Prüfen, ob der Eintrag vorhanden ist, ...
        return $this->checkEntity->validationDelete($item);
    }
}
