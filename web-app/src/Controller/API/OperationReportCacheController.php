<?php
declare(strict_types=1);

namespace App\Controller\API;

use App\Entity\OperationReportCache;
use App\Repository\DepartmentRepository;
use App\Repository\OperationReportCacheRepository;
use App\Service\CheckEntityService;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\QueryException;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/api/operation-report-cache', name: 'api_operation_report_cache')]
#[IsGranted('ROLE_USER')]
class OperationReportCacheController extends AbstractController
{

    private const LOGGER_ALL_ENTRIES = "Operation Report Caches";
    private const LOGGER_ONE_ENTRY = "Operation Report Cache";

    public function __construct(
        public LoggerInterface                $logger,
        public CheckEntityService             $checkEntity,
        public EntityManagerInterface         $entityManager,
        public TranslatorInterface            $translator,
        public DepartmentRepository           $departmentRepository,
        public OperationReportCacheRepository $operationReportCacheRepository
    )
    {
    }

    /**
     * Read all Entries from the Database
     *
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     * @throws JsonException
     * @throws QueryException
     */
    #[Route('s', name: 's', methods: ['GET'])]
    #[IsGranted('ROLE_OPERATION_REPORT_READ')]
    public function readAll(SerializerInterface $serializer, Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Read all " . self::LOGGER_ALL_ENTRIES . " from the Database!");
        // Filtern, nur die eigenen Daten anzeigen
        $criteria = ["department" => $this->getUser()->getDepartment()->getId()];

        if ($this->isGranted('ROLE_FULL_ADMINISTRATOR')) {
            $criteria = [];
        }

        // Auslesen des Einträges
        $items = $this->operationReportCacheRepository->findBy($criteria, ['createdAt' => 'DESC']);
        // Objekte Konvertieren
        $serialize = $serializer->serialize(
            $items,
            'json',
            ['groups' => ['operationReportCaches'], AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT => 2]
        );
        // Ausgabe als JSON
        return new JsonResponse([
            'items' => json_decode($serialize, false, 512, JSON_THROW_ON_ERROR),
            'filterCount' => count($items),
            'totalCount' => count($items)
        ]);
    }

    /**
     * Read the Entry with ID from the Database
     *
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     * @throws JsonException
     */
    #[Route('/get-by-id/{id}', name: '_get_by_id', methods: ['GET'])]
    #[IsGranted('ROLE_OPERATION_REPORT_READ')]
    public function getById(SerializerInterface $serializer, Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Read the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id') . " from the Database!");
        // Auslesen des Einträges
        $items = $this->operationReportCacheRepository->find($request->get('id'));
        // Objekte Konvertieren
        $serialize = $serializer->serialize(
            $items,
            'json',
            ['groups' => ['dataById'], AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT => 2]
        );
        // Ausgabe als JSON
        return new JsonResponse(
            ['item' => json_decode($serialize, false, 512, JSON_THROW_ON_ERROR)]
        );
    }

    /**
     * Create a new Entry in the Database
     *
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    #[Route('/new', name: '_new', methods: ['POST'])]
    #[IsGranted('ROLE_OPERATION_REPORT_NEW')]
    public function new(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Create a new " . self::LOGGER_ONE_ENTRY);
        // Übermittelte Daten
        $requestEntry = $request->toArray();

        // Anlegen eines neuen Eintrages
        $item = (new OperationReportCache())
            ->setDepartment($this->getUser()->getDepartment())
            ->setStore($requestEntry)
            ->setComment((string)$requestEntry['comment'])
            ->setCreatedFrom($this->getUser()->getSurname() . ', ' . $this->getUser()->getFirstname())
            ->setCreatedAt(new DateTimeImmutable());

        return $this->checkEntity->validation($item, 'operationReportCaches');
    }

    /**
     * Update the Entry with ID in the Database
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/edit/{id}', name: '_edit', methods: ['POST'])]
    #[IsGranted('ROLE_OPERATION_REPORT_NEW')]
    public function edit(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Update the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id'));
        // Suchen des Eintrages in der Datenbank
        $item = $this->operationReportCacheRepository->find($request->get('id'));
        // Übermittelte Daten
        $requestEntry = $request->toArray();

        if ($item) {
            $item
                ->setDepartment($this->getUser()->getDepartment())
                ->setStore($requestEntry)
                ->setComment((string)$requestEntry['comment'])
                ->setUpdatedFrom($this->getUser()->getSurname() . ', ' . $this->getUser()->getFirstname())
                ->setUpdatedAt(new DateTimeImmutable());
        }

        // Prüfen von doppelten Einträgen sowie leeren Feldern, ...
        return $this->checkEntity->validation($item, 'operationReportCaches');
    }

    /**
     * Delete the Entry with ID in the Database
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/delete/{id}', name: '_delete', methods: ['POST'])]
    #[IsGranted('ROLE_OPERATION_REPORT_CACHE_DELETE')]
    public function delete(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Delete the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id'));
        // Suchen des Eintrages in der Datenbank
        $item = $this->operationReportCacheRepository->find($request->get('id'));
        // Prüfen, ob der Eintrag vorhanden ist, ...
        return $this->checkEntity->validationDelete($item);
    }
}
