<?php
declare(strict_types=1);
namespace App\Controller\API;

use App\Entity\OperationTypeCategory;
use App\Repository\DepartmentRepository;
use App\Repository\OperationTypeCategoryRepository;
use App\Service\CheckEntityService;
use App\Service\DataTableService;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\QueryException;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/api/operation-type-categor', name: 'api_operation_type_categor')]
#[IsGranted('ROLE_USER')]
class OperationTypeCategoryController extends AbstractController
{

    private const LOGGER_ALL_ENTRIES = "Operation Type Categories";
    private const LOGGER_ONE_ENTRY = "Operation Type Category";

    public function __construct(
        public LoggerInterface        $logger,
        public CheckEntityService     $checkEntity,
        public EntityManagerInterface $entityManager,
        public TranslatorInterface    $translator,
        public OperationTypeCategoryRepository $operationTypeCategoryRepository,
        public DepartmentRepository   $departmentRepository,
        public DataTableService       $dataTableService)
    {
    }

    /**
     * Read all Entries from the Database
     *
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     * @throws JsonException
     * @throws QueryException
     */
    #[Route('ies', name: 'ies', methods: ['GET'])]
    #[IsGranted('ROLE_OPERATION_TYPE_CATEGORY_READ')]
    public function readAll(SerializerInterface $serializer, Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Read all " . self::LOGGER_ALL_ENTRIES . " from the Database!");

        $param = [
            "department" => $this->getUser()->getDepartment()->getId(),
            "isFullAdmin" => $this->isGranted('ROLE_FULL_ADMINISTRATOR')
        ];

        // Auslesen aller Einträge
        $items = $this->operationTypeCategoryRepository->findByDataTableService($request, $this->dataTableService, $param, $this->isGranted("ROLE_FULL_ADMINISTRATOR"));
        // Objekte Konvertieren
        $serialize = $serializer->serialize(
            $items['items'],
            'json',
            ['groups' => ['operationTypeCategories', 'operationType', 'department'], AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT => 2]
        );
        // Ausgabe als JSON
        return new JsonResponse([
            'items' => json_decode($serialize, false, 512, JSON_THROW_ON_ERROR),
            'filterCount' => $items['filterCount'],
            'totalCount' => $items['totalCount']
        ]);
    }

    /**
     * Read all Entries from the Database for Dropdown
     *
     * @param SerializerInterface $serializer
     * @return Response
     * @throws JsonException
     */
    #[Route('y/get-for-dropdown', name: 'y_get_for_dropdown', methods: ['GET'])]
    #[IsGranted('ROLE_OPERATION_TYPE_CATEGORY_READ')]
    public function getForDropdown(SerializerInterface $serializer): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Read the all " . self::LOGGER_ALL_ENTRIES . " from the Database!");

        // Filtern, nur die eigenen Daten anzeigen
        $criteria = [
            "department" => $this->getUser()->getDepartment()->getId(),
            "isVisibility" => true
        ];

        if ($this->isGranted('ROLE_FULL_ADMINISTRATOR')) {
            $criteria = [
                "isVisibility" => true
            ];
        }

        // Auslesen des Einträges
        $items = $this->operationTypeCategoryRepository->findBy($criteria, ['abbreviation' => 'ASC']);
        // Objekte Konvertieren
        $serialize = $serializer->serialize(
            $items,
            'json',
            ['groups' => ['dataByIdOperationTypeCategory', 'operationType'], AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT => 2]
        );
        // Ausgabe als JSON
        return new JsonResponse(
            ['items' => json_decode($serialize, false, 512, JSON_THROW_ON_ERROR)]
        );
    }

    /**
     * Read the Entry with ID from the Database
     *
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     * @throws JsonException
     */
    #[Route('y/get-by-id/{id}', name: 'y_get_by_id', methods: ['GET'])]
    #[IsGranted('ROLE_OPERATION_TYPE_CATEGORY_READ')]
    public function getById(SerializerInterface $serializer, Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Read the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id') . " from the Database!");
        // Auslesen des Einträges
        $items = $this->operationTypeCategoryRepository->find($request->get('id'));
        // Objekte Konvertieren
        $serialize = $serializer->serialize(
            $items,
            'json',
            ['groups' => ['dataByIdOperationTypeCategory', 'operationTypeCategory', 'department'], AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT => 2]
        );
        // Ausgabe als JSON
        return new JsonResponse(
            ['item' => json_decode($serialize, false, 512, JSON_THROW_ON_ERROR)]
        );
    }

    /**
     * Create a new Entry in the Database
     *
     * @param Request $request
     * @return Response
     */
    #[Route('y/new', name: 'y_new', methods: ['POST'])]
    #[IsGranted('ROLE_OPERATION_TYPE_CATEGORY_NEW')]
    public function new(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Create a new " . self::LOGGER_ONE_ENTRY);
        // Übermittelte Daten
        $requestEntry = $request->toArray();

        // Anlegen eines neuen Eintrages
        $item = (new OperationTypeCategory())
            ->setDepartment($this->departmentRepository->find($requestEntry['department']['id']))
            ->setAbbreviation(trim((string)$requestEntry['abbreviation']))
            ->setDescription(trim((string)$requestEntry['description']))
            ->setIsVisibility((bool)$requestEntry['isVisibility'])
            ->setSorting((int)$requestEntry['sorting'])
            ->setComment(trim((string)$requestEntry['comment']))
            ->setCreatedFrom($this->getUser()->getSurname(). ', '. $this->getUser()->getFirstname())
            ->setCreatedAt(new DateTimeImmutable());

        // Prüfen von doppelten Einträgen sowie leeren Feldern, ...
        return $this->checkEntity->validation($item);
    }

    /**
     * Update the Entry with ID in the Database
     *
     * @param Request $request
     * @return Response
     */
    #[Route('y/edit/{id}', name: 'y_edit', methods: ['POST'])]
    #[IsGranted('ROLE_OPERATION_TYPE_CATEGORY_EDIT')]
    public function edit(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Update the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id'));
        // Suchen des Eintrages in der Datenbank
        $item = $this->operationTypeCategoryRepository->find($request->get('id'));
        // Übermittelte Daten
        $requestEntry = $request->toArray();

        if ($item) {
            $item
                ->setDepartment($this->departmentRepository->find($requestEntry['department']['id']))
                ->setAbbreviation(trim((string)$requestEntry['abbreviation']))
                ->setDescription(trim((string)$requestEntry['description']))
                ->setIsVisibility((bool)$requestEntry['isVisibility'])
                ->setSorting((int)$requestEntry['sorting'])
                ->setComment(trim((string)$requestEntry['comment']))
                ->setUpdatedFrom($this->getUser()->getSurname(). ', '. $this->getUser()->getFirstname())
                ->setUpdatedAt(new DateTimeImmutable());
        }

        // Prüfen von doppelten Einträgen sowie leeren Feldern, ...
        return $this->checkEntity->validation($item);
    }

    /**
     * Delete the Entry with ID in the Database
     *
     * @param Request $request
     * @return Response
     */
    #[Route('y/delete/{id}', name: 'y_delete', methods: ['POST'])]
    #[IsGranted('ROLE_OPERATION_TYPE_CATEGORY_DELETE')]
    public function delete(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Delete the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id'));
        // Suchen des Eintrages in der Datenbank
        $item = $this->operationTypeCategoryRepository->find($request->get('id'));
        // Prüfen, ob der Eintrag vorhanden ist, ...
        return $this->checkEntity->validationDelete($item);
    }
}
