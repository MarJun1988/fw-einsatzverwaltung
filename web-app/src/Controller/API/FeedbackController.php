<?php
declare(strict_types=1);

namespace App\Controller\API;

use App\Entity\Feedback;
use App\Repository\DepartmentRepository;
use App\Repository\FederalStateRepository;
use App\Repository\FeedbackRepository;
use App\Service\CheckEntityService;
use App\Service\DataTableService;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use JsonException;
use phpDocumentor\Reflection\DocBlock\Description;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/api/feedback', name: 'api_feedback')]
#[IsGranted('ROLE_USER')]
class FeedbackController extends AbstractController
{
    private const LOGGER_ALL_ENTRIES = "Feedbacks";
    private const LOGGER_ONE_ENTRY = "Feedback";


    public function __construct(
        public LoggerInterface        $logger,
        public CheckEntityService     $checkEntity,
        public EntityManagerInterface $entityManager,
        public TranslatorInterface    $translator,
        public FederalStateRepository $federalStateRepository,
        public DepartmentRepository   $departmentRepository,
        public FeedbackRepository     $feedbackRepository,
        public DataTableService       $dataTableService)
    {
    }

    /**
     * Read all Entries from the Database
     *
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     * @throws JsonException
     */
    #[Route('s', name: 's', methods: ['GET'])]
    #[IsGranted('ROLE_FEEDBACK_READ')]
    public function readAll(SerializerInterface $serializer, Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Read all " . self::LOGGER_ALL_ENTRIES . " from the Database!");

        // Auslesen aller Einträge
        $items = $this->feedbackRepository->findByDataTableService($request, $this->dataTableService);
        // Objekte Konvertieren
        $serialize = $serializer->serialize(
            $items['items'],
            'json',
            ['groups' => ['feedbacks', 'federalState', 'department'], AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT => 2]
        );
        // Ausgabe als JSON
        return new JsonResponse([
            'items' => json_decode($serialize, false, 512, JSON_THROW_ON_ERROR),
            'filterCount' => $items['filterCount'],
            'totalCount' => $items['totalCount']
        ]);
    }

    /**
     * Read the Entry with ID from the Database
     *
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     * @throws JsonException
     */
    #[Route('/get-by-id/{id}', name: '_get_by_id', methods: ['GET'])]
    #[IsGranted('ROLE_FEEDBACK_READ')]
    public function getById(SerializerInterface $serializer, Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Read the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id') . " from the Database!");
        // Auslesen des Einträges
        $items = $this->feedbackRepository->find($request->get('id'));
        // Objekte Konvertieren
        $serialize = $serializer->serialize(
            $items,
            'json',
            ['groups' => ['dataByIdFeedback', 'federalState', 'department'], AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT => 2]
        );
        // Ausgabe als JSON
        return new JsonResponse(
            ['item' => json_decode($serialize, false, 512, JSON_THROW_ON_ERROR)]
        );
    }

    /**
     * Create a new Entry in the Database
     *
     * @param Request $request
     * @param Client $client
     * @return Response
     */
    #[Route('/new', name: '_new', methods: ['POST'])]
    #[IsGranted('ROLE_USER')]
    public function new(Request $request, Client $client): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Create a new " . self::LOGGER_ONE_ENTRY);
        // Übermittelte Daten
        $requestEntry = $request->toArray();

        // Anlegen eines neuen Eintrages
        $item = (new Feedback())
            ->setFederalState($this->getUser()->getFederalState())
            ->setDepartment($this->getUser()->getDepartment())
            ->setTitle(trim((string)$requestEntry['title']))
            ->setMessage(trim((string)$requestEntry['message']))
            ->setComment(trim((string)$requestEntry['comment']))
            ->setCreatedFrom($this->getUser()->getSurname() . ', ' . $this->getUser()->getFirstname())
            ->setCreatedAt(new DateTimeImmutable());

        $this->sendTelegramm(trim((string)$requestEntry['message']), $client);

        // Prüfen von doppelten Einträgen sowie leeren Feldern, ...
        return $this->checkEntity->validation($item);
    }

    /**
     * Update the Entry with ID in the Database
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/edit/{id}', name: '_edit', methods: ['POST'])]
    #[IsGranted('ROLE_FEEDBACK_EDIT')]
    public function edit(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Update the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id'));
        // Suchen des Eintrages in der Datenbank
        $item = $this->feedbackRepository->find($request->get('id'));
        // Übermittelte Daten
        $requestEntry = $request->toArray();

        if ($item) {
            $item
                ->setTitle(trim((string)$requestEntry['title']))
                ->setMessage(trim((string)$requestEntry['message']))
                ->setComment(trim((string)$requestEntry['comment']))
                ->setUpdatedFrom($this->getUser()->getSurname() . ', ' . $this->getUser()->getFirstname())
                ->setUpdatedAt(new DateTimeImmutable());
        }

        // Prüfen von doppelten Einträgen sowie leeren Feldern, ...
        return $this->checkEntity->validation($item);
    }

    /**
     * Delete the Entry with ID in the Database
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/delete/{id}', name: '_delete', methods: ['POST'])]
    #[IsGranted('ROLE_FEEDBACK_DELETE')]
    public function delete(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Delete the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id'));
        // Suchen des Eintrages in der Datenbank
        $item = $this->feedbackRepository->find($request->get('id'));
        // Prüfen, ob der Eintrag vorhanden ist, ...
        return $this->checkEntity->validationDelete($item);
    }


    public function sendTelegramm(string $message, Client $client): Response
    {

        $telegramBotToken = $_ENV['TELEGRAM_BOT_TOKEN'];
        $telegramChatId = $_ENV['TELEGRAM_CHAT_ID'];

        try {
            $response = $client->request('GET', "https://api.telegram.org/bot{$telegramBotToken}/sendMessage", [
                'query' => [
                    'chat_id' => $telegramChatId,
                    'text' => "Fw Einsatzverwaltung: $message"
                ]
            ]);

            return new Response($message);
        } catch (\Exception $e) {
            return new Response('An error occurred: ' . $e->getMessage(), 500);
        }
    }
}
