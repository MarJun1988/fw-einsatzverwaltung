<?php
declare(strict_types=1);

namespace App\Controller\API;

use App\Entity\General;
use App\Repository\DepartmentRepository;
use App\Repository\GeneralRepository;
use App\Service\CheckEntityService;
use App\Service\DataTableService;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\QueryException;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/api/general', name: 'api_general')]
class GeneralController extends AbstractController
{

    private const LOGGER_ALL_ENTRIES = "Generals";
    private const LOGGER_ONE_ENTRY = "General";

    public function __construct(
        public LoggerInterface        $logger,
        public CheckEntityService     $checkEntity,
        public EntityManagerInterface $entityManager,
        public TranslatorInterface    $translator,
        public GeneralRepository      $generalRepository,
        public DepartmentRepository   $departmentRepository,
        public DataTableService       $dataTableService)
    {
    }

    /**
     * Read all Entries from the Database
     *
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     * @throws JsonException
     * @throws QueryException
     */
    #[Route('s', name: 's', methods: ['GET'])]
    public function readAll(SerializerInterface $serializer, Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Read all " . self::LOGGER_ALL_ENTRIES . " from the Database!");
        // Auslesen aller Einträge
        $items = $this->generalRepository->findByDataTableService($request, $this->dataTableService);
        if ($this->getUser()) {
            $items = $this->generalRepository->findBy(['department' => $this->getUser()->getDepartment()]);
        } else {
            $items = $this->generalRepository->findAll();
        }
        // Objekte Konvertieren
        $serialize = $serializer->serialize(
            $items,
            'json',
            ['groups' => 'generals']
        );
        // Ausgabe als JSON
        return new JsonResponse([
            'items' => json_decode($serialize, false, 512, JSON_THROW_ON_ERROR),
        ]);
    }

    /**
     * Read the Entry with ID from the Database
     *
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     * @throws JsonException
     */
    #[Route('/get-by-id/{id}', name: '_get_by_id', methods: ['GET'])]
    public function getById(SerializerInterface $serializer, Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Read the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id') . " from the Database!");
        // Auslesen des Einträges
        $items = $this->generalRepository->find($request->get('id'));
        // Objekte Konvertieren
        $serialize = $serializer->serialize(
            $items,
            'json',
            ['groups' => 'dataByIdGeneral']
        );
        // Ausgabe als JSON
        return new JsonResponse(
            ['item' => json_decode($serialize, false, 512, JSON_THROW_ON_ERROR)]
        );
    }

    /**
     * Create a new Entry in the Database
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/new', name: '_new', methods: ['POST'])]
    public function new(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Create a new " . self::LOGGER_ONE_ENTRY);
        // Übermittelte Daten
        $requestEntry = $request->toArray();

        // Anlegen eines neuen Eintrages
        $item = (new General())
            ->setDepartment($this->getUser()->getDepartment())
            ->setName(trim((string)$requestEntry['name']))
            ->setValue(trim((string)$requestEntry['value']))
            ->setDescription(trim((string)$requestEntry['description']))
            ->setIsVisibility((bool)$requestEntry['isVisibility'])
            ->setComment(trim((string)$requestEntry['comment']))
            ->setCreatedFrom($this->getUser()->getSurname() . ', ' . $this->getUser()->getFirstname())
            ->setCreatedAt(new DateTimeImmutable());

        // Prüfen von doppelten Einträgen sowie leeren Feldern, ...
        return $this->checkEntity->validation($item);
    }

    /**
     * Update the Entry with ID in the Database
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/edit/{id}', name: '_edit', methods: ['POST'])]
    public function edit(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Update the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id'));
        // Suchen des Eintrages in der Datenbank
        $item = $this->generalRepository->find($request->get('id'));
        // Übermittelte Daten
        $requestEntry = $request->toArray();

        if ($item) {
            $item
                ->setDepartment($this->getUser()->getDepartment())
                ->setName(trim((string)$requestEntry['name']))
                ->setValue(trim((string)$requestEntry['value']))
                ->setDescription(trim((string)$requestEntry['description']))
                ->setIsVisibility((bool)$requestEntry['isVisibility'])
                ->setComment(trim((string)$requestEntry['comment']))
                ->setUpdatedFrom($this->getUser()->getSurname() . ', ' . $this->getUser()->getFirstname())
                ->setUpdatedAt(new DateTimeImmutable());
        }

        // Prüfen von doppelten Einträgen sowie leeren Feldern, ...
        return $this->checkEntity->validation($item);
    }

    /**
     * Delete the Entry with ID in the Database
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/delete/{id}', name: '_delete', methods: ['POST'])]
    public function delete(Request $request): Response
    {
        // Ausgabe in der Logdatei
        $this->logger->info("Delete the " . self::LOGGER_ONE_ENTRY . " with ID " . $request->get('id'));
        // Suchen des Eintrages in der Datenbank
        $item = $this->generalRepository->find($request->get('id'));
        // Prüfen, ob der Eintrag vorhanden ist, ...
        return $this->checkEntity->validationDelete($item);
    }
}
