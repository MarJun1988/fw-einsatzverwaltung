<?php
// src/Twig/AppExtension.php

namespace App\Twig;

use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Security\Core\Security;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    private $containerBag;
    private $security;

    public function __construct(ContainerBagInterface $containerBag, Security $security)
    {
        $this->containerBag = $containerBag;
        $this->security = $security;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_inherited_roles', [$this, 'getInheritedRoles']),
        ];
    }

    public function getInheritedRoles(): array
    {
        $token = $this->security->getToken();

        if (!$token) {
            return [];
        }

        $roles = $token->getRoleNames();
        $inheritedRoles = [];

        foreach ($roles as $role) {
            $inheritedRoles = array_merge($inheritedRoles, $this->getInheritedRolesForRole($role));
        }

        return array_unique($inheritedRoles);
    }

    private function getInheritedRolesForRole(string $role): array
    {
        // Hier implementiere Logik, um ererbte Rollen für eine bestimmte Rolle abzurufen
        // Du kannst die Role Hierarchy oder andere Mechanismen verwenden

        // Beispiel (wenn du die Role Hierarchy verwenden möchtest):
        $roleHierarchy = $this->containerBag->get('security.role_hierarchy.roles');
        return $roleHierarchy[$role] ?? [];

        // Beachte, dass die genaue Implementierung je nach deinem Symfony-Setup variieren kann
        return [];
    }
}