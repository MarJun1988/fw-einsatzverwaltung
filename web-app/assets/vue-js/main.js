import {createApp} from 'vue'
/* Store Pinia */
import {createPinia} from 'pinia'
import de from './localization/de.json'

/* Vue Router */
import router from './router'

import App from './App.vue'
// PDf
import htmlToPdf from 'vue3-html2pdf'

// BalmUi
import BalmUI from 'balm-ui'; // Official Google Material Components
import 'balm-ui-css';

// PrimeVue Components
import Button from "primevue/button"
import MegaMenu from 'primevue/megamenu';
import Menubar from 'primevue/menubar';
import Tree from 'primevue/tree';
import TabView from 'primevue/tabview';
import TabPanel from 'primevue/tabpanel';
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';
import ColumnGroup from 'primevue/columngroup'; // optional
import Row from 'primevue/row'; // optional
import InputSwitch from 'primevue/inputswitch';
import InputText from 'primevue/inputtext';
import InputMask from 'primevue/inputmask';
import InputNumber from 'primevue/inputnumber';
import Dialog from 'primevue/dialog';
import Toast from 'primevue/toast';
import Message from 'primevue/message';
import Dropdown from 'primevue/dropdown';
import MultiSelect from 'primevue/multiselect';
import Calendar from 'primevue/calendar';
import TriStateCheckbox from 'primevue/tristatecheckbox';
import Checkbox from 'primevue/checkbox';
import ColorPicker from 'primevue/colorpicker';
import ToggleButton from 'primevue/togglebutton';
import Card from 'primevue/card';
import Steps from 'primevue/steps';
import Password from 'primevue/password';
import Sidebar from 'primevue/sidebar';
import ProgressSpinner from 'primevue/progressspinner';
import ProgressBar from 'primevue/progressbar';
import AutoComplete from 'primevue/autocomplete';
import ConfirmDialog from 'primevue/confirmdialog';
import ToastService from 'primevue/toastservice';
import Textarea from 'primevue/textarea';
import VirtualScroller from 'primevue/virtualscroller';
import Breadcrumb from 'primevue/breadcrumb';
import Listbox from 'primevue/listbox';
import Skeleton from 'primevue/skeleton';
import SplitButton from 'primevue/splitbutton';
import Tooltip from 'primevue/tooltip';
import Chart from 'primevue/chart';

// PrimeVue
// Flex
import "/node_modules/primeflex/primeflex.css";
// theme
import "../styles/design-feuerwehr.css";
// core
import "primevue/resources/primevue.min.css";
// icons
import "primeicons/primeicons.css";
// Default Style
import "../styles/_default.scss"
import BalmUIPlus from "balm-ui/src/scripts/balm-ui-plus";
import PrimeVue from "primevue/config";
import ConfirmationService from "primevue/confirmationservice";

const app = createApp(App)
const pinia = createPinia()

app.component('Button', Button);
app.component('Dialog', Dialog);
app.component('MegaMenu', MegaMenu);
app.component('Menubar', Menubar);
app.component('Tree', Tree);
app.component('TabView', TabView);
app.component('TabPanel', TabPanel);
app.component('DataTable', DataTable);
app.component('Column', Column);
app.component('ColumnGroup', ColumnGroup);
app.component('Row', Row);
app.component('InputMask', InputMask);
app.component('InputNumber', InputNumber);
app.component('InputSwitch', InputSwitch);
app.component('InputText', InputText);
app.component('Toast', Toast);
app.component('Message', Message);
app.component('ConfirmDialog', ConfirmDialog);
app.component('Dropdown', Dropdown);
app.component('MultiSelect', MultiSelect);
app.component('Calendar', Calendar);
app.component('TriStateCheckbox', TriStateCheckbox);
app.component('Checkbox', Checkbox);
app.component('ColorPicker', ColorPicker);
app.component('ToggleButton', ToggleButton);
app.component('Card', Card);
app.component('Steps', Steps);
app.component('Password', Password);
app.component('Sidebar', Sidebar);
app.component('ProgressSpinner', ProgressSpinner);
app.component('ProgressBar', ProgressBar);
app.component('AutoComplete', AutoComplete);
app.component('Textarea', Textarea);
app.component('VirtualScroller', VirtualScroller);
app.component('Breadcrumb', Breadcrumb);
app.component('Listbox', Listbox);
app.component('Skeleton', Skeleton);
app.component('SplitButton', SplitButton);
app.component('Chart', Chart);

app.use(pinia)
app.use(router)
app.use(BalmUI, {
    $theme: {
        'primary': "#ba0000",
        'on-primary': "#fff",
    }
});
app.use(BalmUIPlus);
app.use(ConfirmationService);
app.use(ToastService);
app.use(PrimeVue, {ripple: true, locale: de.de})

app.mount('#app')
