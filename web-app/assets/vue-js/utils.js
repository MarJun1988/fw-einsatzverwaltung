import {useLoggedUserStore} from "@/stores/loggedUser";
import {storeToRefs} from "pinia";
import {mainMenuRoutes as authenticationNames} from "@/router/default";

// Store - angemeldete Nutzer
const storeLoggedUser = useLoggedUserStore();
const {loggedUser} = storeLoggedUser;

/**
 * Generate the OrderBy Objekt for the Symfony API
 * @param param
 * @returns {*[]}
 */
const generateOrderBy = (param) => {
    let order = [];

    param.forEach(sort => {
        order.push(`${sort.field};${sort.order === 1 ? 'ASC' : 'DESC'}`);
    })

    return order;
};

/**
 * Check the Required Rights from a Element
 * @param userRights
 * @param requiredRights
 * @returns {boolean}
 */
const checkUserRight = (userRights, requiredRights) => {
    let rolesArray = [];
    if (storeLoggedUser.loggedUser.rolesInherited) {
        let rolesKeys = Reflect.ownKeys(storeLoggedUser.loggedUser.rolesInherited);
        rolesArray = rolesKeys.reduce((arr, key) => {
            arr.push(storeLoggedUser.loggedUser.rolesInherited[key]);
            return arr;
        }, []);
    }
    if ((userRights && requiredRights) &&
        (userRights.length > 0 && requiredRights.length > 0)) {
        for (let i = 0; i < requiredRights.length; i++) {
            if (userRights.includes(requiredRights[i]) || rolesArray.includes(requiredRights[i]) || Array.from(loggedUser.rolesInherited).includes(requiredRights[i])) {
                return true
            }
        }
    }
    return false;
};

/**
 * Added Full Admin Fields to the Original Fields
 * @param fields
 * @param addedFields
 * @returns {*|(*)}
 */
const addedFullAdminFields = (fields, addedFields) => {
    let mergedObj = {};
    // Prüfen der Rechte, sowie testen ob inhalt vorhanden ist
    if (loggedUser && loggedUser.roles.includes('ROLE_FULL_ADMINISTRATOR') &&
        fields && Object.keys(fields).length && addedFields && Object.keys(addedFields).length) {
        mergedObj = {...addedFields, ...fields};
        return mergedObj;
    }
    return fields;
};

/**
 * Added Full Admin Fields to the Original Fields
 * @param fields
 * @param addedFields
 * @returns {*|(*)}
 */
const addedFullAdminColumns = (fields, addedFields) => {
    let copyFields = [...fields];
    let mergedObj = [];
    // Prüfen der Rechte, sowie testen ob inhalt vorhanden ist
    if (loggedUser && loggedUser.roles.includes('ROLE_FULL_ADMINISTRATOR') &&
        fields && fields.length > 0 && addedFields && addedFields.length > 0) {
        // Entfernen der ID Spalte
        fields.shift()
        // Zusammenfügen der zwei Arrays
        mergedObj = [...addedFields, ...fields];
        // Hinzufügen der ID Spalte
        mergedObj.unshift(copyFields[0])
        return mergedObj;
    }
    return fields;
};

/**
 * Check is the Current User logged In?
 * @param response
 */
const checkUserIsLoggedIn = (response) => {
    if (response && response.request && response.request.responseURL && response.request.responseURL.includes('login')) {
        window.location.href = `${authenticationNames.login}`;
    }
};

export {checkUserIsLoggedIn, generateOrderBy, checkUserRight, addedFullAdminFields, addedFullAdminColumns}