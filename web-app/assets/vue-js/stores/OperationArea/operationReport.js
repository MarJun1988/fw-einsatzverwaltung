import {defineStore, storeToRefs} from "pinia";
import {ref} from "vue";
import axios from "axios";
import {useApiStore} from "@/stores/api";
import {checkUserIsLoggedIn, generateOrderBy} from "@/utils";
import {useLoggedUserStore} from "@/stores/loggedUser";

let nameOfTheStore = `OperationReport`;

// Store - angemeldete Nutzer
const storeLoggedUser = useLoggedUserStore();
const {loggedUser} = storeToRefs(storeLoggedUser);

export const useOperationReportStore = defineStore(`store${nameOfTheStore}`, () => {
    // State
    const dataById = ref({
        id: null,
        federalState: {
            id: loggedUser.value && loggedUser.value.federalState && loggedUser.value.federalState.id ? loggedUser.value.federalState.id : ''
        },
        department: {
            id: loggedUser.value && loggedUser.value.department && loggedUser.value.department.id ? loggedUser.value.department.id : ''
        },
        operationReportDetail: {
            id: '',
            operationType: {},
            incomingDateTime: new Date(),
            startetAt: new Date(),
            endetAt: new Date(),
            numberControlCenter: '',
            keywordShort: '',
            keywordLong: '',
            location: '',
            otherAddressInformation: '',
            personPatient: '',
            text: '',
            messageComplete: '',
            postalCode: '',
            place: '',
            streetWithNumber: '',
            numberFireDepartment: '',
            numberPolice: '',
        },
        incomingAlert: {
            id: '',
            item: {
                id: null,
                federalState: {
                    id: ''
                },
                department: {
                    id: ''
                },
                address: {
                    id: ''
                },
                numberControlCenter: '',
                keywordShort: '',
                keywordLong: '',
                location: '',
                otherAddressInformation: '',
                personPatient: '',
                text: '',
                incomingDateTime: new Date(),
                messageComplete: '',
                isVisibilityOperationReport: true,
                isVisibility: true,
                comment: ''
            }
        },
        ownForces: '',
        otherTaskForces: '',
        tools: '',
        sessionStore: '',
        isVisibility: true,
        commentPrint: '',
        comment: ''
    });
    const dataByIdDefault = ref({
        id: null,
        federalState: {
            id: loggedUser.value && loggedUser.value.federalState && loggedUser.value.federalState.id ? loggedUser.value.federalState.id : ''
        },
        department: {
            id: loggedUser.value && loggedUser.value.department && loggedUser.value.department.id ? loggedUser.value.department.id : ''
        },
        operationReportDetail: {
            id: '',
            operationType: {},
            incomingDateTime: new Date(),
            startetAt: new Date(),
            endetAt: new Date(),
            numberControlCenter: '',
            keywordShort: '',
            keywordLong: '',
            location: '',
            otherAddressInformation: '',
            personPatient: '',
            text: '',
            messageComplete: '',
            postalCode: '',
            place: '',
            streetWithNumber: '',
            numberFireDepartment: '',
            numberPolice: '',
        },
        incomingAlert: {
            id: '',
            item: {
                id: null,
                federalState: {
                    id: ''
                },
                department: {
                    id: ''
                },
                address: {
                    id: ''
                },
                numberControlCenter: '',
                keywordShort: '',
                keywordLong: '',
                location: '',
                otherAddressInformation: '',
                personPatient: '',
                text: '',
                incomingDateTime: new Date(),
                messageComplete: '',
                isVisibilityOperationReport: true,
                isVisibility: true,
                comment: ''
            }
        },
        ownForces: '',
        otherTaskForces: '',
        tools: '',
        sessionStore: '',
        isVisibility: true,
        commentPrint: '',
        comment: ''
    });
    const dataForDropdown = ref([]);
    const dataForTable = ref([]);
    const dataTotalCount = ref(0);
    const dataFilterCount = ref(0);
    const isLoading = ref(false);
    const exportPdfPath = ref("");
    const operationReport = ref({
        id: null,
        loadFromCache: false,
        federalState: {
            id: ''
        },
        department: {
            id: ''
        },
        operationReportDetail: {
            id: '',
            operationType: {},
            incomingDateTime: new Date(),
            startetAt: new Date(),
            endetAt: new Date(),
            numberControlCenter: '',
            keywordShort: '',
            keywordLong: '',
            location: '',
            otherAddressInformation: '',
            personPatient: '',
            text: '',
            messageComplete: '',
            postalCode: '',
            place: '',
            streetWithNumber: '',
            numberFireDepartment: '',
            numberPolice: '',
        },
        incomingAlert: {
            id: '',
            item: {
                id: null,
                federalState: {
                    id: ''
                },
                department: {
                    id: ''
                },
                address: {
                    id: ''
                },
                numberControlCenter: '',
                keywordShort: '',
                keywordLong: '',
                location: '',
                otherAddressInformation: '',
                personPatient: '',
                text: '',
                incomingDateTime: new Date(),
                messageComplete: '',
                isVisibilityOperationReport: true,
                isVisibility: true,
                comment: ''
            }
        },
        ownForces: {
            vehicles: [],
            members: [],
            vehiclesWithMemberAndFunction: []
        },
        otherTaskForces: [],
        tools: {
            allTools: [],
            toolsWithNumbers: []
        },
        commentPrint: '',
        comment: ''
    });
    const operationReportDefault = ref({
        id: null,
        loadFromCache: false,
        federalState: {
            id: ''
        },
        department: {
            id: ''
        },
        operationReportDetail: {
            id: '',
            operationType: {},
            incomingDateTime: new Date(),
            startetAt: new Date(),
            endetAt: new Date(),
            numberControlCenter: '',
            keywordShort: '',
            keywordLong: '',
            location: '',
            otherAddressInformation: '',
            personPatient: '',
            text: '',
            messageComplete: '',
            postalCode: '',
            place: '',
            streetWithNumber: '',
            numberFireDepartment: '',
            numberPolice: '',
        },
        incomingAlert: {
            id: '',
            item: {
                id: null,
                federalState: {
                    id: ''
                },
                department: {
                    id: ''
                },
                address: {
                    id: ''
                },
                numberControlCenter: '',
                keywordShort: '',
                keywordLong: '',
                location: '',
                otherAddressInformation: '',
                personPatient: '',
                text: '',
                incomingDateTime: new Date(),
                messageComplete: '',
                isVisibilityOperationReport: true,
                isVisibility: true,
                comment: ''
            }
        },
        ownForces: {
            vehicles: [],
            members: [],
            vehiclesWithMemberAndFunction: []
        },
        otherTaskForces: [],
        tools: {
            allTools: [],
            toolsWithNumbers: []
        },
        commentPrint: '',
        comment: ''
    });
    const lastNumberFireDepartment = ref({
        lastNumber: '',
        next: '',
        count: '',
    })
    // Getters

    // Actions
    /**
     * Load all Entries from the Database (api)
     * @param lazyParams
     * @returns {Promise<void>}
     */
    const loadDataForTable = async (lazyParams = null) => {
        await setIsLoading(true);
        let para = {};
        let url = `${useApiStore().operationReports}`;

        if (lazyParams) {
            para = {
                limit: lazyParams.rows, offset: lazyParams.first, order: [], filter: []
            };

            if (lazyParams.multiSortMeta) {
                para.order = generateOrderBy(lazyParams.multiSortMeta);
            }

            // Für die Filterfunktion
            for (const [key, value] of Object.entries(lazyParams.filters)) {
                // Wichtig ist das man auch false nimmt, für die Abfrage von Boolean Werten
                if (value.value || value.value === false) {
                    para.filter.push({
                        column: key, text: value.value, matchMode: value.matchMode,
                    });
                }
            }

            url += `?${JSON.stringify(para)}`;
        }

        axios.get(url)
            .then(async (response) => {
                // Prüfen ob angemeldet ist
                checkUserIsLoggedIn(response);
                if (response && response.data && response.data.items) {
                    dataForTable.value = await response.data.items;
                    await generateDataForDropdown(response.data.items);
                }
                if (response && response.data && response.data.totalCount) {
                    dataTotalCount.value = await response.data.totalCount;
                }
                if (response && response.data && response.data.filterCount) {
                    dataFilterCount.value = await response.data.filterCount;
                }
                await setIsLoading(false);

            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .finally(async () => {
                await setIsLoading(false);
            });
    };

    /**
     * Load all Entries from the Database (api)
     * @param lazyParams
     * @param year
     * @returns {Promise<void>}
     */
    const loadDataForTableEvaluation = async (lazyParams = null, year = null) => {
        await setIsLoading(true);
        let para = {};
        // let url = `${useApiStore().operationReporteValuations.slice(0, -4)}/${year}`;
        let url = `${useApiStore().operationReporteValuations}`;

        if (lazyParams) {
            para = {
                limit: lazyParams.rows, offset: lazyParams.first, order: [], filter: []
            };

            if (lazyParams.multiSortMeta) {
                para.order = generateOrderBy(lazyParams.multiSortMeta);
            }

            // Für die Filterfunktion
            for (const [key, value] of Object.entries(lazyParams.filters)) {
                // Wichtig ist das man auch false nimmt, für die Abfrage von Boolean Werten
                if (value.value || value.value === false) {
                    para.filter.push({
                        column: key, text: value.value, matchMode: value.matchMode,
                    });
                }
            }

            url += `?${JSON.stringify(para)}`;
        }

        axios.get(url)
            .then(async (response) => {
                // Prüfen ob angemeldet ist
                checkUserIsLoggedIn(response);
                if (response && response.data && response.data.items) {
                    dataForTable.value = await response.data.items;
                    await generateDataForDropdown(response.data.items);
                }
                if (response && response.data && response.data.totalCount) {
                    dataTotalCount.value = await response.data.totalCount;
                }
                if (response && response.data && response.data.filterCount) {
                    dataFilterCount.value = await response.data.filterCount;
                }
                await setIsLoading(false);

            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .finally(async () => {
                await setIsLoading(false);
            });
    };

    /**
     * Load one Entry with the ID from the Database (api)
     * @param id The ID from the Entry
     * @returns {Promise<void>}
     */
    const loadDataById = async (id) => {
        await setIsLoading(true);
        if (id) {
            axios.get(`${useApiStore().operationReport.getById.slice(0, -4)}/${id}`)
                .then(async function (response) {
                    if (response && response.data && response.data.item) {
                        exportPdfPath.value = await response.data.item.exportPdfPath;
                        await transferDataCache(response.data.item);
                    }
                    await setIsLoading(false);
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })
                .finally(async () => {
                    await setIsLoading(false);
                });
        }
    };

    /**
     * Load one Entry with the ID from the Database (api)
     * @returns {Promise<void>}
     */
    const loadLastNumberFireDepartment = async () => {
        await setIsLoading(true);
        axios.get(`${useApiStore().operationReport.getLastNumberFireDepartment}`)
            .then(async function (response) {
                if (response && response.data) {
                    lastNumberFireDepartment.value = response.data;
                }
                await setIsLoading(false);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .finally(async () => {
                await setIsLoading(false);
            });
    };

    /**
     * Set the Loading State from the Data
     * @param state
     * @returns {Promise<void>}
     */
    const setIsLoading = async (state) => {
        if (typeof state !== 'undefined' || state !== null) {
            isLoading.value = state;
        }
    };

    /**
     * Set the Item of the Default Values
     * @returns {Promise<void>}
     */
    const resetDefaultDataById = async () => {
        dataById.value = Object.assign({}, dataByIdDefault.value);
        operationReport.value = Object.assign({}, operationReportDefault.value);
    }

    /**
     * Generate the array for Dropdown Used
     * @param items
     * @returns {Promise<void>}
     */
    const generateDataForDropdown = async (items) => {
        dataForDropdown.value = [];
        if (items && items.length > 0) {
            for (const item of items) {
                if (await item && item.id && item.abbreviation && item.description) {
                    dataForDropdown.value.push({
                        value: `${item.id}`,
                        label: `${item.description} (${item.abbreviation})`
                    });
                }
            }
        }
    };

    /**
     * Set the Data to the Database Entry
     * @returns {Promise<void>}
     */
    const transferDataEntry = async () => {

        const fromData = operationReport.value;

        dataById.value = {
            id: fromData.id,
            federalState: {
                id: fromData.federalState.id
            },
            department: {
                id: fromData.department.id
            },
            operationReportDetail: fromData.operationReportDetail,
            incomingAlert: {
                id: fromData.incomingAlert.id,
                address: fromData.incomingAlert.address,
                text: fromData.incomingAlert.text,
                incomingDateTime: fromData.incomingAlert.incomingDateTime,
                messageComplete: fromData.incomingAlert.messageComplete,
                numberControlCenter: fromData.incomingAlert.numberControlCenter,
                keywordShort: fromData.incomingAlert.keywordShort,
                keywordLong: fromData.incomingAlert.keywordLong,
                location: fromData.incomingAlert.location,
                otherAddressInformation: fromData.incomingAlert.otherAddressInformation,
                personPatient: fromData.incomingAlert.personPatient,
                item: fromData.incomingAlert
            },
            ownForces: fromData.ownForces,
            otherTaskForces: fromData.otherTaskForces,
            tools: fromData.tools,
            sessionStore: '',
            isVisibility: true,
            commentPrint: fromData.commentPrint,
            comment: fromData.comment
        };
    }

    /**
     * Set the Data to the Cache Entry
     * @returns {Promise<void>}
     */
    const transferDataCache = async (data) => {

        const formatDate = {
            year: 'numeric',
            month: '2-digit',
            day: '2-digit',
        };

        const fromData = await data;

        operationReport.value = {
            id: fromData.id,
            operationReportId: await fromData.id,
            federalState: {
                id: await fromData.federalState.id
            },
            department: {
                id: await fromData.department.id
            },
            operationReportDetail: await fromData.operationReportDetail,
            incomingAlert: {
                id: await fromData.incomingAlert.id,
                address: await fromData.incomingAlert.address,
                text: await fromData.incomingAlert.text,
                incomingDateTime: `${new Date(fromData.incomingAlert.incomingDateTime).toLocaleDateString('de-DE', formatDate)} ${new Date(fromData.incomingAlert.incomingDateTime).toLocaleTimeString('de-DE')}`,
                messageComplete: await fromData.incomingAlert.messageComplete,
                numberControlCenter: await fromData.incomingAlert.numberControlCenter,
                keywordShort: await fromData.incomingAlert.keywordShort,
                keywordLong: await fromData.incomingAlert.keywordLong,
                location: await fromData.incomingAlert.location,
                otherAddressInformation: await fromData.incomingAlert.otherAddressInformation,
                personPatient: await fromData.incomingAlert.personPatient,
                item: await fromData.incomingAlert
            },
            ownForces: await fromData.ownForces,
            otherTaskForces: await fromData.otherTaskForces,
            tools: await fromData.tools,
            sessionStore: '',
            isVisibility: true,
            commentPrint: await fromData.commentPrint,
            exportPdfPath: await fromData.exportPdfPath,
            comment: await fromData.comment,
            fromDatabase: true
        };

        // Formatierung des Datums
        if (await fromData.incomingAlert.incomingDateTime) {
            // operationReport.value.incomingAlert.item.incomingDateTime = `${new Date(fromData.incomingAlert.incomingDateTime).toLocaleDateString('de-DE', formatDate)} ${new Date(fromData.incomingAlert.incomingDateTime).toLocaleTimeString('de-DE')}`
            operationReport.value.incomingAlert.item.incomingDateTime = new Date(fromData.incomingAlert.incomingDateTime);
        }

        if (await fromData.operationReportDetail.startetAt) {
            // operationReport.value.operationReportDetail.startetAt = `${new Date(fromData.operationReportDetail.startetAt).toLocaleDateString('de-DE', formatDate)} ${new Date(fromData.operationReportDetail.startetAt).toLocaleTimeString('de-DE')}`
            operationReport.value.operationReportDetail.startetAt = new Date(await fromData.operationReportDetail.startetAt);
        }

        if (await fromData.operationReportDetail.endetAt) {
            // operationReport.value.operationReportDetail.endetAt = `${new Date(fromData.operationReportDetail.endetAt).toLocaleDateString('de-DE', formatDate)} ${new Date(fromData.operationReportDetail.endetAt).toLocaleTimeString('de-DE')}`
            operationReport.value.operationReportDetail.endetAt = new Date(await fromData.operationReportDetail.endetAt);
        }


        if (await !fromData.commentPrint) {
            operationReport.value.commentPrint = ` `
        }

    }

    return {
        // States
        dataById,
        dataForDropdown,
        dataForTable,
        dataTotalCount,
        dataFilterCount,
        isLoading,
        operationReport,
        lastNumberFireDepartment,
        exportPdfPath,
        // Actions
        loadDataForTableEvaluation,
        loadDataForTable,
        loadDataById,
        resetDefaultDataById,
        loadLastNumberFireDepartment,
        transferDataEntry,
        transferDataCache
    }
})