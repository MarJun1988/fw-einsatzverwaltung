import {defineStore, storeToRefs} from "pinia";
import {ref} from "vue";
import axios from "axios";
import {useApiStore} from "@/stores/api";
import {checkUserIsLoggedIn, generateOrderBy} from "@/utils";
import {useLoggedUserStore} from "@/stores/loggedUser";

let nameOfTheStore = `IncomingAlert`;

// Store - angemeldete Nutzer
const storeLoggedUser = useLoggedUserStore();
const {loggedUser} = storeToRefs(storeLoggedUser);

export const useIncomingAlertStore = defineStore(`store${nameOfTheStore}`, () => {
    // State
    const dataById = ref({
        id: null,
        federalState: {
            id: loggedUser.value && loggedUser.value.federalState && loggedUser.value.federalState.id ? loggedUser.value.federalState.id : ''
        },
        department: {
            id: loggedUser.value && loggedUser.value.department && loggedUser.value.department.id ? loggedUser.value.department.id : ''
        },
        address: {
            id: ''
        },
        numberControlCenter: '',
        keywordShort: '',
        keywordLong: '',
        location: '',
        otherAddressInformation: '',
        personPatient: '',
        text: '',
        incomingDateTime: new Date(),
        messageComplete: '',
        isVisibilityOperationReport: true,
        isVisibility: true,
        comment: ''
    });
    const dataByIdDefault = ref({
        id: null,
        federalState: {
            id: loggedUser.value && loggedUser.value.federalState && loggedUser.value.federalState.id ? loggedUser.value.federalState.id : ''
        },
        department: {
            id: loggedUser.value && loggedUser.value.department && loggedUser.value.department.id ? loggedUser.value.department.id : ''
        },
        address: {
            id: ''
        },
        numberControlCenter: '',
        keywordShort: '',
        keywordLong: '',
        location: '',
        otherAddressInformation: '',
        personPatient: '',
        text: '',
        incomingDateTime: new Date(),
        messageComplete: '',
        isVisibilityOperationReport: true,
        isVisibility: true,
        comment: ''
    });
    const dataForDropdown = ref([]);
    const dataForListBox = ref([]);
    const dataForTable = ref([]);
    const dataTotalCount = ref(0);
    const dataFilterCount = ref(0);
    const dataUserRicsForTable = ref([]);
    const dataUserRicsTotalCount = ref(0);
    const dataUserRicsFilterCount = ref(0);
    const evaluationIncomingAlerts = ref({});
    const isLoading = ref(false);

    // Icons
    const iconPersonMedic = 'personal_injury';
    const iconCarCrash = 'car_crash';
    const iconOilBarrel = 'oil_barrel';
    const iconFire = 'local_fire_department';
    const iconDoorOpen = 'door_open';
    const iconAbc = 'science';

    // Getters

    // Actions
    /**
     * Load all Entries from the Database (api)
     * @param lazyParams
     * @returns {Promise<void>}
     */
    const loadDataForTable = async (lazyParams = null) => {
        await setIsLoading(true);
        let para = {};
        let url = `${useApiStore().incomingAlerts}`;

        if (lazyParams) {
            para = {
                limit: lazyParams.rows, offset: lazyParams.first, order: [], filter: []
            };

            if (lazyParams.multiSortMeta) {
                para.order = generateOrderBy(lazyParams.multiSortMeta);
            }

            // Für die Filterfunktion
            for (const [key, value] of Object.entries(lazyParams.filters)) {
                // Wichtig ist das man auch false nimmt, für die Abfrage von Boolean Werten
                if (value.value || value.value === false) {
                    para.filter.push({
                        column: key, text: value.value, matchMode: value.matchMode,
                    });
                }
            }

            url += `?${JSON.stringify(para)}`;
        }

        axios.get(url)
            .then(async (response) => {
                // Prüfen ob angemeldet ist
                checkUserIsLoggedIn(response);
                if (response && response.data && response.data.items) {
                    dataForTable.value = await response.data.items;
                    // await generateDataForDropdown(response.data.items);
                    await generateDataForListBox(response.data.items);
                }
                if (response && response.data && response.data.totalCount) {
                    dataTotalCount.value = await response.data.totalCount;
                }
                if (response && response.data && response.data.filterCount) {
                    dataFilterCount.value = await response.data.filterCount;
                }
                await setIsLoading(false);

            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .finally(async () => {
                await setIsLoading(false);
            });
    };

    /**
     * Load all Entries from the Database (api)
     * @param lazyParams
     * @returns {Promise<void>}
     */
    const loadDataUserRicsForTable = async (lazyParams = null) => {
        await setIsLoading(true);
        let para = {};
        let url = `${useApiStore().incomingAlertsUserRics}`;

        if (lazyParams) {
            para = {
                limit: lazyParams.rows, offset: lazyParams.first, order: [], filter: []
            };

            if (lazyParams.multiSortMeta) {
                para.order = generateOrderBy(lazyParams.multiSortMeta);
            }

            // Für die Filterfunktion
            if (lazyParams.filters) {
                for (const [key, value] of Object.entries(lazyParams.filters)) {
                    // Wichtig ist das man auch false nimmt, für die Abfrage von Boolean Werten
                    if (value.value || value.value === false) {
                        para.filter.push({
                            column: key, text: value.value, matchMode: value.matchMode,
                        });
                    }
                }
            }
            url += `?${JSON.stringify(para)}`;
        }

        axios.get(url)
            .then(async (response) => {
                // Prüfen ob angemeldet ist
                checkUserIsLoggedIn(response);
                if (response && response.data && response.data.items) {
                    dataUserRicsForTable.value = await response.data.items;
                    // await generateDataForDropdown(response.data.items);
                    await generateDataForListBox(response.data.items);
                }
                if (response && response.data && response.data.totalCount) {
                    dataUserRicsTotalCount.value = await response.data.totalCount;
                }
                if (response && response.data && response.data.filterCount) {
                    dataUserRicsFilterCount.value = await response.data.filterCount;
                }
                await setIsLoading(false);

            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .finally(async () => {
                await setIsLoading(false);
            });
    };

    /**
     * Load all Entries from the Database (api)
     * @param lazyParams
     * @returns {Promise<void>}
     */
    const loadDataUserRicsForOperationReport = async (lazyParams = null) => {
        await setIsLoading(true);
        let para = {};
        let url = `${useApiStore().incomingAlertsUserRicsForOperationReport}`;

        axios.get(url)
            .then(async (response) => {
                // Prüfen ob angemeldet ist
                checkUserIsLoggedIn(response);
                if (response && response.data && response.data.items) {
                    dataUserRicsForTable.value = await response.data.items;
                    // await generateDataForDropdown(response.data.items);
                    await generateDataForListBox(response.data.items);
                }
                if (response && response.data && response.data.totalCount) {
                    dataUserRicsTotalCount.value = await response.data.totalCount;
                }
                if (response && response.data && response.data.filterCount) {
                    dataUserRicsFilterCount.value = await response.data.filterCount;
                }
                await setIsLoading(false);

            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .finally(async () => {
                await setIsLoading(false);
            });
    };

    /**
     * Load one Entry with the ID from the Database (api)
     * @param id The ID from the Entry
     * @returns {Promise<void>}
     */
    const loadDataById = async (id) => {
        await setIsLoading(true);
        if (id) {
            axios.get(`${useApiStore().incomingAlert.getById.slice(0, -4)}/${id}`)
                .then(async function (response) {
                    if (response && response.data && response.data.item) {
                        dataById.value = await response.data.item;

                        // Datum und Uhrzeit Anpassen
                        let date = new Date(await response.data.item.incomingDateTime);
                        date.setHours(date.getHours() - 2);
                        dataById.value.incomingDateTime = await date;
                    }
                    await setIsLoading(false);
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })
                .finally(async () => {
                    await setIsLoading(false);
                });
        }
    };

    /**
     * Load one Entry with the ID from the Database (api)
     * @returns {Promise<void>}
     * @param year
     */
    const loadDataEvaluation = async (year) => {
        await setIsLoading(true);

        if (year) {
            // evaluationIncomingAlerts.value[`${year}`] = [];
            // console.log("=>(incomingAlert.js:241) evaluationIncomingAlerts.value", evaluationIncomingAlerts.value);
            axios.get(`${useApiStore().incomingAlertEvaluation.getChartByYear.slice(0, -4)}/${year}`)
                .then(async function (response) {
                    if (response && response.data && response.data && response.data.item) {
                        evaluationIncomingAlerts.value[year] = {
                            labels: response.data.item.labels,
                            datasets: [
                                {
                                    label: response.data.item.datasets.label,
                                    backgroundColor: response.data.item.datasets.backgroundColor,
                                    data: response.data.item.datasets.data,
                                }
                            ]
                        }
                    }
                    await setIsLoading(false);
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })
                .finally(async () => {
                    await setIsLoading(false);
                });

        }
    };

    /**
     * Set the Loading State from the Data
     * @param state
     * @returns {Promise<void>}
     */
    const setIsLoading = async (state) => {
        if (typeof state !== 'undefined' || state !== null) {
            isLoading.value = state;
        }
    };

    /**
     * Set the Item of the Default Values
     * @returns {Promise<void>}
     */
    const resetDefaultDataById = async () => {
        dataById.value = Object.assign({}, dataByIdDefault.value);
    }

    /**
     * Generate the array for Dropdown Used
     * @param items
     * @returns {Promise<void>}
     */
    const generateDataForDropdown = async (items) => {
        dataForDropdown.value = [];
        if (items && items.length > 0) {
            for (const item of items) {
                if (await item && item.id && item.abbreviation && item.description) {
                    dataForDropdown.value.push({
                        value: `${item.id}`,
                        label: `${item.description} (${item.abbreviation})`
                    });
                }
            }
        }
    };

    /**
     * Generate the array for ListBox Used
     * @param items
     * @returns {Promise<void>}
     */
    const generateDataForListBox = async (items) => {
        dataForListBox.value = [];
        let i = 0;
        if (items && items.length > 0) {
            for (const item of items) {
               if (await item && item.id &&
                    item.incomingDateTime &&
                    item.numberControlCenter &&
                    item.keywordShort  && i < 10) {

                    let date = new Date(item.incomingDateTime);
                    date.setHours(date.getHours() - 1);

                    dataForListBox.value.push(
                        {
                            id: item.id,
                            icon: checkIconForListBox(item.keywordShort),
                            incomingDateTime: date.toLocaleTimeString('de-DE', {
                                day: '2-digit',
                                month: '2-digit',
                                year: 'numeric',
                                hours: '2-digit',
                                minutes: '2-digit'
                            }),
                            numberControlCenter: item.numberControlCenter,
                            keywordShort: item.keywordShort,
                            text: item.text ?? 'BITTE NOCH AUSFÜLLEN',
                            item: item
                        }
                    );

                    i++;
                }
            }
        }
    };

    /**
     *
     */
    const checkIconForListBox = (item) => {

        if (item) {
            switch (item.toLowerCase()) {
                case 'th':
                case 'th1':
                case 'th klein':
                case 'th mittel':
                case 'th groß':
                case 'th - klein':
                case 'th - mittel':
                case 'th - groß':
                    return iconOilBarrel;
                    break;
                case 'brand':
                case 'brand - klein':
                case 'brand klein':
                case 'brand - mittel':
                case 'brand mittel':
                case 'brand - groß':
                case 'brand groß':
                case 'b4':
                case 'brand1':
                case 'brand2':
                    return iconFire;
                    break;
                case 'bma':
                    return iconFire;
                    break;
                case 'notöffnung':
                    return iconDoorOpen;
                    break;
                case 'abc':
                case 'abc1':
                case 'abc klein':
                    return iconAbc;
                    break;
                case 'tragehilfe':
                    return iconPersonMedic;
                    break;
                default:
                // code block
            }
        }

    };

    return {
        // States
        dataById,
        dataForDropdown,
        dataForTable,
        dataForListBox,
        dataTotalCount,
        dataUserRicsForTable,
        dataUserRicsTotalCount,
        dataFilterCount,
        dataUserRicsFilterCount,
        evaluationIncomingAlerts,
        isLoading,
        // Actions
        loadDataForTable,
        loadDataUserRicsForTable,
        loadDataUserRicsForOperationReport,
        loadDataById,
        loadDataEvaluation,
        resetDefaultDataById
    }
})