import {defineStore} from "pinia";
import {ref} from "vue";

let storeName = `LoggedUser`;
export const useLoggedUserStore = defineStore(`store${storeName}`, () => {
    // State
    const loggedUser = ref(__LOGGED_USER__);

    return {
        loggedUser
    }
});