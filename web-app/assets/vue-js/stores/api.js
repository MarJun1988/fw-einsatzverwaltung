import {defineStore} from "pinia";
import {ref} from "vue";

let storeName = `Api`;
export const useApiStore = defineStore(`store${storeName}`, () => {
    // State
    // Login

    // Bundesländer
    const federalStates = ref(__API__.federalstates);
    const federalState = ref(__API__.federalstate);
    // Dienststellen
    const departments = ref(__API__.departments);
    const department = ref(__API__.department);
    // Versions
    const versions = ref(__API__.versions);
    const version = ref(__API__.version);
    // Nutzerrollen
    const userRoles = ref(__API__.userRoles);
    const userRole = ref(__API__.userRole);
    // Mitglieder Funktionen
    const memberFunctions = ref(__API__.memberFunctions);
    const memberFunction = ref(__API__.memberFunction);
    // Einsatzart Kategorie Type
    const operationTypeCategories = ref(__API__.operationTypeCategories);
    const operationTypeCategory = ref(__API__.operationTypeCategory);
    // Einsatzart
    const operationTypes = ref(__API__.operationTypes);
    const operationType = ref(__API__.operationType);
    // Fahrzeuge
    const vehicles = ref(__API__.vehicles);
    const vehicle = ref(__API__.vehicle);
    // Hilfsmittel Kategorie
    const toolCategories = ref(__API__.toolCategories);
    const toolCategory = ref(__API__.toolCategory);
    // Hilfsmittel
    const tools = ref(__API__.tools);
    const tool = ref(__API__.tool);
    // Alarm Adressen (RIC)
    const alertAddresses = ref(__API__.alertAddresses);
    const alertAddress = ref(__API__.alertAddress);
    // Mitglieder
    const members = ref(__API__.members);
    const member = ref(__API__.member);
    // Nutzer
    const users = ref(__API__.users);
    const user = ref(__API__.user);
    // Allgemeines
    const generals = ref(__API__.generals);
    const general = ref(__API__.general);
    // Eingegangene Alarmierungen
    const incomingAlertEvaluation = ref(__API__.incomingAlertEvaluation);
    const incomingAlerts = ref(__API__.incomingAlerts);
    const incomingAlertsUserRics = ref(__API__.incomingAlertsUserRics);
    const incomingAlertsUserRicsForOperationReport = ref(__API__.incomingAlertsUserRicsForOperationReport);
    const incomingAlert = ref(__API__.incomingAlert);
    // Einsatzberichte
    const operationReports = ref(__API__.operationReports);
    const operationReporteValuations = ref(__API__.operationReporteValuations);
    const operationReport = ref(__API__.operationReport);
    // Einsatzberichte Zwischenspeicher
    const operationReportCaches = ref(__API__.operationReportCaches);
    const operationReportCache = ref(__API__.operationReportCache);
    // Feedback
    const feedbacks = ref(__API__.feedbacks);
    const feedback = ref(__API__.feedback);


    return {
        federalStates,
        federalState,
        departments,
        department,
        versions,
        version,
        userRoles,
        userRole,
        memberFunctions,
        memberFunction,
        operationTypeCategories,
        operationTypeCategory,
        operationTypes,
        operationType,
        vehicles,
        vehicle,
        toolCategories,
        toolCategory,
        tools,
        tool,
        alertAddresses,
        alertAddress,
        members,
        member,
        users,
        user,
        generals,
        general,
        incomingAlertEvaluation,
        incomingAlerts,
        incomingAlertsUserRics,
        incomingAlertsUserRicsForOperationReport,
        incomingAlert,
        operationReports,
        operationReporteValuations,
        operationReport,
        operationReportCaches,
        operationReportCache,
        feedback,
        feedbacks
    }
});