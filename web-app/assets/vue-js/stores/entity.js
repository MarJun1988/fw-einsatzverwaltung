import {defineStore, storeToRefs} from "pinia";
import {useApiStore} from "@/stores/api";
import {ref} from "vue";
import axios from "axios";
import {useFederalStateStore} from "@/stores/ProvisioningArea/federalState";
import {useDataTableStore} from "@/stores/dataTable";
import {useTranslationStore} from "@/stores/translation";
import {useDepartmentStore} from "@/stores/ReferenceArea/department";
import {useVersionStore} from "@/stores/ProvisioningArea/version";
import {useUserRoleStore} from "@/stores/ProvisioningArea/userRole";
import {useMemberFunctionStore} from "@/stores/ReferenceArea/memberFunction";
import {useOperationTypeCategoryStore} from "@/stores/ReferenceArea/operationTypeCategory";
import {useOperationTypeStore} from "@/stores/ReferenceArea/operationType";
import {useVehicleStore} from "@/stores/Managements/vehicle";
import {useToolCategoryStore} from "@/stores/Managements/toolCategory";
import {useToolStore} from "@/stores/Managements/tool";
import {useAlertAddressStore} from "@/stores/Managements/alertAddress";
import {useMemberStore} from "@/stores/Managements/member";
import {useUserStore} from "@/stores/Managements/user";
import {useGeneralStore} from "@/stores/Managements/general";
import {useIncomingAlertStore} from "@/stores/OperationArea/incomingAlert";
import {useOperationReportCacheStore} from "@/stores/OperationArea/operationReportCache";
import {useOperationReportStore} from "@/stores/OperationArea/operationReport";
import {checkUserIsLoggedIn} from "@/utils";
import {useFeedbackStore} from "@/stores/EvaluationArea/feedback";

const apiUrl = useApiStore();

// Store - Übersetzungen
const storeTranslation = useTranslationStore();
const {components} = storeToRefs(storeTranslation)

// Angemeldeter Nutzer
// const storeLoggedUser = useLoggedUserStore();
// const {currentUser} = storeToRefs(storeLoggedUser)

let nameOfTheStore = `Entity`;
export const useEntityStore = defineStore(`store${nameOfTheStore}`, () => {

        // State
        const result = ref({
            status: null, message: null, errors: [], type: null
        });


        // Actions
        /**
         * Reset the state result of Default
         *
         * @returns {Promise<void>}
         */
        const resetResult = async () => {
            result.value = {
                status: null, message: null, errors: [], type: null
            }
        }

        /**
         * Action, Create, Update and Delete the Entry
         *
         * @param entity
         * @param param
         * @param actionType
         * @returns {Promise<void>}
         */
        const itemAction = async (entity, param, actionType) => {
            // Löschen der alten Results
            await resetResult();

            let url = '';
            let store = '';

            switch (await entity) {
                case 'FederalState':
                    if (actionType === 'edit' || actionType === 'delete') {
                        url = apiUrl.federalState[actionType].slice(0, -4) + `/${param.id}`;
                    } else {
                        url = apiUrl.federalState[actionType]
                    }
                    store = useFederalStateStore();
                    break;
                case 'Department':
                    if (actionType === 'edit' || actionType === 'delete') {
                        url = apiUrl.department[actionType].slice(0, -4) + `/${param.id}`;
                    } else {
                        url = apiUrl.department[actionType]
                    }
                    store = useDepartmentStore();
                    break;
                case 'Version':
                    if (actionType === 'edit' || actionType === 'delete') {
                        url = apiUrl.version[actionType].slice(0, -4) + `/${param.id}`;
                    } else {
                        url = apiUrl.version[actionType]
                    }
                    store = useVersionStore();
                    break;
                case 'UserRole':
                    if (actionType === 'edit' || actionType === 'delete') {
                        url = apiUrl.userRole[actionType].slice(0, -4) + `/${param.id}`;
                    } else {
                        url = apiUrl.userRole[actionType]
                    }
                    store = useUserRoleStore();
                    break;
                case 'MemberFunction':
                    if (actionType === 'edit' || actionType === 'delete') {
                        url = apiUrl.memberFunction[actionType].slice(0, -4) + `/${param.id}`;
                    } else {
                        url = apiUrl.memberFunction[actionType]
                    }
                    store = useMemberFunctionStore();
                    break;
                case 'OperationTypeCategory':
                    if (actionType === 'edit' || actionType === 'delete') {
                        url = apiUrl.operationTypeCategory[actionType].slice(0, -4) + `/${param.id}`;
                    } else {
                        url = apiUrl.operationTypeCategory[actionType]
                    }
                    store = useOperationTypeCategoryStore();
                    break;
                case 'OperationType':
                    if (actionType === 'edit' || actionType === 'delete') {
                        url = apiUrl.operationType[actionType].slice(0, -4) + `/${param.id}`;
                    } else {
                        url = apiUrl.operationType[actionType]
                    }
                    store = useOperationTypeStore();
                    break;
                case 'Vehicle':
                    if (actionType === 'edit' || actionType === 'delete') {
                        url = apiUrl.vehicle[actionType].slice(0, -4) + `/${param.id}`;
                    } else {
                        url = apiUrl.vehicle[actionType]
                    }
                    store = useVehicleStore();
                    break;
                case 'ToolCategory':
                    if (actionType === 'edit' || actionType === 'delete') {
                        url = apiUrl.toolCategory[actionType].slice(0, -4) + `/${param.id}`;
                    } else {
                        url = apiUrl.toolCategory[actionType]
                    }
                    store = useToolCategoryStore();
                    break;
                case 'Tool':
                    if (actionType === 'edit' || actionType === 'delete') {
                        url = apiUrl.tool[actionType].slice(0, -4) + `/${param.id}`;
                    } else {
                        url = apiUrl.tool[actionType]
                    }
                    store = useToolStore();
                    break;
                case 'AlertAddress':
                    if (actionType === 'edit' || actionType === 'delete') {
                        url = apiUrl.alertAddress[actionType].slice(0, -4) + `/${param.id}`;
                    } else {
                        url = apiUrl.alertAddress[actionType]
                    }
                    store = useAlertAddressStore();
                    break;
                case 'Member':
                    if (actionType === 'edit' || actionType === 'delete') {
                        url = apiUrl.member[actionType].slice(0, -4) + `/${param.id}`;
                    } else {
                        url = apiUrl.member[actionType]
                    }
                    store = useMemberStore();
                    break;
                case 'User':
                    if (actionType === 'edit' || actionType === 'delete') {
                        url = apiUrl.user[actionType].slice(0, -4) + `/${param.id}`;
                    } else {
                        url = apiUrl.user[actionType]
                    }
                    store = useUserStore();
                    break;
                case 'UserPassword':
                    if (actionType === 'edit' || actionType === 'delete') {
                        url = apiUrl.user['editPassword'].slice(0, -4) + `/${param.id}`;
                    } else {
                        url = apiUrl.user[actionType]
                    }
                    store = useUserStore();
                    break;
                case 'General':
                    if (actionType === 'edit' || actionType === 'delete') {
                        url = apiUrl.general[actionType].slice(0, -4) + `/${param.id}`;
                    } else {
                        url = apiUrl.general[actionType]
                    }
                    store = useGeneralStore();
                    break;
                case 'IncomingAlert':
                    if (actionType === 'edit' || actionType === 'delete') {
                        url = apiUrl.incomingAlert[actionType].slice(0, -4) + `/${param.id}`;
                    } else {
                        url = apiUrl.incomingAlert[actionType]
                    }
                    store = useIncomingAlertStore();
                    break;
                case 'OperationReport':
                    if (actionType === 'edit' || actionType === 'delete') {
                        url = apiUrl.operationReport[actionType].slice(0, -4) + `/${param.id}`;
                    } else {
                        url = apiUrl.operationReport[actionType]
                    }
                    store = useOperationReportStore();
                    break;
                case 'OperationReportCache':
                    if (actionType === 'edit' || actionType === 'delete') {
                        url = apiUrl.operationReportCache[actionType].slice(0, -4) + `/${param.id}`;
                    } else {
                        url = apiUrl.operationReportCache[actionType]
                    }
                    store = useOperationReportCacheStore();
                    break;
                case 'Feedback':
                    if (actionType === 'edit' || actionType === 'delete') {
                        url = apiUrl.feedback[actionType].slice(0, -4) + `/${param.id}`;
                    } else {
                        url = apiUrl.feedback[actionType]
                    }
                    store = useFeedbackStore();
                    break;

            }

            await axios.post(url, param)
                .then(async response => {
                    if (response && response.data) {
                        // Prüfen ob angemeldet ist
                        checkUserIsLoggedIn(response);
                        // Rückgabewert im Store Speichern
                        result.value = await response.data
                        // Abrufen der Daten aus der Datenbank
                        if (store) {
                            await store.loadDataForTable(useDataTableStore().lastLazyParams);
                        }
                    }
                })
                .catch(async error => {
                        if (error && error.response && error.response.data) {
                            // Keine Rechte
                            if (parseInt(error.response.data.status) === 403) {
                                result.value = {
                                    status: 403, message: components.value.toastMessage.noRight.message
                                };
                            } else {
                                // Rückgabewert im Store Speichern
                                result.value = await error.response.data;
                                // if (currentUser.value.developer) {
                                //     console.error(error);
                                // }
                            }
                        }
                    }
                )
            ;
        }

        return {result, resetResult, itemAction}
    })
;