import {defineStore} from "pinia";
import {ref} from "vue";

let nameOfTheStore = `DataTable`;
export const useDataTableStore = defineStore(`store${nameOfTheStore}`, () => {
    // State
    const lastLazyParams = ref({
        first: 0, rows: 25, sortField: null, sortOrder: null, filters: [], multiSortMeta: [],
    });

    const selectedColumns = ref({
        // Einsatzbereich
        'AllIncomingAlerts': [],
        'MyIncomingAlerts': [],
        'OperationReports': [],
        // Verwaltung
        'AlertAddresses': [],
        'Members': [],
        'ToolCategories': [],
        'Tools': [],
        'Users': [],
        'Vehicles': [],
        // Bereitstellungsdaten
        'Departments': [],
        'MemberFunctions': [],
        'OperationTypeCategories': [],
        'OperationTypes': [],
        // Nachschlagetabellen
        'FederalState': [],
        'UserRoles': [],
        'Versions': [],
        // Auswertung
        'OperationReportsEvaluation': [],
        'Feedbacks': [],
    });
    // Getters
    // Actions
    /**
     *
     * @param columns Spalten die Sichtbar sein sollen
     * @param storeName Name der Variable
     * @param storeLocation (DEFAULT) LocalStorage oder SessionStorage
     * @returns {Promise<void>}
     */
    const setSelectedColumns = async (columns, storeName, storeLocation = "localStorage") => {
        if (columns && columns.length > 0) {
            selectedColumns.value[`${storeName}`] = await columns.sort((a, b) => {
                if (a.sorting > b.sorting) {
                    return 1;
                }
                if (a.sorting < b.sorting) {
                    return -1;
                }
                return 0;
            });
            await saveInStore(storeName, storeLocation);
        }
    }

    /**
     * Speichern der ausgewählten Spalten im Browser Store
     *
     * @param storeName Name der Variable
     * @param storeLocation (DEFAULT) LocalStorage oder SessionStorage
     * @returns {Promise<void>}
     */
    const saveInStore = async (storeName, storeLocation = "localStorage") => {
        // Speichern von Strings
        if (storeLocation === "localStorage") {
            await localStorage.setItem(`tableColumns-${storeName}`, JSON.stringify(selectedColumns.value[storeName]));
        } else if (storeLocation === "sessionStorage") {
            await sessionStorage.setItem(`tableColumns-${storeName}`, JSON.stringify(selectedColumns.value[storeName]));
        }
    }

    /**
     * Auslesen der ausgewählten Spalten vom Browser Store
     *
     * @param storeName Name der Variable
     * @param storeLocation (DEFAULT) LocalStorage oder SessionStorage
     * @returns {Promise<void>}
     */
    const loadFromStore = async (storeName, storeLocation = "localStorage") => {
        // Speichern von Strings
        if (storeLocation === "localStorage" && localStorage.getItem(`tableColumns-${storeName}`) && localStorage.getItem(`tableColumns-${storeName}`) !== undefined && localStorage.getItem(`tableColumns-${storeName}`).length !== 0) {

            selectedColumns.value[storeName] = await JSON.parse(localStorage.getItem(`tableColumns-${storeName}`));

        } else if (storeLocation === "sessionStorage" && sessionStorage.getItem(`tableColumns-${storeName}`) && sessionStorage.getItem(`tableColumns-${storeName}`) !== undefined && sessionStorage.getItem(`tableColumns-${storeName}`).length !== 0) {
            selectedColumns.value[storeName] = await JSON.parse(sessionStorage.getItem(`tableColumns-${storeName}`));
        }
    }

    return {
        lastLazyParams, selectedColumns, setSelectedColumns, saveInStore, loadFromStore
    }
})