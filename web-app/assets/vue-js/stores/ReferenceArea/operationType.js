import {defineStore, storeToRefs} from "pinia";
import {ref} from "vue";
import axios from "axios";
import {useApiStore} from "@/stores/api";
import {checkUserIsLoggedIn, generateOrderBy} from "@/utils";
import {useLoggedUserStore} from "@/stores/loggedUser";

let nameOfTheStore = `OperationType`;

// Store - angemeldete Nutzer
const storeLoggedUser = useLoggedUserStore();
const {loggedUser} = storeToRefs(storeLoggedUser);

export const useOperationTypeStore = defineStore(`store${nameOfTheStore}`, () => {
    // State
    const dataById = ref({
        id: null,
        department: {
            id: loggedUser.value && loggedUser.value.department && loggedUser.value.department.id ? loggedUser.value.department.id : ''
        },
        abbreviation: '',
        description: '',
        isVisibility: true,
        operationTypeCategory: {
            id: null
        },
        sorting: 0,
        comment: ''
    });
    const dataByIdDefault = ref({
        id: null,
        department: {
            id: loggedUser.value && loggedUser.value.department && loggedUser.value.department.id ? loggedUser.value.department.id : ''
        },
        abbreviation: '',
        description: '',
        isVisibility: true,
        operationTypeCategory: {
            id: null
        },
        sorting: 0,
        comment: ''
    });
    const dataForDropdown = ref([]);
    const dataForTable = ref([]);
    const dataTotalCount = ref(0);
    const dataFilterCount = ref(0);
    const isLoading = ref(false);
    // Getters

    // Actions
    /**
     * Load all Entries from the Database (api)
     * @param lazyParams
     * @returns {Promise<void>}
     */
    const loadDataForTable = async (lazyParams = null) => {
        await setIsLoading(true);
        let para = {};

        if (lazyParams) {
            para = {
                limit: lazyParams.rows, offset: lazyParams.first, order: [], filter: []
            };

            if (lazyParams.multiSortMeta) {
                para.order = generateOrderBy(lazyParams.multiSortMeta);
            }

            // Für die Filterfunktion
            for (const [key, value] of Object.entries(lazyParams.filters)) {
                // Wichtig ist das man auch false nimmt, für die Abfrage von Boolean Werten
                if (value.value || value.value === false) {
                    para.filter.push({
                        column: key, text: value.value, matchMode: value.matchMode,
                    });
                }
            }
        }

        axios.get(`${useApiStore().operationTypes}?${JSON.stringify(para)}`)
                .then(async (response) => {
                    // Prüfen ob angemeldet ist
                    checkUserIsLoggedIn(response);
                if (response && response.data && response.data.items) {
                    dataForTable.value = await response.data.items;
                }
                if (response && response.data && response.data.totalCount) {
                    dataTotalCount.value = await response.data.totalCount;
                }
                if (response && response.data && response.data.filterCount) {
                    dataFilterCount.value = await response.data.filterCount;
                }
                await setIsLoading(false);

            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .finally(async () => {
                await setIsLoading(false);
            });

    };

    /**
     * Load all Entries from the Database (api), for Dropdown
     * @returns {Promise<void>}
     */
    const loadDataForDropdown = async () => {
        // Prüfen ob der Store schon befühlt ist
        if (dataForDropdown.value && dataForDropdown.value.length === 0) {
            await setIsLoading(true);

            axios.get(`${useApiStore().operationType.getForDropdown}`)
                            .then(async (response) => {
                // Prüfen ob angemeldet ist
                checkUserIsLoggedIn(response);
                    if (response && response.data && response.data.items) {
                        await generateDataForDropdown(response.data.items);
                    }
                    await setIsLoading(false);
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })
                .finally(async () => {
                    await setIsLoading(false);
                });
        }
    };

    /**
     * Load one Entry with the ID from the Database (api)
     * @param id The ID from the Entry
     * @returns {Promise<void>}
     */
    const loadDataById = async (id) => {
        await setIsLoading(true);
        if (id) {
            axios.get(`${useApiStore().operationType.getById.slice(0, -4)}/${id}`)
                .then(async function (response) {
                    if (response && response.data && response.data.item) {
                        dataById.value = response.data.item;
                    }
                    await setIsLoading(false);
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })
                .finally(async () => {
                    await setIsLoading(false);
                });
        }
    };

    /**
     * Set the Loading State from the Data
     * @param state
     * @returns {Promise<void>}
     */
    const setIsLoading = async (state) => {
        if (typeof state !== 'undefined' || state !== null) {
            isLoading.value = state;
        }
    };

    /**
     * Set the Item of the Default Values
     * @returns {Promise<void>}
     */
    const resetDefaultDataById = async () => {
        dataById.value = Object.assign({}, dataByIdDefault.value);
    };

    /**
     * Generate the array for Dropdown Used
     * @param items
     * @returns {Promise<void>}
     */
    const generateDataForDropdown = async (items) => {
        dataForDropdown.value = [];
        if (items && items.length > 0) {
            for (const item of items) {
                if (await item && item.id && item.abbreviation && item.description) {
                    dataForDropdown.value.push({
                        value: `${item.id}`,
                        label: `${item.description} (${item.abbreviation})`
                    });
                }
            }
        }
    };

    return {
        // States
        dataById, dataForDropdown, dataForTable, dataTotalCount, dataFilterCount, isLoading,
        // Actions
        loadDataForTable, loadDataForDropdown, loadDataById, resetDefaultDataById
    }
})