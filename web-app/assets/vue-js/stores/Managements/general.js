import {defineStore} from "pinia";
import {ref} from "vue";
import axios from "axios";
import {useApiStore} from "@/stores/api";
import {checkUserIsLoggedIn, generateOrderBy} from "@/utils";

let nameOfTheStore = `General`;
export const useGeneralStore = defineStore(`store${nameOfTheStore}`, () => {
    // State
    const dataById = ref({
        id: null,
        department: {
            id: ''
        },
        name: '',
        value: '',
        description: '',
        isVisibility: true,
        comment: ''
    });
    const dataByIdDefault = ref({
        id: null,
        department: {
            id: ''
        },
        name: '',
        value: '',
        description: '',
        isVisibility: true,
        comment: ''
    });
    const dataForTable = ref([]);
    const dataTotalCount = ref(0);
    const dataFilterCount = ref(0);
    const isLoading = ref(false);
    const lastLoadedData = ref('');

    const siteNamePdf = ref({
        id: '',
        name: '',
        value: '',
        description: '',
        comment: '',
        isVisibility: true,
        department: {
            id: ''
        },
    });
    const siteNameApplicationLong = ref({
        id: '',
        name: '',
        value: '',
        description: '',
        comment: '',
        isVisibility: true,
        department: {
            id: ''
        },
    });
    const siteNameApplicationShort = ref({
        id: '',
        name: '',
        value: '',
        description: '',
        comment: '',
        isVisibility: true,
        department: {
            id: ''
        },
    });
    const siteDefaultMemberFunction = ref({
        id: '',
        name: '',
        value: '',
        description: '',
        comment: '',
        isVisibility: true,
        department: {
            id: ''
        },
    });
    // Getters

    // Actions
    /**
     * Load all Entries from the Database (api)
     * @param lazyParams
     * @returns {Promise<void>}
     */
    const loadDataForTable = async (lazyParams = null) => {
        // Überprüfen, ob die Zeitdifferenz weniger als 10 Minuten beträgt
        if (!lastLoadedData.value || !(new Date() - lastLoadedData.value / (1000 * 60) >= 10)) {
            await setIsLoading(true);
            let para = {};
            let url = `${useApiStore().generals}`;

            if (lazyParams) {
                para = {
                    limit: lazyParams.rows, offset: lazyParams.first, order: [], filter: []
                };

                if (lazyParams.multiSortMeta) {
                    para.order = generateOrderBy(lazyParams.multiSortMeta);
                }

                // Für die Filterfunktion
                for (const [key, value] of Object.entries(lazyParams.filters)) {
                    // Wichtig ist das man auch false nimmt, für die Abfrage von Boolean Werten
                    if (value.value || value.value === false) {
                        para.filter.push({
                            column: key, text: value.value, matchMode: value.matchMode,
                        });
                    }
                }

                url += `?${JSON.stringify(para)}`;
            }

            axios.get(url)
                            .then(async (response) => {
                // Prüfen ob angemeldet ist
                checkUserIsLoggedIn(response);
                    if (response && response.data && response.data.items) {
                        dataForTable.value = await response.data.items;
                        await findDetails(response.data.items);
                    }
                    if (response && response.data && response.data.totalCount) {
                        dataTotalCount.value = await response.data.totalCount;
                    }
                    if (response && response.data && response.data.filterCount) {
                        dataFilterCount.value = await response.data.filterCount;
                    }
                    await setIsLoading(false);
                    lastLoadedData.value = await new Date();

                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })
                .finally(async () => {
                    await setIsLoading(false);
                });
        }
    };

    /**
     * Load one Entry with the ID from the Database (api)
     * @param id The ID from the Entry
     * @returns {Promise<void>}
     */
    const loadDataById = async (id) => {
        await setIsLoading(true);
        if (id) {
            axios.get(`${useApiStore().general.getById.slice(0, -4)}/${id}`)
                .then(async function (response) {
                    if (response && response.data && response.data.item) {
                        dataById.value = response.data.item;
                    }
                    await setIsLoading(false);
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })
                .finally(async () => {
                    await setIsLoading(false);
                });
        }
    };

    /**
     * Load one Entry on the Member Function from the Database (api)
     * @param memberFunction The Functioname from the Entry
     * @returns {Promise<void>}
     */
    const loadDataByMemberFunction = async (memberFunction) => {
        await setIsLoading(true);
        if (memberFunction) {
            axios.get(`${useApiStore().member.getByMemberFunction.slice(0, -4)}/${memberFunction}`)
                .then(async function (response) {
                    if (response && response.data && response.data.item) {
                        if (memberFunction === 'WL') {
                            dataWarChief.value = response.data.item;
                        } else if (memberFunction === 'St. WL') {
                            dataDeputyChief.value = response.data.item;
                        }
                    }
                    await setIsLoading(false);
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })
                .finally(async () => {
                    await setIsLoading(false);
                });
        }
    };

    /**
     * Set the Loading State from the Data
     * @param state
     * @returns {Promise<void>}
     */
    const setIsLoading = async (state) => {
        if (typeof state !== 'undefined' || state !== null) {
            isLoading.value = state;
        }
    };

    /**
     * Set the Item of the Default Values
     * @returns {Promise<void>}
     */
    const resetDefaultDataById = async () => {
        dataById.value = Object.assign({}, dataByIdDefault.value);
    }

    const findDetails = async (items) => {
        if (items) {

            // Name der Fw in der .pdf
            const namePdf = items.find(item => item.name === 'namePdf');
            if (namePdf) {
                siteNamePdf.value = namePdf;
            }

            // Applikations Title - Lang
            const nameApplicationLong = items.find(item => item.name === 'nameApplicationLong');
            if (nameApplicationLong) {
                siteNameApplicationLong.value = nameApplicationLong;
            }

            // Applikations Title - Kurz
            const nameApplicationShort = items.find(item => item.name === 'nameApplicationShort');
            if (nameApplicationShort) {
                siteNameApplicationShort.value = nameApplicationShort;
            }

            // Standard Funktion eines Mitgliedes im Einsatzbericht
            const defaultMemberFunction = items.find(item => item.name === 'operationReportDefaultFunctionMember');
            if (defaultMemberFunction) {
                siteDefaultMemberFunction.value = defaultMemberFunction;
            }
        }
    }

    return {
        // States
        dataById,
        dataForTable,
        dataTotalCount,
        dataFilterCount,
        isLoading,
        siteNamePdf,
        siteNameApplicationLong,
        siteNameApplicationShort,
        siteDefaultMemberFunction,
        // Actions
        loadDataForTable,
        loadDataById,
        resetDefaultDataById,
        loadDataByMemberFunction
    }
})