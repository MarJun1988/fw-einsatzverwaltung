import {defineStore} from "pinia";
import {ref} from "vue";
import axios from "axios";
import {useApiStore} from "@/stores/api";
import {checkUserIsLoggedIn, generateOrderBy} from "@/utils";

let nameOfTheStore = `UserGroup`;
export const useUserGroupStore = defineStore(`store${nameOfTheStore}`, () => {
    // State
    const dataById = ref({
        id: null,
        abbreviation: '',
        description: '',
        comment: ''
    });
    const dataByIdDefault = ref({
        id: null,
        abbreviation: '',
        description: '',
        comment: ''
    });
    const dataForDropdown = ref([]);
    const dataForTable = ref([]);
    const dataTotalCount = ref(0);
    const dataFilterCount = ref(0);
    const isLoading = ref(false);
    // Getters

    // Actions
    /**
     * Load all Entries from the Database (api)
     * @param lazyParams
     * @returns {Promise<void>}
     */
    const loadDataForRegistration = async (lazyParams = null) => {
        await setIsLoading(true);

        axios.get(`${useApiStore().apiRegister.userGroups}`)
                .then(async (response) => {
                    // Prüfen ob angemeldet ist
                    checkUserIsLoggedIn(response);
                if (response && response.data) {
                    dataForTable.value = await response.data;
                    await generateDataForDropdown(response.data);
                }

                await setIsLoading(false);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .finally(async () => {
                await setIsLoading(false);
            });
    };

    /**
     * Load all Entries from the Database (api)
     * @param lazyParams
     * @returns {Promise<void>}
     */
    const loadDataForTable = async (lazyParams = null) => {
        await setIsLoading(true);
        let para = {};
        let url = `${useApiStore().apiRegister.federalStates}`;

        if (lazyParams) {
            para = {
                limit: lazyParams.rows, offset: lazyParams.first, order: [], filter: []
            };

            if (lazyParams.multiSortMeta) {
                para.order = generateOrderBy(lazyParams.multiSortMeta);
            }

            // Für die Filterfunktion
            for (const [key, value] of Object.entries(lazyParams.filters)) {
                if (value.value) {
                    para.filter.push({
                        column: key, text: value.value, matchMode: value.matchMode,
                    });
                }
            }

            url = `${useApiStore().apiRegister.ranks}?${JSON.stringify(para)}`;
        }

        axios.get(url)
                .then(async (response) => {
                    // Prüfen ob angemeldet ist
                    checkUserIsLoggedIn(response);
                if (response && response.data && response.data.items) {
                    dataForTable.value = await response.data.items;
                    await generateDataForDropdown(response.data.items);
                }
                if (response && response.data && response.data.totalCount) {
                    dataTotalCount.value = await response.data.totalCount;
                }
                if (response && response.data && response.data.filterCount) {
                    dataFilterCount.value = await response.data.filterCount;
                }
                await setIsLoading(false);

            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .finally(async () => {
                await setIsLoading(false);
            });
    };

    /**
     * Load one Entry with the ID from the Database (api)
     * @param id The ID from the Entry
     * @returns {Promise<void>}
     */
    const loadDataById = async (id) => {
        await setIsLoading(true);
        if (id) {
            axios.get(`${useApiStore().userGroup.getById.slice(0, -4)}/${id}`)
                .then(async function (response) {
                    if (response && response.data && response.data.item) {
                        dataById.value = response.data.item;
                    }
                    await setIsLoading(false);
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })
                .finally(async () => {
                    await setIsLoading(false);
                });
        }
        await setIsLoading(false);
    };

    /**
     * Set the Loading State from the Data
     * @param state
     * @returns {Promise<void>}
     */
    const setIsLoading = async (state) => {
        if (typeof state !== 'undefined' || state !== null) {
            isLoading.value = state;
        }
    };

    /**
     * Set the Item of the Default Values
     * @returns {Promise<void>}
     */
    const resetDefaultDataById = async () => {
        dataById.value = Object.assign({}, dataByIdDefault.value);
    }

    /**
     * Generate the array for Dropdown Used
     * @param items
     * @returns {Promise<void>}
     */
    const generateDataForDropdown = async (items) => {
        await setIsLoading(true);
        dataForDropdown.value = [];
        if (items && items.length > 0) {
            for (const item of items) {
                if (await item && item.id && item.abbreviation && item.description) {
                    dataForDropdown.value.push({
                        value: `${item.id}`,
                        label: `${item.description} (${item.abbreviation})`
                    });
                }
            }
        }
        await setIsLoading(false);
    };

    return {
        // States
        dataById, dataForDropdown, dataForTable, dataTotalCount, dataFilterCount, isLoading,
        // Actions
        generateDataForDropdown, loadDataForRegistration, loadDataForTable, loadDataById, resetDefaultDataById
    }
})