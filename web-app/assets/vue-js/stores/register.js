import {defineStore} from "pinia";
import {ref} from "vue";

let nameOfTheStore = `Register`;
export const useRegisterStore = defineStore(`store${nameOfTheStore}`, () => {
    // State
    const dataById = ref({
        federalState: {
            id: ''
        },
        department: {
            id: '',
            federalState: {
                id: '',
            },
            abbreviation: '',
            description: '',
            zipCode: '',
            placeName: '',
            street: '',
            comment: 'Angelegt, bei der Registration'
        },
        userGroup: {
            id: '',
            federalState: {
                id: '',
            },
            department: {
                id: '',
            },
            abbreviation: '',
            description: '',
            comment: 'Angelegt, bei der Registration'
        },
        rank: {
            id: ''
        },
        surname: '',
        firstname: '',
        staffNumber: '',
        email: '',
        comment: 'Angelegt, bei der Registration'
    });
    // Getters

    // Actions

    return {
        // States
        dataById,
        // Actions
    };
})
