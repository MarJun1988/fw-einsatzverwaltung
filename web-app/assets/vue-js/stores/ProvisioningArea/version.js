import {defineStore} from "pinia";
import {ref} from "vue";
import axios from "axios";
import {useApiStore} from "@/stores/api";
import {checkUserIsLoggedIn, generateOrderBy} from "@/utils";

let nameOfTheStore = `Version`;
export const useVersionStore = defineStore(`store${nameOfTheStore}`, () => {
    // State
    const dataById = ref({
        id: null,
        number: '',
        buildNumber: '',
        description: '',
        isVisibility: true,
        comment: ''
    });
    const dataByIdDefault = ref({
        id: null,
        number: '',
        buildNumber: '',
        description: '',
        isVisibility: true,
        comment: ''
    });
    const dataForDropdown = ref([]);
    const dataForTable = ref([]);
    const dataTotalCount = ref(0);
    const dataFilterCount = ref(0);
    const isLoading = ref(false);
    const lastVersion = ref({});
    // Getters

    // Actions
    /**
     * Load all Entries from the Database (api)
     * @param lazyParams
     * @returns {Promise<void>}
     */
    const loadDataForTable = async (lazyParams = null) => {
        await setIsLoading(true);
        let para = {};
        let url = `${useApiStore().versions}`;

        if (lazyParams) {
            para = {
                limit: lazyParams.rows, offset: lazyParams.first, order: [], filter: []
            };

            if (lazyParams.multiSortMeta) {
                para.order = generateOrderBy(lazyParams.multiSortMeta);
            }

            // Für die Filterfunktion
            for (const [key, value] of Object.entries(lazyParams.filters)) {
                // Wichtig ist das man auch false nimmt, für die Abfrage von Boolean Werten
                if (value.value || value.value === false) {
                    para.filter.push({
                        column: key, text: value.value, matchMode: value.matchMode,
                    });
                }
            }

            url += `?${JSON.stringify(para)}`;
        }

        axios.get(url)
                .then(async (response) => {
                    // Prüfen ob angemeldet ist
                    checkUserIsLoggedIn(response);
                if (response && response.data && response.data.items) {
                    dataForTable.value = await response.data.items;
                    await generateDataForDropdown(response.data.items);
                    await getLastVersion(response.data.items);
                }
                if (response && response.data && response.data.totalCount) {
                    dataTotalCount.value = await response.data.totalCount;
                }
                if (response && response.data && response.data.filterCount) {
                    dataFilterCount.value = await response.data.filterCount;
                }
                await setIsLoading(false);

            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .finally(async () => {
                await setIsLoading(false);
            });

    };

    /**
     * Load one Entry with the ID from the Database (api)
     * @param id The ID from the Entry
     * @returns {Promise<void>}
     */
    const loadDataById = async (id) => {
        await setIsLoading(true);
        if (id) {
            axios.get(`${useApiStore().version.getById.slice(0, -4)}/${id}`)
                .then(async function (response) {
                    if (response && response.data && response.data.item) {
                        dataById.value = response.data.item;
                    }
                    await setIsLoading(false);
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })
                .finally(async () => {
                    await setIsLoading(false);
                });
        }
    };

    /**
     * Set the Loading State from the Data
     * @param state
     * @returns {Promise<void>}
     */
    const setIsLoading = async (state) => {
        if (typeof state !== 'undefined' || state !== null) {
            isLoading.value = state;
        }
    };

    /**
     * Set the Item of the Default Values
     * @returns {Promise<void>}
     */
    const resetDefaultDataById = async () => {
        dataById.value = Object.assign({}, dataByIdDefault.value);
    };

    /**
     * Generate the array for Dropdown Used
     * @param items
     * @returns {Promise<void>}
     */
    const generateDataForDropdown = async (items) => {
        dataForDropdown.value = [];
        if (items && items.length > 0) {
            for (const item of items) {
                if (await item && item.id && item.abbreviation && item.description) {
                    dataForDropdown.value.push({
                        value: `${item.id}`,
                        label: `${item.description} (${item.abbreviation})`
                    });
                }
            }
        }
    };

    /**
     * Retrieves the last version from an array of versions.
     *
     * @param {Array<Object>} versions - An array of versions to search from.
     * @returns {Promise<void>} - A promise that resolves once the last version has been retrieved.
     */
    const getLastVersion = async (versions) => {
        if (versions && versions.length > 0) {
            const sortedVersions = versions.sort((a, b) => {
                if (a.number < b.number) {
                    return 1;
                }
                if (a.number > b.number) {
                    return -1;
                }
                return 0;
            })
            lastVersion.value = sortedVersions[0];
        }
    };


    return {
        // States
        dataById, dataForDropdown, dataForTable, dataTotalCount, dataFilterCount, isLoading, lastVersion,
        // Actions
        loadDataForTable, loadDataById, resetDefaultDataById
    }
})