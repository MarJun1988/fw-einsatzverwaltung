import {useTranslationStore} from "@/stores/translation";
import pinia from "@/stores/store";
import {mainMenuNames, mainMenuRoutes, userAreaNames, userAreaRoutes} from "@/router/default";
// import {useDefaultRightStore} from "@/stores/defaultRight";

// Store - Übersetzungen
const storeTranslation = useTranslationStore(pinia);
const {menus} = (storeTranslation)
// Store - Standard-Rechte
// const defaultRightStore = useDefaultRightStore(pinia);

const routesToolbar = [
    // Hilfebereich
    {
        path: `${mainMenuRoutes.helpArea}`,
        name: `${mainMenuNames.helpArea}`,
        component: () => import("@/views/HelpArea.vue"),
        meta: {
            mainMenu: false,
            toolbar: true,
            requiresAuth: false,
            requiresRole: [],
            label: `${menus.toolbar.helpArea.label}`,
            icon: `${menus.toolbar.helpArea.icon}`,
            tooltip: `${menus.toolbar.helpArea.tooltip}`,
        },
        children: []
    },
    // Nutzermenü
    {
        path: `${mainMenuRoutes.userArea}`,
        name: `${mainMenuNames.userArea}`,
        components: {
            default: () => import("@/views/UserArea.vue"),
        },
        meta: {
            mainMenu: false,
            toolbar: true,
            requiresAuth: false,
            requiresRole: [],
            label: `${menus.toolbar.userArea.label}`,
            icon: `${menus.toolbar.userArea.icon}`,
            tooltip: `${menus.toolbar.userArea.tooltip}`,
        },
        children: [
            {
                path: `${userAreaRoutes.settings}`,
                name: `${userAreaNames.settings}`,
                components: {
                    view: () => import("@/views/UserArea/Settings.vue")
                },
                meta: {
                    mainMenu: false,
                    requiresAuth: true,
                    requiresRole: ['ROLE_USER'],
                    label: `${menus.toolbar.userArea.children.settings.label}`,
                    icon: `${menus.toolbar.userArea.children.settings.icon}`,
                },
            },
            {
                path: `${userAreaRoutes.changePassword}`,
                name: `${userAreaNames.changePassword}`,
                components: {
                    view: () => import("@/views/UserArea/ChangePassword.vue")
                },
                meta: {
                    mainMenu: false,
                    requiresAuth: true,
                    requiresRole: ['ROLE_USER'],
                    label: `${menus.toolbar.userArea.children.changePassword.label}`,
                    icon: `${menus.toolbar.userArea.children.changePassword.icon}`,
                },
            },
            {
                path: `${userAreaRoutes.logout}`,
                name: `${userAreaNames.logout}`,
                components: {
                    view: () => import("@/views/Authentication/Login.vue")
                },
                meta: {
                    mainMenu: false,
                    requiresAuth: true,
                    requiresRole: [],
                    label: `${menus.toolbar.userArea.children.logout.label}`,
                    icon: `${menus.toolbar.userArea.children.logout.icon}`,
                },
            },
        ]
    }
];

export {
    routesToolbar
};