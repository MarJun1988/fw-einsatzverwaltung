import {useTranslationStore} from "@/stores/translation";
import pinia from "@/stores/store";
import {mainMenuNames, mainMenuRoutes, provisioningAreaNames, provisioningAreaRoutes} from "@/router/default";
import {useDefaultRightRoleStore} from "@/stores/defaultRightRole";

// Store - Übersetzungen
const storeTranslation = useTranslationStore(pinia);
const {menus, generals} = (storeTranslation)

// Store - Berechtigung
const storeDefaultRightRole = useDefaultRightRoleStore();
const {right} = storeDefaultRightRole;

const routesProvisioningArea = {
    path: `${mainMenuRoutes.provisioningArea}`,
    name: `${mainMenuNames.provisioningArea}`,
    redirect: `${provisioningAreaRoutes.federalstates}`,
    components: {
        default: () => import("@/views/ProvisioningArea.vue"),
    },
    meta: {
        label: `${menus.mainMenu.provisioningArea.label}`,
        mainMenu: true,
        requiresAuth: true,
        requiresRole: right.mainMenu.provisioningArea
    },
    children: [
        // Bundesländer
        {
            path: `${provisioningAreaRoutes.federalstates}`,
            name: `${provisioningAreaNames.federalstates}`,
            components: {
                view: () => import("@/views/ProvisioningArea/FederalStates.vue"),
                dialog: () => import("@/views/ProvisioningArea/FederalStateDialog.vue")
            },
            meta: {
                mainMenu: false,
                requiresAuth: true,
                requiresRole: right.federalstate.read,
                label: `${menus.mainMenu.provisioningArea.children.federalstates.label}`,
                icon: `${menus.mainMenu.provisioningArea.children.federalstates.icon}`,
            },
            children: [
                {
                    path: `${provisioningAreaRoutes.federalstateNew}`,
                    name: `${provisioningAreaNames.federalstateNew}`,
                    components: {
                        dialog: () => import("@/views/ProvisioningArea/FederalStateDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.federalstate.new,
                        label: `${menus.mainMenu.provisioningArea.children.federalstates.children.label} - ${generals.siteTitleNewItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${provisioningAreaRoutes.federalstateEdit}/:id`,
                    name: `${provisioningAreaNames.federalstateEdit}`,
                    components: {
                        dialog: () => import("@/views/ProvisioningArea/FederalStateDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.federalstate.edit,
                        label: `${menus.mainMenu.provisioningArea.children.federalstates.children.label} - ${generals.siteTitleEditItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${provisioningAreaRoutes.federalstateDelete}/:id`,
                    name: `${provisioningAreaNames.federalstateDelete}`,
                    components: {
                        dialog: () => import("@/views/ProvisioningArea/FederalStateDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.federalstate.delete,
                        label: `${menus.mainMenu.provisioningArea.children.federalstates.children.label} - ${generals.siteTitleDeleteItem}`,
                        icon: ``,
                    },
                },
            ]
        },
        // Nutzerrollen
        {
            path: `${provisioningAreaRoutes.userRoles}`,
            name: `${provisioningAreaNames.userRoles}`,
            components: {
                view: () => import("@/views/ProvisioningArea/UserRoles.vue"),
                dialog: () => import("@/views/ProvisioningArea/UserRoleDialog.vue")
            },
            meta: {
                mainMenu: false,
                requiresAuth: true,
                requiresRole: right.userRole.read,
                label: `${menus.mainMenu.provisioningArea.children.userRoles.label}`,
                icon: `${menus.mainMenu.provisioningArea.children.userRoles.icon}`,
            },
            children: [
                {
                    path: `${provisioningAreaRoutes.userRoleNew}`,
                    name: `${provisioningAreaNames.userRoleNew}`,
                    components: {
                        dialog: () => import("@/views/ProvisioningArea/UserRoleDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.userRole.new,
                        label: `${menus.mainMenu.provisioningArea.children.userRoles.children.label} -${generals.siteTitleNewItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${provisioningAreaRoutes.userRoleEdit}/:id`,
                    name: `${provisioningAreaNames.userRoleEdit}`,
                    components: {
                        dialog: () => import("@/views/ProvisioningArea/UserRoleDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.userRole.edit,
                        label: `${menus.mainMenu.provisioningArea.children.userRoles.children.label} - ${generals.siteTitleEditItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${provisioningAreaRoutes.userRoleDelete}/:id`,
                    name: `${provisioningAreaNames.userRoleDelete}`,
                    components: {
                        dialog: () => import("@/views/ProvisioningArea/UserRoleDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.userRole.delete,
                        label: `${menus.mainMenu.provisioningArea.children.userRoles.children.label} - ${generals.siteTitleDeleteItem}`,
                        icon: ``,
                    },
                },
            ]
        },
        // Versionen
        {
            path: `${provisioningAreaRoutes.versions}`,
            name: `${provisioningAreaNames.versions}`,
            components: {
                view: () => import("@/views/ProvisioningArea/Versions.vue"),
                dialog: () => import("@/views/ProvisioningArea/VersionDialog.vue")
            },
            meta: {
                mainMenu: false,
                requiresAuth: true,
                requiresRole: right.version.read,
                label: `${menus.mainMenu.provisioningArea.children.versions.label}`,
                icon: `${menus.mainMenu.provisioningArea.children.versions.icon}`,
            },
            children: [
                {
                    path: `${provisioningAreaRoutes.versionNew}`,
                    name: `${provisioningAreaNames.versionNew}`,
                    components: {
                        dialog: () => import("@/views/ProvisioningArea/VersionDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.version.new,
                        label: `${menus.mainMenu.provisioningArea.children.versions.children.label} - ${generals.siteTitleNewItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${provisioningAreaRoutes.versionEdit}/:id`,
                    name: `${provisioningAreaNames.versionEdit}`,
                    components: {
                        dialog: () => import("@/views/ProvisioningArea/VersionDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.version.edit,
                        label: `${menus.mainMenu.provisioningArea.children.versions.children.label} - ${generals.siteTitleEditItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${provisioningAreaRoutes.versionDelete}/:id`,
                    name: `${provisioningAreaNames.versionDelete}`,
                    components: {
                        dialog: () => import("@/views/ProvisioningArea/VersionDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.version.delete,
                        label: `${menus.mainMenu.provisioningArea.children.versions.children.label} - ${generals.siteTitleDeleteItem}`,
                        icon: ``,
                    },
                },
            ]
        }
    ]
}

export {
    routesProvisioningArea
};