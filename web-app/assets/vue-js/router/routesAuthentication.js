import {useTranslationStore} from "@/stores/translation";
import pinia from "@/stores/store";
import {mainMenuNames, mainMenuRoutes} from "@/router/default";

// Store - Übersetzungen
const storeTranslation = useTranslationStore(pinia);
const {menus} = (storeTranslation)

const routesAuthentication = [
    {
        path: `${mainMenuRoutes.login}`,
        name: `${mainMenuNames.login}`,
        component: () => import("@/views/Authentication/Login.vue"),
        meta: {
            label: `${menus.authentication.login.label}`,
            mainMenu: false,
            requiresAuth: false,
            requiresRole: []
        },
        children: []
    },
    {
        path: `${mainMenuRoutes.logout}`,
        name: `${mainMenuNames.logout}`,
        component: () => import("@/views/Authentication/Login.vue"),
        meta: {
            label: `${menus.authentication.logout.label}`,
            mainMenu: false,
            requiresAuth: false,
            requiresRole: []
        },
        children: []
    },
    {
        path: `${mainMenuRoutes.noRights}`,
        name: `${mainMenuNames.noRights}`,
        components: {
            view: () =>import("@/views/Authentication/NoRights.vue")
        },
        meta: {
            label: `${menus.authentication.noRights.label}`,
            mainMenu: false,
            requiresAuth: false,
            requiresRole: []
        },
        children: []
    }
];

export {
    routesAuthentication
};