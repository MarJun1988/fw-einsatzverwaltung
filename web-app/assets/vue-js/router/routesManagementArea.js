import {useTranslationStore} from "@/stores/translation";
import pinia from "@/stores/store";
import {mainMenuNames, mainMenuRoutes, managementAreaNames, managementAreaRoutes} from "@/router/default";
import {useDefaultRightRoleStore} from "@/stores/defaultRightRole";

// Store - Übersetzungen
const storeTranslation = useTranslationStore(pinia);
const {menus, generals} = (storeTranslation)

// Store - Berechtigung
const storeDefaultRightRole = useDefaultRightRoleStore();
const {right} = storeDefaultRightRole;

const routesManagementArea = {
    path: `${mainMenuRoutes.managementArea}`,
    name: `${mainMenuNames.managementArea}`,
    redirect: `${managementAreaRoutes.generals}`,
    components: {
        default: () => import("@/views/ManagementArea.vue"),
    },
    meta: {
        label: `${menus.mainMenu.managementArea.label}`,
        mainMenu: true,
        requiresAuth: true,
        requiresRole: right.mainMenu.managementArea
    },
    children: [
        // Grundeinstellungen
        {
            path: `${managementAreaRoutes.generals}`,
            name: `${managementAreaNames.generals}`,
            components: {
                view: () => import("@/views/ManagementArea/Generals.vue"),
                dialog: () => import("@/views/OperationArea/AllIncomingAlertDialog.vue")
            },
            meta: {
                mainMenu: false,
                requiresAuth: true,
                requiresRole: ['ROLE_USER'],
                label: `${menus.mainMenu.managementArea.children.generals.label}`,
                icon: `${menus.mainMenu.managementArea.children.generals.icon}`,
            },
            children: []
        },
        // Nutzer
        {
            path: `${managementAreaRoutes.users}`,
            name: `${managementAreaNames.users}`,
            components: {
                view: () => import("@/views/ManagementArea/Users.vue"),
                dialog: () => import("@/views/ManagementArea/UserDialog.vue")
            },
            meta: {
                mainMenu: false,
                requiresAuth: true,
                requiresRole: right.user.read,
                label: `${menus.mainMenu.managementArea.children.users.label}`,
                icon: `${menus.mainMenu.managementArea.children.users.icon}`,
            },
            children: [
                {
                    path: `${managementAreaRoutes.userNew}`,
                    name: `${managementAreaNames.userNew}`,
                    components: {
                        dialog: () => import("@/views/ManagementArea/UserDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.user.new,
                        label: `${menus.mainMenu.managementArea.children.users.children.label} - ${generals.siteTitleNewItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${managementAreaRoutes.userEdit}/:id`,
                    name: `${managementAreaNames.userEdit}`,
                    components: {
                        dialog: () => import("@/views/ManagementArea/UserDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.user.edit,
                        label: `${menus.mainMenu.managementArea.children.users.children.label} - ${generals.siteTitleEditItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${managementAreaRoutes.userDelete}/:id`,
                    name: `${managementAreaNames.userDelete}`,
                    components: {
                        dialog: () => import("@/views/ManagementArea/UserDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.user.delete,
                        label: `${menus.mainMenu.managementArea.children.users.children.label} - ${generals.siteTitleDeleteItem}`,
                        icon: ``,
                    },
                },
            ]
        },
        // Mitglieder
        {
            path: `${managementAreaRoutes.members}`,
            name: `${managementAreaNames.members}`,
            components: {
                view: () => import("@/views/ManagementArea/Members.vue"),
                dialog: () => import("@/views/ManagementArea/MemberDialog.vue")
            },
            meta: {
                mainMenu: false,
                requiresAuth: true,
                requiresRole: right.member.read,
                label: `${menus.mainMenu.managementArea.children.members.label}`,
                icon: `${menus.mainMenu.managementArea.children.members.icon}`,
            },
            children: [
                {
                    path: `${managementAreaRoutes.memberNew}`,
                    name: `${managementAreaNames.memberNew}`,
                    components: {
                        dialog: () => import("@/views/ManagementArea/MemberDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.member.new,
                        label: `${menus.mainMenu.managementArea.children.members.children.label} - ${generals.siteTitleNewItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${managementAreaRoutes.memberEdit}/:id`,
                    name: `${managementAreaNames.memberEdit}`,
                    components: {
                        dialog: () => import("@/views/ManagementArea/MemberDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.member.edit,
                        label: `${menus.mainMenu.managementArea.children.members.children.label} - ${generals.siteTitleEditItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${managementAreaRoutes.memberDelete}/:id`,
                    name: `${managementAreaNames.memberDelete}`,
                    components: {
                        dialog: () => import("@/views/ManagementArea/MemberDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.member.delete,
                        label: `${menus.mainMenu.managementArea.children.members.children.label} - ${generals.siteTitleDeleteItem}`,
                        icon: ``,
                    },
                },
            ]
        },
        // Alarm Adressen (RICS)
        {
            path: `${managementAreaRoutes.alertAddresses}`,
            name: `${managementAreaNames.alertAddresses}`,
            components: {
                view: () => import("@/views/ManagementArea/AlertAddresses.vue"),
                dialog: () => import("@/views/ManagementArea/AlertAddressDialog.vue")
            },
            meta: {
                mainMenu: false,
                requiresAuth: true,
                requiresRole: right.alertAddress.read,
                label: `${menus.mainMenu.managementArea.children.alertAddresses.label}`,
                icon: `${menus.mainMenu.managementArea.children.alertAddresses.icon}`,
            },
            children: [
                {
                    path: `${managementAreaRoutes.alertAddressNew}`,
                    name: `${managementAreaNames.alertAddressNew}`,
                    components: {
                        dialog: () => import("@/views/ManagementArea/AlertAddressDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.alertAddress.new,
                        label: `${menus.mainMenu.managementArea.children.alertAddresses.children.label} - ${generals.siteTitleNewItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${managementAreaRoutes.alertAddressEdit}/:id`,
                    name: `${managementAreaNames.alertAddressEdit}`,
                    components: {
                        dialog: () => import("@/views/ManagementArea/AlertAddressDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.alertAddress.edit,
                        label: `${menus.mainMenu.managementArea.children.alertAddresses.children.label} - ${generals.siteTitleEditItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${managementAreaRoutes.alertAddressDelete}/:id`,
                    name: `${managementAreaNames.alertAddressDelete}`,
                    components: {
                        dialog: () => import("@/views/ManagementArea/AlertAddressDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.alertAddress.delete,
                        label: `${menus.mainMenu.managementArea.children.alertAddresses.children.label} - ${generals.siteTitleDeleteItem}`,
                        icon: ``,
                    },
                },
            ]
        },
        // Tools - Kategorien
        {
            path: `${managementAreaRoutes.toolCategories}`,
            name: `${managementAreaNames.toolCategories}`,
            components: {
                view: () => import("@/views/ManagementArea/ToolCategories.vue"),
                dialog: () => import("@/views/ManagementArea/ToolCategoryDialog.vue")
            },
            meta: {
                mainMenu: false,
                requiresAuth: true,
                requiresRole: right.toolCategory.read,
                label: `${menus.mainMenu.managementArea.children.toolCategories.label}`,
                icon: `${menus.mainMenu.managementArea.children.toolCategories.icon}`,
            },
            children: [
                {
                    path: `${managementAreaRoutes.toolCategoryNew}`,
                    name: `${managementAreaNames.toolCategoryNew}`,
                    components: {
                        dialog: () => import("@/views/ManagementArea/ToolCategoryDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.toolCategory.new,
                        label: `${menus.mainMenu.managementArea.children.toolCategories.children.label} - ${generals.siteTitleNewItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${managementAreaRoutes.toolCategoryEdit}/:id`,
                    name: `${managementAreaNames.toolCategoryEdit}`,
                    components: {
                        dialog: () => import("@/views/ManagementArea/ToolCategoryDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.toolCategory.edit,
                        label: `${menus.mainMenu.managementArea.children.toolCategories.children.label} - ${generals.siteTitleEditItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${managementAreaRoutes.toolCategoryDelete}/:id`,
                    name: `${managementAreaNames.toolCategoryDelete}`,
                    components: {
                        dialog: () => import("@/views/ManagementArea/ToolCategoryDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.toolCategory.delete,
                        label: `${menus.mainMenu.managementArea.children.toolCategories.children.label} - ${generals.siteTitleDeleteItem}`,
                        icon: ``,
                    },
                },
            ]
        },
        // Tools
        {
            path: `${managementAreaRoutes.tools}`,
            name: `${managementAreaNames.tools}`,
            components: {
                view: () => import("@/views/ManagementArea/Tools.vue"),
                dialog: () => import("@/views/ManagementArea/ToolDialog.vue")
            },
            meta: {
                mainMenu: false,
                requiresAuth: true,
                requiresRole: right.tool.read,
                label: `${menus.mainMenu.managementArea.children.tools.label}`,
                icon: `${menus.mainMenu.managementArea.children.tools.icon}`,
            },
            children: [
                {
                    path: `${managementAreaRoutes.toolNew}`,
                    name: `${managementAreaNames.toolNew}`,
                    components: {
                        dialog: () => import("@/views/ManagementArea/ToolDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.tool.new,
                        label: `${menus.mainMenu.managementArea.children.tools.children.label} - ${generals.siteTitleNewItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${managementAreaRoutes.toolEdit}/:id`,
                    name: `${managementAreaNames.toolEdit}`,
                    components: {
                        dialog: () => import("@/views/ManagementArea/ToolDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.tool.edit,
                        label: `${menus.mainMenu.managementArea.children.tools.children.label} - ${generals.siteTitleEditItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${managementAreaRoutes.toolDelete}/:id`,
                    name: `${managementAreaNames.toolDelete}`,
                    components: {
                        dialog: () => import("@/views/ManagementArea/ToolDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.tool.delete,
                        label: `${menus.mainMenu.managementArea.children.tools.children.label} - ${generals.siteTitleDeleteItem}`,
                        icon: ``,
                    },
                },
            ]
        },
        // Fahrzeuge
        {
            path: `${managementAreaRoutes.vehicles}`,
            name: `${managementAreaNames.vehicles}`,
            components: {
                view: () => import("@/views/ManagementArea/Vehicles.vue"),
                dialog: () => import("@/views/ManagementArea/VehicleDialog.vue")
            },
            meta: {
                mainMenu: false,
                requiresAuth: true,
                requiresRole: right.vehicle.read,
                label: `${menus.mainMenu.managementArea.children.vehicles.label}`,
                icon: `${menus.mainMenu.managementArea.children.vehicles.icon}`,
            },
            children: [
                {
                    path: `${managementAreaRoutes.vehicleNew}`,
                    name: `${managementAreaNames.vehicleNew}`,
                    components: {
                        dialog: () => import("@/views/ManagementArea/VehicleDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.vehicle.new,
                        label: `${menus.mainMenu.managementArea.children.vehicles.children.label} - ${generals.siteTitleNewItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${managementAreaRoutes.vehicleEdit}/:id`,
                    name: `${managementAreaNames.vehicleEdit}`,
                    components: {
                        dialog: () => import("@/views/ManagementArea/VehicleDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.vehicle.edit,
                        label: `${menus.mainMenu.managementArea.children.vehicles.children.label} - ${generals.siteTitleEditItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${managementAreaRoutes.vehicleDelete}/:id`,
                    name: `${managementAreaNames.vehicleDelete}`,
                    components: {
                        dialog: () => import("@/views/ManagementArea/VehicleDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.vehicle.delete,
                        label: `${menus.mainMenu.managementArea.children.vehicles.children.label} - ${generals.siteTitleDeleteItem}`,
                        icon: ``,
                    },
                },
            ]
        },
    ]
}

export {
    routesManagementArea
};