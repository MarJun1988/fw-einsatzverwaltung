import {useTranslationStore} from "@/stores/translation";
import pinia from "@/stores/store";
import {mainMenuNames, mainMenuRoutes, referenceAreaNames, referenceAreaRoutes} from "@/router/default";
import {useDefaultRightRoleStore} from "@/stores/defaultRightRole";

// Store - Übersetzungen
const storeTranslation = useTranslationStore(pinia);
const {menus, generals} = (storeTranslation)

// Store - Berechtigung
const storeDefaultRightRole = useDefaultRightRoleStore();
const {right} = storeDefaultRightRole;

const routesReferenceArea = {
    path: `${mainMenuRoutes.referenceArea}`,
    name: `${mainMenuNames.referenceArea}`,
    redirect: `${referenceAreaRoutes.departments}`,
    components: {
        default: () => import("@/views/ReferenceArea.vue"),
    },
    meta: {
        label: `${menus.mainMenu.referenceArea.label}`,
        mainMenu: true,
        requiresAuth: true,
        requiresRole: right.mainMenu.referenceArea
    },
    children: [
        // Dienststellen
        {
            path: `${referenceAreaRoutes.departments}`,
            name: `${referenceAreaNames.departments}`,
            components: {
                view: () => import("@/views/ReferenceArea/Departments.vue"),
                dialog: () => import("@/views/ReferenceArea/DepartmentDialog.vue")
            },
            meta: {
                mainMenu: false,
                requiresAuth: true,
                requiresRole: right.department.read,
                label: `${menus.mainMenu.referenceArea.children.departments.label}`,
                icon: `${menus.mainMenu.referenceArea.children.departments.icon}`,
            },
            children: [
                {
                    path: `${referenceAreaRoutes.departmentNew}`,
                    name: `${referenceAreaNames.departmentNew}`,
                    components: {
                        dialog: () => import("@/views/ReferenceArea/DepartmentDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.department.new,
                        label: `${menus.mainMenu.referenceArea.children.departments.children.label} - ${generals.siteTitleNewItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${referenceAreaRoutes.departmentEdit}/:id`,
                    name: `${referenceAreaNames.departmentEdit}`,
                    components: {
                        dialog: () => import("@/views/ReferenceArea/DepartmentDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.department.edit,
                        label: `${menus.mainMenu.referenceArea.children.departments.children.label} - ${generals.siteTitleEditItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${referenceAreaRoutes.departmentDelete}/:id`,
                    name: `${referenceAreaNames.departmentDelete}`,
                    components: {
                        dialog: () => import("@/views/ReferenceArea/DepartmentDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.department.delete,
                        label: `${menus.mainMenu.referenceArea.children.departments.children.label} - ${generals.siteTitleDeleteItem}`,
                        icon: ``,
                    },
                },
            ]
        },
        // Funktionen der Mitglieder
        {
            path: `${referenceAreaRoutes.memberFunctions}`,
            name: `${referenceAreaNames.memberFunctions}`,
            components: {
                view: () => import("@/views/ReferenceArea/MemberFunctions.vue"),
                dialog: () => import("@/views/ReferenceArea/MemberFunctionDialog.vue")
            },
            meta: {
                mainMenu: false,
                requiresAuth: true,
                requiresRole: right.memberFunction.read,
                label: `${menus.mainMenu.referenceArea.children.memberFunctions.label}`,
                icon: `${menus.mainMenu.referenceArea.children.memberFunctions.icon}`,
            },
            children: [
                {
                    path: `${referenceAreaRoutes.memberFunctionNew}`,
                    name: `${referenceAreaNames.memberFunctionNew}`,
                    components: {
                        dialog: () => import("@/views/ReferenceArea/MemberFunctionDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.memberFunction.new,
                        label: `${menus.mainMenu.referenceArea.children.memberFunctions.children.label} - ${generals.siteTitleNewItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${referenceAreaRoutes.memberFunctionEdit}/:id`,
                    name: `${referenceAreaNames.memberFunctionEdit}`,
                    components: {
                        dialog: () => import("@/views/ReferenceArea/MemberFunctionDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.memberFunction.edit,
                        label: `${menus.mainMenu.referenceArea.children.memberFunctions.children.label} - ${generals.siteTitleEditItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${referenceAreaRoutes.memberFunctionDelete}/:id`,
                    name: `${referenceAreaNames.memberFunctionDelete}`,
                    components: {
                        dialog: () => import("@/views/ReferenceArea/MemberFunctionDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.memberFunction.delete,
                        label: `${menus.mainMenu.referenceArea.children.memberFunctions.children.label} - ${generals.siteTitleDeleteItem}`,
                        icon: ``,
                    },
                },
            ]
        },
        // Einsatzarten - Kategorien
        {
            path: `${referenceAreaRoutes.operationTypeCategories}`,
            name: `${referenceAreaNames.operationTypeCategories}`,
            components: {
                view: () => import("@/views/ReferenceArea/OperationTypeCategories.vue"),
                dialog: () => import("@/views/ReferenceArea/OperationTypeCategoryDialog.vue")
            },
            meta: {
                mainMenu: false,
                requiresAuth: true,
                requiresRole: right.operationTypeCategory.read,
                label: `${menus.mainMenu.referenceArea.children.operationTypeCategories.label}`,
                icon: `${menus.mainMenu.referenceArea.children.operationTypeCategories.icon}`,
            },
            children: [
                {
                    path: `${referenceAreaRoutes.operationTypeCategoryNew}`,
                    name: `${referenceAreaNames.operationTypeCategoryNew}`,
                    components: {
                        dialog: () => import("@/views/ReferenceArea/OperationTypeCategoryDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.operationTypeCategory.new,
                        label: `${menus.mainMenu.referenceArea.children.operationTypeCategories.children.label} - ${generals.siteTitleNewItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${referenceAreaRoutes.operationTypeCategoryEdit}/:id`,
                    name: `${referenceAreaNames.operationTypeCategoryEdit}`,
                    components: {
                        dialog: () => import("@/views/ReferenceArea/OperationTypeCategoryDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.operationTypeCategory.edit,
                        label: `${menus.mainMenu.referenceArea.children.operationTypeCategories.children.label} - ${generals.siteTitleEditItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${referenceAreaRoutes.operationTypeCategoryDelete}/:id`,
                    name: `${referenceAreaNames.operationTypeCategoryDelete}`,
                    components: {
                        dialog: () => import("@/views/ReferenceArea/OperationTypeCategoryDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.operationTypeCategory.delete,
                        label: `${menus.mainMenu.referenceArea.children.operationTypeCategories.children.label} - ${generals.siteTitleDeleteItem}`,
                        icon: ``,
                    },
                },
            ]
        },
        // Einsatzarten
        {
            path: `${referenceAreaRoutes.operationTypes}`,
            name: `${referenceAreaNames.operationTypes}`,
            components: {
                view: () => import("@/views/ReferenceArea/OperationTypes.vue"),
                dialog: () => import("@/views/ReferenceArea/OperationTypeDialog.vue")
            },
            meta: {
                mainMenu: false,
                requiresAuth: true,
                requiresRole: right.operationType.read,
                label: `${menus.mainMenu.referenceArea.children.operationTypes.label}`,
                icon: `${menus.mainMenu.referenceArea.children.operationTypes.icon}`,
            },
            children: [
                {
                    path: `${referenceAreaRoutes.operationTypeNew}`,
                    name: `${referenceAreaNames.operationTypeNew}`,
                    components: {
                        dialog: () => import("@/views/ReferenceArea/OperationTypeDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.operationType.new,
                        label: `${menus.mainMenu.referenceArea.children.operationTypes.children.label} - ${generals.siteTitleNewItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${referenceAreaRoutes.operationTypeEdit}/:id`,
                    name: `${referenceAreaNames.operationTypeEdit}`,
                    components: {
                        dialog: () => import("@/views/ReferenceArea/OperationTypeDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.operationType.edit,
                        label: `${menus.mainMenu.referenceArea.children.operationTypes.children.label} - ${generals.siteTitleEditItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${referenceAreaRoutes.operationTypeDelete}/:id`,
                    name: `${referenceAreaNames.operationTypeDelete}`,
                    components: {
                        dialog: () => import("@/views/ReferenceArea/OperationTypeDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.operationType.delete,
                        label: `${menus.mainMenu.referenceArea.children.operationTypes.children.label} - ${generals.siteTitleDeleteItem}`,
                        icon: ``,
                    },
                },
            ]
        },
    ]
}

export {
    routesReferenceArea
};