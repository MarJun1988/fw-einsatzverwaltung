import {useTranslationStore} from "@/stores/translation";
import pinia from "@/stores/store";
import {evaluationAreaNames, evaluationAreaRoutes, mainMenuNames, mainMenuRoutes} from "@/router/default";
import {useDefaultRightRoleStore} from "@/stores/defaultRightRole";

// Store - Übersetzungen
const storeTranslation = useTranslationStore(pinia);
const {menus, generals} = (storeTranslation)

// Store - Berechtigung
const storeDefaultRightRole = useDefaultRightRoleStore();
const {right} = storeDefaultRightRole;

const routesEvaluationArea = {
    path: `${mainMenuRoutes.evaluationArea}`,
    name: `${mainMenuNames.evaluationArea}`,
    redirect: `${evaluationAreaRoutes.feedbacks}`,
    components: {
        default: () => import("@/views/EvaluationArea.vue"),
    },
    meta: {
        addLineBefore: true,
        label: `${menus.mainMenu.evaluationArea.label}`,
        mainMenu: true,
        requiresAuth: true,
        requiresRole: right.mainMenu.evaluationArea
    },
    children: [
        {
            path: `${evaluationAreaRoutes.myIncomingAlerts}`,
            name: `${evaluationAreaNames.myIncomingAlerts}`,
            components: {
                view: () => import("@/views/EvaluationArea/MyIncomingAlerts.vue")
            },
            meta: {
                mainMenu: false,
                requiresAuth: true,
                requiresRole: ['ROLE_USER'],
                // requiresRole: right.myIncomingAlerts.read,
                label: `${menus.mainMenu.evaluationArea.children.myIncomingAlerts.label}`,
                icon: `${menus.mainMenu.evaluationArea.children.myIncomingAlerts.icon}`,
            },
            children: []
        },
        {
            path: `${evaluationAreaRoutes.operationReports}`,
            name: `${evaluationAreaNames.operationReports}`,
            components: {
                view: () => import("@/views/EvaluationArea/OperationReports.vue")
            },
            meta: {
                mainMenu: false,
                requiresAuth: true,
                requiresRole: right.myOperationReports.read,
                label: `${menus.mainMenu.evaluationArea.children.operationReports.label}`,
                icon: `${menus.mainMenu.evaluationArea.children.operationReports.icon}`,
            },
            children: []
        },
        {
            path: `${evaluationAreaRoutes.feedbacks}`,
            name: `${evaluationAreaNames.feedbacks}`,
            components: {
                view: () => import("@/views/EvaluationArea/Feedbacks.vue"),
                dialog: () => import("@/views/EvaluationArea/FeedbackDialog.vue")
            },
            meta: {
                mainMenu: false,
                requiresAuth: true,
                requiresRole: right.feedbacks.read,
                label: `${menus.mainMenu.evaluationArea.children.feedbacks.label}`,
                icon: `${menus.mainMenu.evaluationArea.children.feedbacks.icon}`,
            },
            children: [
                {
                    path: `${evaluationAreaRoutes.feedbackNew}`,
                    name: `${evaluationAreaNames.feedbackNew}`,
                    components: {
                        dialog: () => import("@/views/EvaluationArea/FeedbackDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.feedbacks.new,
                        label: `${menus.mainMenu.evaluationArea.children.feedbacks.children.label} - ${generals.siteTitleNewItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${evaluationAreaRoutes.feedbackEdit}/:id`,
                    name: `${evaluationAreaNames.feedbackEdit}`,
                    components: {
                        dialog: () => import("@/views/EvaluationArea/FeedbackDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.feedbacks.edit,
                        label: `${menus.mainMenu.evaluationArea.children.feedbacks.children.label} - ${generals.siteTitleEditItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${evaluationAreaRoutes.feedbackDelete}/:id`,
                    name: `${evaluationAreaNames.feedbackDelete}`,
                    components: {
                        dialog: () => import("@/views/EvaluationArea/FeedbackDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.feedbacks.delete,
                        label: `${menus.mainMenu.evaluationArea.children.feedbacks.children.label} - ${generals.siteTitleDeleteItem}`,
                        icon: ``,
                    },
                },
            ]},
    ]
}

export {
    routesEvaluationArea
};