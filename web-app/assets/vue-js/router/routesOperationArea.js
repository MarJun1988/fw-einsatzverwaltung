import pinia from "@/stores/store";
import {useTranslationStore} from "@/stores/translation";
import {mainMenuNames, mainMenuRoutes, operationAreaNames, operationAreaRoutes} from "@/router/default";
import {useDefaultRightRoleStore} from "@/stores/defaultRightRole";

// Store - Übersetzungen
const storeTranslation = useTranslationStore(pinia);
const {menus, generals} = (storeTranslation)

// Store - Berechtigung
const storeDefaultRightRole = useDefaultRightRoleStore(pinia);
const {right} = (storeDefaultRightRole);

const routesOperationArea = {
    path: `${mainMenuRoutes.operationArea}`,
    name: `${mainMenuNames.operationArea}`,
    redirect: `${operationAreaRoutes.operationReports}`,
    components: {
        default: () => import("@/views/OperationArea.vue"),
    },
    meta: {
        label: `${menus.mainMenu.operationArea.label}`,
        mainMenu: true,
        requiresAuth: true,
        requiresRole: right.mainMenu.operationArea
    },
    children: [
        // Alle Einsätze
        {
            path: `${operationAreaRoutes.allIncomingAlerts}`,
            name: `${operationAreaNames.allIncomingAlerts}`,
            components: {
                view: () => import("@/views/OperationArea/AllIncomingAlerts.vue"),
                dialog: () => import("@/views/OperationArea/AllIncomingAlertDialog.vue")
            },
            meta: {
                mainMenu: false,
                requiresAuth: true,
                requiresRole: right.allIncomingAlert.read,
                label: `${menus.mainMenu.operationArea.children.allIncomingAlerts.label}`,
                icon: `${menus.mainMenu.operationArea.children.allIncomingAlerts.icon}`,
            },
            children: [
                {
                    path: `${operationAreaRoutes.allIncomingAlertNew}`,
                    name: `${operationAreaNames.allIncomingAlertNew}`,
                    components: {
                        dialog: () => import("@/views/OperationArea/AllIncomingAlertDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.allIncomingAlert.new,
                        label: `${menus.mainMenu.operationArea.children.allIncomingAlerts.children.label} - ${generals.siteTitleNewItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${operationAreaRoutes.allIncomingAlertEdit}/:id`,
                    name: `${operationAreaNames.allIncomingAlertEdit}`,
                    components: {
                        dialog: () => import("@/views/OperationArea/AllIncomingAlertDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.allIncomingAlert.edit,
                        label: `${menus.mainMenu.operationArea.children.allIncomingAlerts.children.label} - ${generals.siteTitleEditItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${operationAreaRoutes.allIncomingAlertDelete}/:id`,
                    name: `${operationAreaNames.allIncomingAlertDelete}`,
                    components: {
                        dialog: () => import("@/views/OperationArea/AllIncomingAlertDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.allIncomingAlert.delete,
                        label: `${menus.mainMenu.operationArea.children.allIncomingAlerts.children.label} - ${generals.siteTitleDeleteItem}`,
                        icon: ``,
                    },
                },
            ]
        },
        // Einsätze, wo man die RIC sehen darf
        {
            path: `${operationAreaRoutes.myIncomingAlerts}`,
            name: `${operationAreaNames.myIncomingAlerts}`,
            components: {
                view: () => import("@/views/OperationArea/MyIncomingAlerts.vue"),
                dialog: () => import("@/views/OperationArea/MyIncomingAlertsDialog.vue")
            },
            meta: {
                mainMenu: false,
                requiresAuth: true,
                requiresRole: right.myIncomingAlert.read,
                label: `${menus.mainMenu.operationArea.children.myIncomingAlerts.label}`,
                icon: `${menus.mainMenu.operationArea.children.myIncomingAlerts.icon}`,
            },
            children: [
                {
                    path: `${operationAreaRoutes.myIncomingAlertNew}`,
                    name: `${operationAreaNames.myIncomingAlertNew}`,
                    components: {
                        dialog: () => import("@/views/OperationArea/MyIncomingAlertsDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.myIncomingAlert.new,
                        label: `${menus.mainMenu.operationArea.children.myIncomingAlerts.children.label} - ${generals.siteTitleNewItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${operationAreaRoutes.myIncomingAlertEdit}/:id`,
                    name: `${operationAreaNames.myIncomingAlertEdit}`,
                    components: {
                        dialog: () => import("@/views/OperationArea/MyIncomingAlertsDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.myIncomingAlert.edit,
                        label: `${menus.mainMenu.operationArea.children.myIncomingAlerts.children.label} - ${generals.siteTitleEditItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${operationAreaRoutes.myIncomingAlertDelete}/:id`,
                    name: `${operationAreaNames.myIncomingAlertDelete}`,
                    components: {
                        dialog: () => import("@/views/OperationArea/MyIncomingAlertsDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.myIncomingAlert.delete,
                        label: `${menus.mainMenu.operationArea.children.myIncomingAlerts.children.label} - ${generals.siteTitleDeleteItem}`,
                        icon: ``,
                    },
                },
            ]
        },
        // Einsatzberichte
        {
            path: `${operationAreaRoutes.operationReports}`,
            name: `${operationAreaNames.operationReports}`,
            components: {
                view: () => import("@/views/OperationArea/OperationReports.vue"),
                // dialog: () => import("@/views/OperationArea/AllIncomingAlertDialog.vue")
            },
            meta: {
                mainMenu: false,
                requiresAuth: true,
                requiresRole: right.operationReport.read,
                label: `${menus.mainMenu.operationArea.children.operationReports.label}`,
                icon: `${menus.mainMenu.operationArea.children.operationReports.icon}`,
            },
            children: [
                {
                    path: `${operationAreaRoutes.operationReportNew}`,
                    name: `${operationAreaNames.operationReportNew}`,
                    components: {
                        step: () => import("@/views/OperationArea/OperationReportStep.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.operationReport.new,
                        label: `${menus.mainMenu.operationArea.children.operationReports.children.label} - ${generals.siteTitleNewItem}`,
                        icon: ``,
                    },
                    children: [
                        {
                            path: `${operationAreaRoutes.operationReportNewStepOne}/:alertId?`,
                            name: `${operationAreaNames.operationReportNewStepOne}`,
                            props: false,
                            components: {
                                step: () => import("@/views/OperationArea/OperationReport/StepOne.vue")
                            },
                            meta: {
                                mainMenu: false,
                                requiresAuth: true,
                                requiresRole: right.operationReport.new,
                                label: `${menus.mainMenu.operationArea.children.operationReports.children.headlines.one}`,
                                icon: ``,
                                isStep: true
                            },
                        },
                        {
                            path: `${operationAreaRoutes.operationReportNewStepTwo}`,
                            name: `${operationAreaNames.operationReportNewStepTwo}`,
                            components: {
                                step: () => import("@/views/OperationArea/OperationReport/StepTwo.vue")
                            },
                            meta: {
                                mainMenu: false,
                                requiresAuth: true,
                                requiresRole: right.operationReport.new,
                                label: `${menus.mainMenu.operationArea.children.operationReports.children.headlines.two}`,
                                icon: ``,
                                isStep: true
                            },
                        },
                        {
                            path: `${operationAreaRoutes.operationReportNewStepThree}`,
                            name: `${operationAreaNames.operationReportNewStepThree}`,
                            components: {
                                step: () => import("@/views/OperationArea/OperationReport/StepThree.vue")
                            },
                            meta: {
                                mainMenu: false,
                                requiresAuth: true,
                                requiresRole: right.operationReport.new,
                                label: `${menus.mainMenu.operationArea.children.operationReports.children.headlines.three}`,
                                icon: ``,
                                isStep: true
                            },
                        },
                        {
                            path: `${operationAreaRoutes.operationReportNewStepFour}`,
                            name: `${operationAreaNames.operationReportNewStepFour}`,
                            components: {
                                step: () => import("@/views/OperationArea/OperationReport/StepFour.vue")
                            },
                            meta: {
                                mainMenu: false,
                                requiresAuth: true,
                                requiresRole: right.operationReport.new,
                                label: `${menus.mainMenu.operationArea.children.operationReports.children.headlines.four}`,
                                icon: ``,
                                isStep: true
                            },
                        },
                        {
                            path: `${operationAreaRoutes.operationReportNewStepFive}`,
                            name: `${operationAreaNames.operationReportNewStepFive}`,
                            components: {
                                step: () => import("@/views/OperationArea/OperationReport/StepFive.vue")
                            },
                            meta: {
                                mainMenu: false,
                                requiresAuth: true,
                                requiresRole: right.operationReport.new,
                                label: `${menus.mainMenu.operationArea.children.operationReports.children.headlines.five}`,
                                icon: ``,
                                isStep: true
                            },
                        },
                        {
                            path: `${operationAreaRoutes.operationReportNewStepSix}`,
                            name: `${operationAreaNames.operationReportNewStepSix}`,
                            components: {
                                step: () => import("@/views/OperationArea/OperationReport/StepSix.vue")
                            },
                            meta: {
                                mainMenu: false,
                                requiresAuth: true,
                                requiresRole: right.operationReport.new,
                                label: `${menus.mainMenu.operationArea.children.operationReports.children.headlines.six}`,
                                icon: ``,
                                isStep: true
                            },
                        },
                    ]
                },
                {
                    path: `${operationAreaRoutes.operationReportEdit}/:id`,
                    name: `${operationAreaNames.operationReportEdit}`,
                    components: {
                        step: () => import("@/views/OperationArea/OperationReportStep.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.operationReport.edit,
                        label: `${menus.mainMenu.operationArea.children.operationReports.children.label} - ${generals.siteTitleEditItem}`,
                        icon: ``,
                    },
                    children: [
                        {
                            path: `${operationAreaRoutes.operationReportEditStepOne}/:id`,
                            name: `${operationAreaNames.operationReportEditStepOne}`,
                            components: {
                                step: () => import("@/views/OperationArea/OperationReport/StepOne.vue")
                            },
                            meta: {
                                mainMenu: false,
                                requiresAuth: true,
                                requiresRole: right.operationReport.edit,
                                label: `${menus.mainMenu.operationArea.children.operationReports.children.headlines.one}`,
                                icon: ``,
                                isStep: true
                            },
                        },
                        {
                            path: `${operationAreaRoutes.operationReportEditStepTwo}/:id`,
                            name: `${operationAreaNames.operationReportEditStepTwo}`,
                            components: {
                                step: () => import("@/views/OperationArea/OperationReport/StepTwo.vue")
                            },
                            meta: {
                                mainMenu: false,
                                requiresAuth: true,
                                requiresRole: right.operationReport.edit,
                                label: `${menus.mainMenu.operationArea.children.operationReports.children.headlines.two}`,
                                icon: ``,
                                isStep: true
                            },
                        },
                        {
                            path: `${operationAreaRoutes.operationReportEditStepThree}/:id`,
                            name: `${operationAreaNames.operationReportEditStepThree}`,
                            components: {
                                step: () => import("@/views/OperationArea/OperationReport/StepThree.vue")
                            },
                            meta: {
                                mainMenu: false,
                                requiresAuth: true,
                                requiresRole: right.operationReport.edit,
                                label: `${menus.mainMenu.operationArea.children.operationReports.children.headlines.three}`,
                                icon: ``,
                                isStep: true
                            },
                        },
                        {
                            path: `${operationAreaRoutes.operationReportEditStepFour}/:id`,
                            name: `${operationAreaNames.operationReportEditStepFour}`,
                            components: {
                                step: () => import("@/views/OperationArea/OperationReport/StepFour.vue")
                            },
                            meta: {
                                mainMenu: false,
                                requiresAuth: true,
                                requiresRole: right.operationReport.edit,
                                label: `${menus.mainMenu.operationArea.children.operationReports.children.headlines.four}`,
                                icon: ``,
                                isStep: true
                            },
                        },
                        {
                            path: `${operationAreaRoutes.operationReportEditStepFive}/:id`,
                            name: `${operationAreaNames.operationReportEditStepFive}`,
                            components: {
                                step: () => import("@/views/OperationArea/OperationReport/StepFive.vue")
                            },
                            meta: {
                                mainMenu: false,
                                requiresAuth: true,
                                requiresRole: right.operationReport.edit,
                                label: `${menus.mainMenu.operationArea.children.operationReports.children.headlines.five}`,
                                icon: ``,
                                isStep: true
                            },
                        },
                        {
                            path: `${operationAreaRoutes.operationReportEditStepSix}/:id`,
                            name: `${operationAreaNames.operationReportEditStepSix}`,
                            components: {
                                step: () => import("@/views/OperationArea/OperationReport/StepSix.vue")
                            },
                            meta: {
                                mainMenu: false,
                                requiresAuth: true,
                                requiresRole: right.operationReport.edit,
                                label: `${menus.mainMenu.operationArea.children.operationReports.children.headlines.six}`,
                                icon: ``,
                                isStep: true
                            },
                        },
                    ]
                },
                {
                    path: `${operationAreaRoutes.operationReportDelete}/:id`,
                    name: `${operationAreaNames.operationReportDelete}`,
                    components: {
                        dialog: () => import("@/views/OperationArea/AllIncomingAlertDialog.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.operationReport.delete,
                        label: `${menus.mainMenu.operationArea.children.operationReports.children.label} - ${generals.siteTitleDeleteItem}`,
                        icon: ``,
                    },
                },
                {
                    path: `${operationAreaRoutes.operationReportDetails}/:id`,
                    name: `${operationAreaNames.operationReportDetails}`,
                    components: {
                        view: () => import("@/views/OperationArea/OperationReport/OperationReportDetails.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.operationReport.read,
                        label: `${menus.mainMenu.operationArea.children.operationReports.label}`,
                        icon: ``,
                    },
                },
                {
                    path: `${operationAreaRoutes.operationReportExport}/:id`,
                    name: `${operationAreaNames.operationReportExport}`,
                    components: {
                        view: () => import("@/views/OperationArea/OperationReports.vue"),
                        dialog: () => import("@/views/OperationArea/OperationReport/OperationReportExport.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.operationReport.read,
                        label: `${menus.mainMenu.operationArea.children.operationReports.label}`,
                        icon: ``,
                    },
                },
                {
                    path: `${operationAreaRoutes.operationReportShowPdf}/:id`,
                    name: `${operationAreaNames.operationReportShowPdf}`,
                    components: {
                        view: () => import("@/views/OperationArea/OperationReports.vue"),
                        dialog: () => import("@/views/OperationArea/OperationReport/OperationReportShowPdf.vue")
                    },
                    meta: {
                        mainMenu: false,
                        requiresAuth: true,
                        requiresRole: right.operationReport.read,
                        label: `${menus.mainMenu.operationArea.children.operationReports.label}`,
                        icon: ``,
                    },
                },
            ]
        },
    ]
}

export {
    routesOperationArea
};