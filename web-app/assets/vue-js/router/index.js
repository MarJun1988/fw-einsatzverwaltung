import {createRouter, createWebHistory} from "vue-router";
import pinia from "@/stores/store";

import {useTranslationStore} from "@/stores/translation";
import {mainMenuNames, mainMenuRoutes, operationAreaNames, operationAreaRoutes, userAreaNames} from "@/router/default";
import {routesDashboard} from "@/router/routesDashboard";
import {routesOperationArea} from "@/router/routesOperationArea";
import {routesManagementArea} from "@/router/routesManagementArea";
import {routesReferenceArea} from "@/router/routesReferenceArea";
import {routesProvisioningArea} from "@/router/routesProvisioningArea";
import {routesEvaluationArea} from "@/router/routesEvaluationArea";
import {routesAuthentication} from "@/router/routesAuthentication";
import {storeToRefs} from "pinia";
import {useLoggedUserStore} from "@/stores/loggedUser";
import {routesToolbar} from "@/router/routesToolbar";
import {routesRegisterArea} from "@/router/routesRegisterArea";
import {useGeneralStore} from "@/stores/Managements/general";
import {useDefaultRightRoleStore} from "@/stores/defaultRightRole";
import {checkUserRight} from "@/utils";

// Translations-Store
const storeTranslation = useTranslationStore(pinia);
const {generals, menus} = storeTranslation;

// Store - Berechtigung
const storeDefaultRightRole = useDefaultRightRoleStore();
const {right} = storeDefaultRightRole;

// Store - Allgemeines
const storeGenerals = useGeneralStore();
// Auslesen der Title, aus der Datenbank
const {
    siteNameApplicationLong,
    siteNameApplicationShort
} = storeToRefs(storeGenerals);
storeGenerals.loadDataForTable();

const router = createRouter({
    history: createWebHistory(), routes: [
        // Standard Seite
        {
            path: `${mainMenuRoutes.default}`,
            name: `${mainMenuNames.default}`,
            redirect: `${mainMenuNames.dashboard}`,
            component: () => import("@/views/DefaultSite.vue"),
            meta: {
                label: `${menus.mainMenu.label}`,
                mainMenu: false,
                requiresAuth: true,
                requiresRole: []
            },
            children: []
        },
        // Dashboard
        routesDashboard,
        // Einsatzbereich
        routesOperationArea,
        // Verwaltung
        routesManagementArea,
        // Nachschlagebereich
        routesReferenceArea,
        // Bereitstellungsbereich
        routesProvisioningArea,
        // Auswertung
        routesEvaluationArea,
        // Authentifizierung
        routesAuthentication[0],
        routesAuthentication[1],
        routesAuthentication[2],
        // Symbole des Rechts im Header liegen
        // Hilfebereich
        routesToolbar[0],
        // Nutzermenü
        routesToolbar[1],
        // Registrierung neue Nutzer
        routesRegisterArea,
        // Seite nicht gefunden
        {
            path: "/:catchAll(.*)",
            component: () => import('@/views/NotFound.vue')
        },

    ]
});

router.beforeEach((to, from) => {

    // Rechte Prüfen
    // Store - angemeldete Nutzer
    const storeLoggedUser = useLoggedUserStore();
    const {loggedUser} = storeToRefs(storeLoggedUser);

    // Festlegung des Seiten Titles
    let siteTitle = siteNameApplicationLong.value.value ? siteNameApplicationLong.value.value : `${generals.siteTitle}`;
    let siteTitleShort = siteNameApplicationShort.value.value ? siteNameApplicationShort.value.value : `${generals.siteTitleShort}`;

    // Festlegung der Seiten Titels
    window.document.title = to && to.meta && to.meta.label ? `${siteTitleShort} - ${to.meta.label}` : siteTitle;

    // Weiterleitung zur Anmeldeseite
    if (!loggedUser.value.isLoggedIn && to.name !== mainMenuNames.login) {
        router.push({name: `${mainMenuNames.login}`});
    } else if (loggedUser.value.isLoggedIn && to.name === mainMenuNames.login) {
        router.push({name: `${mainMenuNames.dashboard}`});
    }

    // Nach dem Abmelden, zum Anmeldebildschirm leiten
    // Wichtig "window.location.href" nutzen, damit die abmeldung richtig läuft
    if (to.name === userAreaNames.logout) {
        setTimeout(() => {
            window.location.href = `${mainMenuRoutes.logout}`;
        }, 1000);
    }

    let rolesArray = [];
    if (storeLoggedUser.loggedUser.rolesInherited) {
        let rolesKeys = Reflect.ownKeys(storeLoggedUser.loggedUser.rolesInherited);
        rolesArray = rolesKeys.reduce((arr, key) => {
            arr.push(storeLoggedUser.loggedUser.rolesInherited[key]);
            return arr;
        }, []);
    }

    // Prüfen, ob die erforderlichen Rechte vorhanden sind
    if (to.meta.requiresAuth && loggedUser.value &&
        loggedUser.value.isLoggedIn &&
        to.meta.requiresRole && to.meta.requiresRole.length > 0 &&
        checkUserRight(loggedUser.value.roles, to.meta.requiresRole )
    ) {
        // console.log("=>(index.js:121) to.name", to.name);

        // if (to.name === operationAreaNames.operationReports) {
        //       return {name: `${operationAreaRoutes.operationReports}`, params: {'year': new Date().getFullYear()}}
        // } else {
            return true
        // }
    } else {
        // redirect the no rights Site
        if (to.name !== `${mainMenuNames.noRights}` && (to.name !== mainMenuNames.login && to.name !== mainMenuNames.logout)) {
            return {name: `${mainMenuNames.noRights}`}
        }
    }
});

export default router;