import {useTranslationStore} from "@/stores/translation";
import pinia from "@/stores/store";
import {
    mainMenuNames,
    mainMenuRoutes,
    registerAreaRoutes,
    registerAreaNames
} from "@/router/default";

// Store - Übersetzungen
const storeTranslation = useTranslationStore(pinia);
const {menus} = (storeTranslation)

const routesRegisterArea =
    {
        path: `${mainMenuRoutes.registerArea}`,
        name: `${mainMenuNames.registerArea}`,
        redirect: `${registerAreaRoutes.personalInformation}`,
        component: () => import("@/views/RegisterArea.vue"),
        meta: {
            mainMenu: false,
            requiresAuth: false,
            requiresRole: [],
            label: `${menus.registerArea.label}`
        },
        children: [
            {
                path: `${registerAreaRoutes.personalInformation}`,
                name: `${registerAreaNames.personalInformation}`,
                component: () => import("@/views/RegisterArea/PersonalInformation.vue"),
                meta: {
                    mainMenu: false,
                    requiresAuth: false,
                    requiresRole: [],
                    label: `${menus.registerArea.children.personalInformation.label}`,
                    icon: `${menus.registerArea.children.personalInformation.icon}`,
                },
            },
            {
                path: `${registerAreaRoutes.officeInformation}`,
                name: `${registerAreaNames.officeInformation}`,
                component: () => import("@/views/RegisterArea/OfficeInformation.vue"),
                meta: {
                    mainMenu: false,
                    requiresAuth: false,
                    requiresRole: [],
                    label: `${menus.registerArea.children.officeInformation.label}`,
                    icon: `${menus.registerArea.children.officeInformation.icon}`,
                },
            },
            {
                path: `${registerAreaRoutes.overviewInformation}`,
                name: `${registerAreaNames.overviewInformation}`,
                component: () => import("@/views/RegisterArea/OverviewInformation.vue"),
                meta: {
                    mainMenu: false,
                    requiresAuth: false,
                    requiresRole: [],
                    label: `${menus.registerArea.children.overviewInformation.label}`,
                    icon: `${menus.registerArea.children.overviewInformation.icon}`,
                },
            },
        ]
    };

export {
    routesRegisterArea
};