import {useTranslationStore} from "@/stores/translation";
import pinia from "@/stores/store";
import {mainMenuNames, mainMenuRoutes} from "@/router/default";
import {useDefaultRightRoleStore} from "@/stores/defaultRightRole";

// Store - Übersetzungen
const storeTranslation = useTranslationStore(pinia);
const {menus} = (storeTranslation)

// Store - Berechtigung
const storeDefaultRightRole = useDefaultRightRoleStore();
const {right} = storeDefaultRightRole;

const routesDashboard = {
    path: `${mainMenuRoutes.dashboard}`,
    name: `${mainMenuNames.dashboard}`,
    component: () => import("@/views/Dashboard.vue"),
    meta: {
        label: `${menus.mainMenu.dashboard.label}`,
        mainMenu: true,
        requiresAuth: true,
        requiresRole: right.mainMenu.dashboard
    },
    children: []
}

export {
    routesDashboard
};