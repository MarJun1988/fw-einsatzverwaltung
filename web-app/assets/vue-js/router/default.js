// Hauptmenü
const mainMenuRoutes = {
    default: '/',
    dashboard: '/dashboard',
    operationArea: '/operation-area',
    managementArea: '/management-area',
    referenceArea: '/reference-area',
    provisioningArea: '/provisioning-area',
    evaluationArea: '/evaluation-area',
    registerArea: '/register-area',
    login: '/login',
    logout: '/logout',
    helpArea: '/help-area',
    userArea: '/user-area',
    noRights: '/no-rights'
};
const mainMenuNames = {
    default: 'default',
    dashboard: 'dashboard',
    operationArea: 'operationArea',
    managementArea: 'managementArea',
    referenceArea: 'referenceArea',
    provisioningArea: 'provisioningArea',
    evaluationArea: 'evaluationArea',
    registerArea: 'registerArea',
    login: 'login',
    logout: 'logout',
    helpArea: 'helpArea',
    userArea: 'userArea',
    noRights: 'noRights'
};
// Einsatzbereich
const operationAreaRoutes = {
    // Alle Einsätze
    allIncomingAlerts: `${mainMenuRoutes.operationArea}/all-incoming-alerts`,
    allIncomingAlertNew: `${mainMenuRoutes.operationArea}/all-incoming-alert/new`,
    allIncomingAlertEdit: `${mainMenuRoutes.operationArea}/all-incoming-alert/edit`,
    allIncomingAlertDelete: `${mainMenuRoutes.operationArea}/all-incoming-alert/delete`,
    // Einsätze, wo man die RIC sehen darf
    myIncomingAlerts: `${mainMenuRoutes.operationArea}/my-incoming-alerts`,
    myIncomingAlertNew: `${mainMenuRoutes.operationArea}/my-incoming-alert/new`,
    myIncomingAlertEdit: `${mainMenuRoutes.operationArea}/my-incoming-alert/edit`,
    myIncomingAlertDelete: `${mainMenuRoutes.operationArea}/my-incoming-alert/delete`,
    // Einsatzberichte
    operationReports: `${mainMenuRoutes.operationArea}/operation-reports`,
    operationReportNew: `${mainMenuRoutes.operationArea}/operation-report/new`,
    operationReportNewStepOne: `${mainMenuRoutes.operationArea}/operation-report/new/step=1`,
    operationReportNewStepTwo: `${mainMenuRoutes.operationArea}/operation-report/new/step=2`,
    operationReportNewStepThree: `${mainMenuRoutes.operationArea}/operation-report/new/step=3`,
    operationReportNewStepFour: `${mainMenuRoutes.operationArea}/operation-report/new/step=4`,
    operationReportNewStepFive: `${mainMenuRoutes.operationArea}/operation-report/new/step=5`,
    operationReportNewStepSix: `${mainMenuRoutes.operationArea}/operation-report/new/step=6`,
    operationReportEdit: `${mainMenuRoutes.operationArea}/operation-report/edit`,
    operationReportEditStepOne: `${mainMenuRoutes.operationArea}/operation-report/edit/step=1`,
    operationReportEditStepTwo: `${mainMenuRoutes.operationArea}/operation-report/edit/step=2`,
    operationReportEditStepThree: `${mainMenuRoutes.operationArea}/operation-report/edit/step=3`,
    operationReportEditStepFour: `${mainMenuRoutes.operationArea}/operation-report/edit/step=4`,
    operationReportEditStepFive: `${mainMenuRoutes.operationArea}/operation-report/edit/step=5`,
    operationReportEditStepSix: `${mainMenuRoutes.operationArea}/operation-report/edit/step=6`,
    operationReportDelete: `${mainMenuRoutes.operationArea}/operation-report/delete`,
    operationReportDetails: `${mainMenuRoutes.operationArea}/operation-report/details`,
    operationReportExport: `${mainMenuRoutes.operationArea}/operation-report/export`,
    operationReportShowPdf: `${mainMenuRoutes.operationArea}/operation-report/show-pdf`
};
const operationAreaNames = {
    // Alle Einsätze
    allIncomingAlerts: `${mainMenuNames.operationArea}-allIncomingAlerts`,
    allIncomingAlertNew: `${mainMenuNames.operationArea}-allIncomingAlert-new`,
    allIncomingAlertEdit: `${mainMenuNames.operationArea}-allIncomingAlert-edit`,
    allIncomingAlertDelete: `${mainMenuNames.operationArea}-allIncomingAlert-delete`,
    // Einsätze, wo man die RIC sehen darf
    myIncomingAlerts: `${mainMenuNames.operationArea}-myIncomingAlerts`,
    myIncomingAlertNew: `${mainMenuNames.operationArea}-myIncomingAlert-new`,
    myIncomingAlertEdit: `${mainMenuNames.operationArea}-myIncomingAlert-edit`,
    myIncomingAlertDelete: `${mainMenuNames.operationArea}-myIncomingAlert-delete`,
    // Einsatzberichte
    operationReports: `${mainMenuNames.operationArea}-operationReports`,
    operationReportNew: `${mainMenuNames.operationArea}-operationReport-new`,
    operationReportNewStepOne: `${mainMenuNames.operationArea}-operationReport-new-step-one`,
    operationReportNewStepTwo: `${mainMenuNames.operationArea}-operationReport-new-step-two`,
    operationReportNewStepThree: `${mainMenuNames.operationArea}-operationReport-new-step-three`,
    operationReportNewStepFour: `${mainMenuNames.operationArea}-operationReport-new-step-four`,
    operationReportNewStepFive: `${mainMenuNames.operationArea}-operationReport-new-step-five`,
    operationReportNewStepSix: `${mainMenuNames.operationArea}-operationReport-new-step-six`,
    operationReportEdit: `${mainMenuNames.operationArea}-operationReport-edit`,
    operationReportEditStepOne: `${mainMenuNames.operationArea}-operationReport-edit-step-one`,
    operationReportEditStepTwo: `${mainMenuNames.operationArea}-operationReport-edit-step-two`,
    operationReportEditStepThree: `${mainMenuNames.operationArea}-operationReport-edit-step-three`,
    operationReportEditStepFour: `${mainMenuNames.operationArea}-operationReport-edit-step-four`,
    operationReportEditStepFive: `${mainMenuNames.operationArea}-operationReport-edit-step-five`,
    operationReportEditStepSix: `${mainMenuNames.operationArea}-operationReport-edit-step-six`,
    operationReportDelete: `${mainMenuNames.operationArea}-operationReport-delete`,
    operationReportDetails: `${mainMenuNames.operationArea}-operationReport-details`,
    operationReportExport: `${mainMenuNames.operationArea}-operationReport-export`,
    operationReportShowPdf: `${mainMenuNames.operationArea}-operationReport-show-pdf`
};
// Verwaltung
const managementAreaRoutes = {
    // Grundeinstellungen
    generals: `${mainMenuRoutes.managementArea}/generals`,
    // Nutzer
    users: `${mainMenuRoutes.managementArea}/users`,
    userNew: `${mainMenuRoutes.managementArea}/user/new`,
    userEdit: `${mainMenuRoutes.managementArea}/user/edit`,
    userDelete: `${mainMenuRoutes.managementArea}/user/delete`,
    // Mitglieder
    members: `${mainMenuRoutes.managementArea}/members`,
    memberNew: `${mainMenuRoutes.managementArea}/member/new`,
    memberEdit: `${mainMenuRoutes.managementArea}/member/edit`,
    memberDelete: `${mainMenuRoutes.managementArea}/member/delete`,
    // Alarm Adressen (RICS)
    alertAddresses: `${mainMenuRoutes.managementArea}/alert-addresses`,
    alertAddressNew: `${mainMenuRoutes.managementArea}/alert-address/new`,
    alertAddressEdit: `${mainMenuRoutes.managementArea}/alert-address/edit`,
    alertAddressDelete: `${mainMenuRoutes.managementArea}/alert-address/delete`,
    // Tools - Kategorien
    toolCategories: `${mainMenuRoutes.managementArea}/tool-categories`,
    toolCategoryNew: `${mainMenuRoutes.managementArea}/tool-category/new`,
    toolCategoryEdit: `${mainMenuRoutes.managementArea}/tool-category/edit`,
    toolCategoryDelete: `${mainMenuRoutes.managementArea}/tool-category/delete`,
    // Tools
    tools: `${mainMenuRoutes.managementArea}/tools`,
    toolNew: `${mainMenuRoutes.managementArea}/tool/new`,
    toolEdit: `${mainMenuRoutes.managementArea}/tool/edit`,
    toolDelete: `${mainMenuRoutes.managementArea}/tool/delete`,
    // Fahrzeuge
    vehicles: `${mainMenuRoutes.managementArea}/vehicles`,
    vehicleNew: `${mainMenuRoutes.managementArea}/vehicle/new`,
    vehicleEdit: `${mainMenuRoutes.managementArea}/vehicle/edit`,
    vehicleDelete: `${mainMenuRoutes.managementArea}/vehicle/delete`,
};
const managementAreaNames = {
    // Grundeinstellungen
    generals: `${mainMenuNames.managementArea}-generals`,
    // Nutzer
    users: `${mainMenuRoutes.managementArea}-/users`,
    userNew: `${mainMenuRoutes.managementArea}-/user-new`,
    userEdit: `${mainMenuRoutes.managementArea}-/user-edit`,
    userDelete: `${mainMenuRoutes.managementArea}-/user-delete`,
    // Mitglieder
    members: `${mainMenuNames.managementArea}-members`,
    memberNew: `${mainMenuNames.managementArea}-member-new`,
    memberEdit: `${mainMenuNames.managementArea}-member-edit`,
    memberDelete: `${mainMenuNames.managementArea}-member-delete`,
    // Alarm Adressen (RICS)
    alertAddresses: `${mainMenuNames.managementArea}-alert-addresses`,
    alertAddressNew: `${mainMenuNames.managementArea}-alert-address-new`,
    alertAddressEdit: `${mainMenuNames.managementArea}-alert-address-edit`,
    alertAddressDelete: `${mainMenuNames.managementArea}-alert-address-delete`,
    // Tools - Kategorien
    toolCategories: `${mainMenuNames.managementArea}-tool-categories`,
    toolCategoryNew: `${mainMenuNames.managementArea}-tool-category-new`,
    toolCategoryEdit: `${mainMenuNames.managementArea}-tool-category-edit`,
    toolCategoryDelete: `${mainMenuNames.managementArea}-tool-category-delete`,
    // Tools
    tools: `${mainMenuNames.managementArea}-tools`,
    toolNew: `${mainMenuNames.managementArea}-tool-new`,
    toolEdit: `${mainMenuNames.managementArea}-tool-edit`,
    toolDelete: `${mainMenuNames.managementArea}-tool-delete`,
    // Fahrzeuge
    vehicles: `${mainMenuNames.managementArea}-vehicles`,
    vehicleNew: `${mainMenuNames.managementArea}-vehicle-new`,
    vehicleEdit: `${mainMenuNames.managementArea}-vehicle-edit`,
    vehicleDelete: `${mainMenuNames.managementArea}-vehicle-delete`,
};
// Nachschlagebereich
const referenceAreaRoutes = {
    // Dienststellen
    departments: `${mainMenuRoutes.referenceArea}/departments`,
    departmentNew: `${mainMenuRoutes.referenceArea}/department/new`,
    departmentEdit: `${mainMenuRoutes.referenceArea}/department/edit`,
    departmentDelete: `${mainMenuRoutes.referenceArea}/department/delete`,
    // Funktionen der Mitglieder
    memberFunctions: `${mainMenuRoutes.referenceArea}/member-functions`,
    memberFunctionNew: `${mainMenuRoutes.referenceArea}/member-function/new`,
    memberFunctionEdit: `${mainMenuRoutes.referenceArea}/member-function/edit`,
    memberFunctionDelete: `${mainMenuRoutes.referenceArea}/member-function/delete`,
    // Einsatzarten - Kategorien
    operationTypeCategories: `${mainMenuRoutes.referenceArea}/operation-type-categories`,
    operationTypeCategoryNew: `${mainMenuRoutes.referenceArea}/operation-type-category/new`,
    operationTypeCategoryEdit: `${mainMenuRoutes.referenceArea}/operation-type-category/edit`,
    operationTypeCategoryDelete: `${mainMenuRoutes.referenceArea}/operation-type-category/delete`,
    // Einsatzarten
    operationTypes: `${mainMenuRoutes.referenceArea}/operation-types`,
    operationTypeNew: `${mainMenuRoutes.referenceArea}/operation-type/new`,
    operationTypeEdit: `${mainMenuRoutes.referenceArea}/operation-type/edit`,
    operationTypeDelete: `${mainMenuRoutes.referenceArea}/operation-type/delete`,
};
const referenceAreaNames = {
    // Dienststellen
    departments: `${mainMenuNames.referenceArea}-departments`,
    departmentNew: `${mainMenuNames.referenceArea}-department-new`,
    departmentEdit: `${mainMenuNames.referenceArea}-department-edit`,
    departmentDelete: `${mainMenuNames.referenceArea}-department-delete`,
    // Funktionen der Mitglieder
    memberFunctions: `${mainMenuNames.referenceArea}-member-functions`,
    memberFunctionNew: `${mainMenuNames.referenceArea}-member-function-new`,
    memberFunctionEdit: `${mainMenuNames.referenceArea}-member-function-edit`,
    memberFunctionDelete: `${mainMenuNames.referenceArea}-member-function-delete`,
    // Einsatzarten - Kategorien
    operationTypeCategories: `${mainMenuNames.referenceArea}-operation-type-categories`,
    operationTypeCategoryNew: `${mainMenuNames.referenceArea}-operation-type-category-new`,
    operationTypeCategoryEdit: `${mainMenuNames.referenceArea}-operation-type-category-edit`,
    operationTypeCategoryDelete: `${mainMenuNames.referenceArea}-operation-type-category-delete`,
    // Einsatzarten
    operationTypes: `${mainMenuNames.referenceArea}/operation-types`,
    operationTypeNew: `${mainMenuNames.referenceArea}/operation-type-new`,
    operationTypeEdit: `${mainMenuNames.referenceArea}/operation-type-edit`,
    operationTypeDelete: `${mainMenuNames.referenceArea}/operation-type-delete`,
};
// Bereitstellungsdaten
const provisioningAreaRoutes = {
    // Bundesländer
    federalstates: `${mainMenuRoutes.provisioningArea}/federalstates`,
    federalstateNew: `${mainMenuRoutes.provisioningArea}/federalstate/new`,
    federalstateEdit: `${mainMenuRoutes.provisioningArea}/federalstate/edit`,
    federalstateDelete: `${mainMenuRoutes.provisioningArea}/federalstate/delete`,
    // Nutzerrollen
    userRoles: `${mainMenuRoutes.provisioningArea}/user-roles`,
    userRoleNew: `${mainMenuRoutes.provisioningArea}/user-role/new`,
    userRoleEdit: `${mainMenuRoutes.provisioningArea}/user-role/edit`,
    userRoleDelete: `${mainMenuRoutes.provisioningArea}/user-role/delete`,
    // Versionen
    versions: `${mainMenuRoutes.provisioningArea}/versions`,
    versionNew: `${mainMenuRoutes.provisioningArea}/version/new`,
    versionEdit: `${mainMenuRoutes.provisioningArea}/version/edit`,
    versionDelete: `${mainMenuRoutes.provisioningArea}/version/delete`,

};
const provisioningAreaNames = {
    // Bundesländer
    federalstates: `${mainMenuNames.provisioningArea}-federalstates`,
    federalstateNew: `${mainMenuNames.provisioningArea}-federalstate-new`,
    federalstateEdit: `${mainMenuNames.provisioningArea}-federalstate-edit`,
    federalstateDelete: `${mainMenuNames.provisioningArea}-federalstate-delete`,
    // Nutzerrollen
    userRoles: `${mainMenuNames.provisioningArea}-user-roles`,
    userRoleNew: `${mainMenuNames.provisioningArea}-user-role-new`,
    userRoleEdit: `${mainMenuNames.provisioningArea}-user-role-edit`,
    userRoleDelete: `${mainMenuNames.provisioningArea}-user-role-delete`,
    // Versionen
    versions: `${mainMenuNames.provisioningArea}-versions`,
    versionNew: `${mainMenuNames.provisioningArea}-version-new`,
    versionEdit: `${mainMenuNames.provisioningArea}-version-edit`,
    versionDelete: `${mainMenuNames.provisioningArea}-version-delete`,
};

// Auswertung
const evaluationAreaRoutes = {
    // Einsätze, wo man die RIC sehen darf
    myIncomingAlerts: `${mainMenuRoutes.evaluationArea}/my-incoming-alerts`,
    // Einsatzberichte
    operationReports: `${mainMenuRoutes.evaluationArea}/operation-reports`,
    // Feedbacks
    feedbacks: `${mainMenuRoutes.evaluationArea}/feedbacks`,
    feedbackNew: `${mainMenuRoutes.evaluationArea}/feedback/new`,
    feedbackEdit: `${mainMenuRoutes.evaluationArea}/feedback/edit`,
    feedbackDelete: `${mainMenuRoutes.evaluationArea}/feedback/delete`,
};
const evaluationAreaNames = {
    // Einsätze, wo man die RIC sehen darf
    myIncomingAlerts: `${mainMenuNames.evaluationArea}-my-incoming-alerts`,
    // Einsatzberichte
    operationReports: `${mainMenuNames.evaluationArea}-operation-reports`,
    // Feedbacks
    feedbacks: `${mainMenuNames.evaluationArea}/feedbacks`,
    feedbackNew: `${mainMenuNames.evaluationArea}-feedback-new`,
    feedbackEdit: `${mainMenuNames.evaluationArea}-feedback-edit`,
    feedbackDelete: `${mainMenuNames.evaluationArea}-feedback-delete`,
};

// Registration neuer Nutzer
const registerAreaRoutes = {
    // Persönliche Daten
    personalInformation: `${mainMenuRoutes.registerArea}/personal-information`,
    // Dienststellen Daten
    officeInformation: `${mainMenuRoutes.registerArea}/office-information`,
    // Übersicht
    overviewInformation: `${mainMenuRoutes.registerArea}/overview-information`,
};
const registerAreaNames = {
    // Persönliche Daten
    personalInformation: `${mainMenuNames.registerArea}-personal-information`,
    // Dienststellen Daten
    officeInformation: `${mainMenuNames.registerArea}-office-information`,
    // Übersicht
    overviewInformation: `${mainMenuNames.registerArea}-overview-information`,
};
// Nutzermenü
const userAreaRoutes = {
    // Persönliche Einstellungen
    settings: `${mainMenuRoutes.userArea}/personal-information`,
    changePassword: `${mainMenuRoutes.userArea}/change-password`,
    // Abmeldung
    logout: `${mainMenuRoutes.logout}`
};
const userAreaNames = {
    // Persönliche Einstellungen
    settings: `${mainMenuNames.userArea}-personal-information`,
    changePassword: `${mainMenuNames.userArea}-change-password`,
    // Abmeldung
    logout: `${mainMenuRoutes.userArea}-logout`
};

export {
    mainMenuRoutes,
    mainMenuNames,
    operationAreaRoutes,
    operationAreaNames,
    managementAreaRoutes,
    managementAreaNames,
    referenceAreaRoutes,
    referenceAreaNames,
    provisioningAreaRoutes,
    provisioningAreaNames,
    evaluationAreaRoutes,
    evaluationAreaNames,
    registerAreaRoutes,
    registerAreaNames,
    userAreaRoutes,
    userAreaNames
};