<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240428131355 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `feedbacks` (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', federal_state_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', department_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', title VARCHAR(255) NOT NULL, message LONGTEXT NOT NULL, comment VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_from VARCHAR(255) NOT NULL, updated_from VARCHAR(255) DEFAULT NULL, INDEX IDX_7E6C3F892E9EE243 (federal_state_id), INDEX IDX_7E6C3F89AE80F5DF (department_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `feedbacks` ADD CONSTRAINT FK_7E6C3F892E9EE243 FOREIGN KEY (federal_state_id) REFERENCES `federal_states` (id)');
        $this->addSql('ALTER TABLE `feedbacks` ADD CONSTRAINT FK_7E6C3F89AE80F5DF FOREIGN KEY (department_id) REFERENCES `departments` (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `feedbacks` DROP FOREIGN KEY FK_7E6C3F892E9EE243');
        $this->addSql('ALTER TABLE `feedbacks` DROP FOREIGN KEY FK_7E6C3F89AE80F5DF');
        $this->addSql('DROP TABLE `feedbacks`');
    }
}
