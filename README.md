# Feuerwehr Einsatzverwaltung

Dieses Projekt ist eine Webanwendung zur Pflege und Verwaltung von Einsatzberichten für Feuerwehren. Es ermöglicht das Erfassen und Verwalten von eingesetztem Personal, Fahrzeugen und Hilfsmitteln, die bei einem Feuerwehreinsatz benötigt werden.

## Technologie

Die Anwendung basiert auf dem [Symfony](https://symfony.com/) PHP Framework und [Vue.js](https://vuejs.org/) für das Frontend.

## Features

- Pflege und Verwaltung von Einsatzberichten
- Erfassung und Verwaltung von eingesetztem Personal, Fahrzeugen und Hilfsmitteln
- Automatische Eintragung von Einsätzen über eine API-Schnittstelle

## API

Die Anwendung bietet eine API-Schnittstelle, die es ermöglicht, Einsätze automatisch in die Anwendung zu importieren. Weitere Details zur API-Nutzung finden Sie in der API-Dokumentation.

## First Start

1. git clone https://gitlab.com/MarJun1988/fw-einsatzverwaltung.git
2. cd fw-einsatzverwaltung
3. cd docker
4. docker-compose -p "fw-operation-management" up -d

## Go to the URL with a Webbrowser

[http://127.0.0.1](http://127.0.0.1)

## Import old Data from Version 1.0

### Deployment Type Categories

[https://127.0.0.1/import/old-data/deployment-type-categories](https://127.0.0.1/import/old-data/deployment-type-categories)

### Deployment Types

[https://127.0.0.1/import/old-data/deployment-types](https://127.0.0.1/import/old-data/deployment-types)

### Vehicles

[https://127.0.0.1/import/old-data/vehicles](https://127.0.0.1/import/old-data/vehicles)

### Functions from the Members

[https://127.0.0.1/import/old-data/member-functions](https://127.0.0.1/import/old-data/member-functions)

### Members

[https://127.0.0.1/import/old-data/members](https://127.0.0.1/import/old-data/members)

### Categories from the Tools

[https://127.0.0.1/import/old-data/tool-categories](https://127.0.0.1/import/old-data/tool-categories)

### Tools

[https://127.0.0.1/import/old-data/tools](https://127.0.0.1/import/old-data/tools)

### Incoming Alerts

[https://127.0.0.1/import/old-data/alerts](https://127.0.0.1/import/old-data/alerts)

### Operation Reports

[https://127.0.0.1/import/old-data/operations-reports](https://127.0.0.1/import/old-data/operations-reports)

## Wichtige Befehle

### erstellen einer Entity

    php bin/console make:entity

### Datenbank Migration erstellen

    php bin/console make:migration

    # Migration ausführen
    php bin/console doctrine:migrations:migrate

### erstellen eines Testes

    php bin/console make:test

### Php Unittest ausführen

     php ./vendor/bin/phpunit  

### Erstellen von Text Fixtures

    php bin/console make:fixtures

### Fixtures laden in die Datenbank

    php bin/console --env=test  doctrine:fixtures:load 

["ROLE_ADMINISTRATOR", "ROLE_CURLINE", "ROLE_MOTORMAN", "ROLE_GROUP_LEADERS", "ROLE_USER"]
Admin, Wehrleiter., Zugführer, Gruppenführer, Nutzer 