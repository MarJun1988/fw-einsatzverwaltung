# Changelog

## [3.0.7] - 2024.05.128

### Fixed
- Gruppenführer konnten keine Alarmierung erstellen, dies ist jetzt gefixt
- Fehler behoben, bei der Prüfung ob der Nutzer unterrollen besitzt
- Standard Route bei dem Menü Auswertung auf Feedback umgestellt

## [3.0.6] - 2024.05.15

### Added
- Anzeige der aktuellen Version mit zugehöriger Build-Nummer im Hauptmenü.

## [3.0.5] - 2024.05.10

### Fixed
- Behebung im Einsatzbericht: Das Problem mit der ausgewählten Einsatzkategorie wurde behoben, sodass die Auswahl nun korrekt angezeigt wird. 
- Ausblenden von Kategorien: Kategorien, die die Eigenschaften nicht als "Sichtbar" haben, werden nun entsprechend ausgeblendet, um die Benutzererfahrung zu verbessern.

## [3.0.4] - 2024.05.06

### Fixed
- Fehler beim Laden eines zwischengespeicherten Einsatzberichts (Beginn und Ende des Einsatzes): Das Problem mit dem Laden der zwischengespeicherten Einsatzberichtsdaten, insbesondere dem Beginn und dem Ende des Einsatzes, wurde behoben. Die Daten werden nun korrekt angezeigt.
- Fehler beim Hochstellen der Einsatzmittel (ist immer bei 1x geblieben): Ein Fehler, der dazu führte, dass die Anzahl der hochgestellten Einsatzmittel immer bei 1x blieb, wurde behoben. Die Anzahl wird nun entsprechend aktualisiert.
- Behebung der Sichtbarkeit von Mitgliedern im Einsatzbericht (komplettes Löschen ist noch nicht vorgesehen, wegen älterer Einsatzberichte, in denen diese Kameraden teilgenommen haben): Die Sichtbarkeit von Mitgliedern im Einsatzbericht wurde korrigiert. Das komplette Löschen von Mitgliedern ist noch nicht möglich, da ältere Einsatzberichte betroffen sein könnten, in denen diese Kameraden teilgenommen haben. Es wurde jedoch sichergestellt, dass die Sichtbarkeitseinstellungen korrekt angewendet werden.
- Speichern der persönlichen Tabelleneinstellungen (letzte Suche, Auswahl Datensatz, sichtbare Spalten, usw.): Die persönlichen Tabelleneinstellungen, einschließlich der letzten Suche, der Datensatzauswahl und der sichtbaren Spalten, werden nun gespeichert. Dadurch bleibt die Benutzeroberfläche konsistent und erleichtert die Navigation für Benutzer.

## [3.0.3] - 2024.05.03

### Fixed
- Das Problem mit der Anzeige der Anzahl der Hilfsmitteländerungen im Einsatzbericht wurde behoben
- Vorher wurden alle Mitglieder angezeigt, die das Attribut "Sichtbarkeit" im Einsatzbericht hatten

## [3.0.2] - 2024.05.01

### Changed
- Der Exportordner wurde im falschen Format erstellt, was zu Inkonsistenzen führte.

## [3.0.1] - 2024.04.30

### Added
- Feedback-Funktion hinzugefügt, um direkt vom Nutzer Feedback über die Web-UI zu erhalten,
- Benutzer müssen sich vor der Nutzung der Funktion anmelden, es gibt einen gelben Knopf am linken Rand der Benutzeroberfläche ermöglicht den Zugriff auf die Feedback-Funktion.

### Changed
- Verbesserung der Benutzeroberfläche (UI).
- Verbesserung der Benutzererfahrung (User Experience).
- Geschwindigkeit verbessert.

## [3.0] - 2024.04.30

### Changed
- Umstellung auf Symfony 6.x und Vue.js Setup API.
- Verbesserung der Benutzeroberfläche (UI).
- Verbesserung der Benutzererfahrung (User Experience).
- Geschwindigkeit verbessert.

## [2.0] - 2023 

### Changed
- Die ganze Web-Applikation wurde überarbeitet und neue, moderne Technologien wurden benutzt.

## [1.7]

### Changed
- Eintragen eines neuen Einsatzes (FME) wurde komfortabler gestaltet.
- Das UI/UX wurde für eine bessere Handhabung angepasst.

## [1.6]

### Added
- Möglichkeit, die Farbe des Einsatzberichtes einzustellen.
- Verbesserung bei der Verwendung von mehreren Nutzern (Multiuser).

## [1.5]

### Changed
- Verbesserung des Einsatzberichtes.
- Die Exportierte .pdf hat eine erfrischende und aufgeräumte Ansicht bekommen.
- Verbesserungen bei der Anzeige von Einsatzdetails.
- Optimierung der Ansicht auf mobilen Geräten (Tablet).

## [1.4]

### Changed
- Optimierung des Zusammenspiels von mehreren Feuerwehren im Frontend und Backend.

## [1.3]

### Added
- Möglichkeit, den Namen der Feuerwehr in der Datenbank zu speichern (für die Erstellung des .pdf Einsatzberichtes).

## [1.2]

### Changed
- Verschiedene Änderungen im Backend und einige Verbesserungen im Frontend.
- Abschalten des Speicherknopfes bzw. des Erstellen-Knopfes nach erfolgreichem Abschicken eines Formulars.
- Anpassung der Darstellung für Tablets.

## [1.1]

### Changed
- Viele Fehler wurden behoben und diverse Änderungen mit Layout vorgenommen.
- Bei der Erstellung von Einsatzberichten wird nun eine Prüfung der Personenanzahl in Fahrzeugen vorgenommen.
- Die Willkommensseite wurde komplett verändert.

## [1.0]

### Added
- Erste Entwicklung der Einsatzverwaltung für Feuerwehren, noch mit vielen Fehlern und funktionsarm.