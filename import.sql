select `tEV`.`EinsaVerarID`                AS `EinsaVerarID`,
       `tEV`.`EinsaVerarBemerkung`         AS `EinsaVerarBemerkung`,
       `tEV`.`EinsaVerarEintragDatum`      AS `EinsaVerarEintragDatum`,
       `tEEOK`.`EinsaOrginKopieTimeStamp`  AS `EinsaOrginKopieTimeStamp`,
       `tEEOK`.`EinsaOrginKopieMessage`    AS `EinsaOrginKopieMessage`,
       `tEEOK`.`EinsaOrginKopieAddress`    AS `EinsaOrginKopieAddress`,
       `tEA`.`EinsaArtNameKurz`            AS `EinsaArtNameKurz`,
       `tEA`.`EinsaArtNameLang`            AS `EinsaArtNameLang`,
       `tEE`.`EinsaEingaDatumAnfang`       AS `EinsaEingaDatumAnfang`,
       `tEE`.`EinsaEingaDatumEnde`         AS `EinsaEingaDatumEnde`,
       `tEE`.`EinsaEingaEintrag`           AS `EinsaEingaEintrag`,
       `tEE`.`EinsaEingaNummerFw`          AS `EinsaEingaNummerFw`,
       `tEE`.`EinsaEingaNummerLst`         AS `EinsaEingaNummerLst`,
       `tEE`.`EinsaEingaNummerPol`         AS `EinsaEingaNummerPol`,
       `tEE`.`EinsaEingaOrginalRefID`      AS `EinsaEingaOrginalRefID`,
       `tEE`.`EinsaEingaStichwortKurz`     AS `EinsaEingaStichwortKurz`,
       `tEE`.`EinsaEingaStichwortLang`     AS `EinsaEingaStichwortLang`,
       `tEE`.`EinsaEingaSonstigeInfos`     AS `EinsaEingaSonstigeInfos`,
       `tO2`.`OrtName`                     AS `OrtName`,
       `tO2`.`OrtPostleitzahl`             AS `OrtPostleitzahl`,
       `tEE`.`EinsaEingaStrasseNr`         AS `EinsaEingaStrasseNr`,
       `tEE`.`EinsaEingaText`              AS `EinsaEingaText`,
       `tEE`.`EinsaEingaZusatz`            AS `EinsaEingaZusatz`,
       `tEE`.`EinsaEingaBemerkung`         AS `EinsaEingaBemerkung`,
       `tF`.`FahrzNameKurz`                AS `FahrzNameKurz`,
       `tF`.`FahrzNameLang`                AS `FahrzNameLang`,
       `tF`.`FahrzNameAnzeige`             AS `FahrzNameAnzeige`,
       `tF`.`FahrzBesatzung`               AS `FahrzBesatzung`,
       `tEF`.`EinsaFahrzKameradenRefID`    AS `EinsaFahrzKameradenRefID`,
       `tEF`.`EinsaFahrzFahrzeugRefID`     AS `EinsaFahrzFahrzeugRefID`,
       `tK`.`KamerID`                      AS `KamerID`,
       `tK`.`KamerNameAnzeige`             AS `KamerNameAnzeige`,
       `tK`.`KamerNameNach`                AS `KamerNameNach`,
       `tK`.`KamerNameVor`                 AS `KamerNameVor`,
       `tKF`.`KamerFunktID`                AS `KamerFunktID`,
       `tKF`.`KamerFunktNameAnzeige`       AS `KamerFunktNameAnzeige`,
       `tKF`.`KamerFunktNameLang`          AS `KamerFunktNameLang`,
       `tKF`.`KamerFunktSortierung`        AS `KamerFunktSortierung`,
       `tH`.`HilfsName`                    AS `HilfsName`,
       `tEH`.`EinsaHilfsAnzahl`            AS `EinsaHilfsAnzahl`,
       `tHK`.`HilfsKategName`              AS `HilfsKategName`,
       `tEAF`.`EinsaAndereFwNamen`         AS `EinsaAndereFwNamen`,
       `tEAF`.`EinsaAndereFwKameraden`     AS `EinsaAndereFwKameraden`,
       `tEAF`.`EinsaAndereFwEinsatzmittel` AS `EinsaAndereFwEinsatzmittel`
from (((((((((((((`EinsatzVerwaltungNdw`.`tblEinsatzVerarbeitung` `tEV` left join `EinsatzVerwaltungNdw`.`tblEinsatzEingang` `tEE`
                  on ((`tEE`.`EinsaEingaID` = `tEV`.`EinsaVerarEingaRefID`))) left join `EinsatzVerwaltungNdw`.`tblOrtschaften` `tO2`
                 on ((`tEE`.`EinsaEingaOrtRefID` = `tO2`.`OrtID`))) left join `EinsatzVerwaltungGrund`.`tblEinsatzEingangOrginalKopie` `tEEOK`
                on ((`tEE`.`EinsaEingaOrginalRefID` = `tEEOK`.`EinsaOrginKopieID`))) left join `EinsatzVerwaltungNdw`.`tblEinsatzFahrzeuge` `tEF`
               on ((`tEF`.`EinsaFahrzVerarRefID` = `tEV`.`EinsaVerarFahrzRefID`))) left join `EinsatzVerwaltungNdw`.`tblFahrzeuge` `tF`
              on ((`tEF`.`EinsaFahrzFahrzeugRefID` = `tF`.`FahrzID`))) left join `EinsatzVerwaltungNdw`.`tblEinsatzPersonal` `tEP`
             on ((`tEF`.`EinsaFahrzKameradenRefID` = `tEP`.`EinsaPersoID`))) left join `EinsatzVerwaltungNdw`.`tblKameraden` `tK`
            on ((`tEP`.`EinsaPersoKameradenRefID` = `tK`.`KamerID`))) left join `EinsatzVerwaltungNdw`.`tblKameradenFunktionen` `tKF`
           on ((`tEP`.`EinsaPersoFunktionRefID` = `tKF`.`KamerFunktID`))) left join `EinsatzVerwaltungNdw`.`tblEinsatzArten` `tEA`
          on ((`tEV`.`EinsaVerarKategRefID` = `tEA`.`EinsaArtID`))) left join `EinsatzVerwaltungNdw`.`tblEinsatzHilfmittel` `tEH`
         on ((`tEV`.`EinsaVerarHilfsRefID` = `tEH`.`EinsaHilfsVerarRefID`))) left join `EinsatzVerwaltungNdw`.`tblHilfsmittel` `tH`
        on ((`tEH`.`EinsaHilfsHilfsRefID` = `tH`.`HilfsID`))) left join `EinsatzVerwaltungNdw`.`tblHilfsmittelKategorien` `tHK`
       on ((`tHK`.`HilfsKategID` = `tH`.`HilfsKategRefID`))) left join `EinsatzVerwaltungNdw`.`tblEinsatzAndereFw` `tEAF`
      on ((`tEV`.`EinsaVerarAndereFwRefID` = `tEAF`.`EinsaAndereFwVerarRefID`)))
where (isnull(`tEE`.`EinsaEingaGeloescht`) and (`tEV`.`EinsaVerarEingaRefID` is not null) and
       (`tEV`.`EinsaVerarFahrzRefID` is not null) and (`tEV`.`EinsaVerarKategRefID` is not null) and
       (`tEV`.`EinsaVerarHilfsRefID` is not null) and (`tEV`.`EinsaVerarAndereFwRefID` is not null) and
       (`tEEOK`.`EinsaOrginKopieMessage` is not null) and (`tEEOK`.`EinsaOrginKopieTimeStamp` is not null))





DROP VIEW IF EXISTS `abfImportOldData`;


create view `abfImportOldData`
as
select
    # tblEinsatzVerarbeitung
    tEV.EinsaVerarID,
    tEV.EinsaVerarBemerkung,
    tEV.EinsaVerarEintragDatum,

    # tblEinsatzEingangOrginalKopie
    tEEOK.EinsaOrginKopieTimeStamp,
    tEEOK.EinsaOrginKopieMessage,
    tEEOK.EinsaOrginKopieAddress,

    # tblEinsatzArten
    tEA.EinsaArtNameKurz,
    tEA.EinsaArtNameLang,

    # tblEinsatzEingang
    tEE.EinsaEingaDatumAnfang,
    tEE.EinsaEingaDatumEnde,

    tEE.EinsaEingaEintrag,
    tEE.EinsaEingaNummerFw,
    tEE.EinsaEingaNummerLst,
    tEE.EinsaEingaNummerPol,
    tEE.EinsaEingaOrginalRefID,
    tEE.EinsaEingaStichwortKurz,
    tEE.EinsaEingaStichwortLang,
    tEE.EinsaEingaSonstigeInfos,

    # Ort und Postleitzahl, Straße
    tO2.OrtName,
    tO2.OrtPostleitzahl,
    tEE.EinsaEingaStrasseNr,

    tEE.EinsaEingaText,
    tEE.EinsaEingaZusatz,
    tEE.EinsaEingaBemerkung,

    # tblEinsatzFahrzeuge (tblFahrzeuge)
    tF.FahrzNameKurz,
    tF.FahrzNameLang,
    tF.FahrzNameAnzeige,
    tF.FahrzBesatzung,

    # tblEinsatzFahrzeuge
    tEF.EinsaFahrzKameradenRefID,
    tEF.EinsaFahrzFahrzeugRefID,

    # (tblKameraden)
    tK.KamerID,
    tK.KamerNameAnzeige,
    tK.KamerNameNach,
    tK.KamerNameVor,

    # (tblKameradenFunktionen)
    tKF.KamerFunktID,
    tKF.KamerFunktNameAnzeige,
    tKF.KamerFunktNameLang,
    tKF.KamerFunktSortierung,

    # tblEinsatzHilfmittel (tblHilfsmittel)
    tH.HilfsName,
    tEH.EinsaHilfsAnzahl,

    # (tblHilfsmittelKategorien)
    tHK.HilfsKategName,

    # tblEinsatzAndereFw
    tEAF.EinsaAndereFwNamen,
    tEAF.EinsaAndereFwKameraden,
    tEAF.EinsaAndereFwEinsatzmittel

from tblEinsatzVerarbeitung tEV
         left join tblEinsatzEingang tEE on tEE.EinsaEingaID = tEV.EinsaVerarEingaRefID

         left join tblOrtschaften tO2 on tEE.EinsaEingaOrtRefID = tO2.OrtID

         left join EinsatzVerwaltungGrund.tblEinsatzEingangOrginalKopie tEEOK on tEE.EinsaEingaOrginalRefID =  tEEOK.EinsaOrginKopieID

         left join tblEinsatzFahrzeuge tEF on tEF.EinsaFahrzVerarRefID = tEV.EinsaVerarFahrzRefID
         left join tblFahrzeuge tF on tEF.EinsaFahrzFahrzeugRefID = tF.FahrzID
         left join tblEinsatzPersonal tEP on tEF.EinsaFahrzKameradenRefID = tEP.EinsaPersoID
         left join tblKameraden tK on tEP.EinsaPersoKameradenRefID = tK.KamerID
         left join tblKameradenFunktionen tKF on tEP.EinsaPersoFunktionRefID = tKF.KamerFunktID

         left join tblEinsatzArten tEA on tEV.EinsaVerarKategRefID = tEA.EinsaArtID

         left join tblEinsatzHilfmittel tEH on tEV.EinsaVerarHilfsRefID = tEH.EinsaHilfsVerarRefID
         left join tblHilfsmittel tH on tEH.EinsaHilfsHilfsRefID = tH.HilfsID
         left join tblHilfsmittelKategorien tHK on tHK.HilfsKategID = tH.HilfsKategRefID

         left join tblEinsatzAndereFw tEAF on tEV.EinsaVerarAndereFwRefID = tEAF.EinsaAndereFwVerarRefID

where tEE.EinsaEingaGeloescht is null;

SET FOREIGN_KEY_CHECKS = 0;

delete from operation_reports where id > 0;
delete from operation_report_incomings where id > 0;
# delete from operation_incoming_alerts;

truncate operation_reports;
truncate operation_report_incomings;
# truncate operation_incoming_alerts;



DROP VIEW IF EXISTS `abfImportOldData`;


create view `abfImportOldData`
as
select `tEV`.`EinsaVerarID`                AS `EinsaVerarID`,
       `tEV`.`EinsaVerarBemerkung`         AS `EinsaVerarBemerkung`,
       `tEV`.`EinsaVerarEintragDatum`         AS `EinsaVerarEintragDatum`,
       `tEEOK`.`EinsaOrginKopieTimeStamp`  AS `EinsaOrginKopieTimeStamp`,
       `tEEOK`.`EinsaOrginKopieMessage`    AS `EinsaOrginKopieMessage`,
       `tEEOK`.`EinsaOrginKopieAddress`    AS `EinsaOrginKopieAddress`,
       `tEA`.`EinsaArtNameKurz`            AS `EinsaArtNameKurz`,
       `tEA`.`EinsaArtNameLang`            AS `EinsaArtNameLang`,
       `tEE`.`EinsaEingaDatumAnfang`       AS `EinsaEingaDatumAnfang`,
       `tEE`.`EinsaEingaDatumEnde`         AS `EinsaEingaDatumEnde`,
       `tEE`.`EinsaEingaEintrag`           AS `EinsaEingaEintrag`,
       `tEE`.`EinsaEingaNummerFw`          AS `EinsaEingaNummerFw`,
       `tEE`.`EinsaEingaNummerLst`         AS `EinsaEingaNummerLst`,
       `tEE`.`EinsaEingaNummerPol`         AS `EinsaEingaNummerPol`,
       `tEE`.`EinsaEingaOrginalRefID`      AS `EinsaEingaOrginalRefID`,
       `tEE`.`EinsaEingaStichwortKurz`     AS `EinsaEingaStichwortKurz`,
       `tEE`.`EinsaEingaStichwortLang`     AS `EinsaEingaStichwortLang`,
       `tEE`.`EinsaEingaSonstigeInfos`     AS `EinsaEingaSonstigeInfos`,
       `tO2`.`OrtName`                     AS `OrtName`,
       `tO2`.`OrtPostleitzahl`             AS `OrtPostleitzahl`,
       `tEE`.`EinsaEingaStrasseNr`         AS `EinsaEingaStrasseNr`,
       `tEE`.`EinsaEingaText`              AS `EinsaEingaText`,
       `tEE`.`EinsaEingaZusatz`            AS `EinsaEingaZusatz`,
       `tEE`.`EinsaEingaBemerkung`         AS `EinsaEingaBemerkung`,
       `tF`.`FahrzNameKurz`                AS `FahrzNameKurz`,
       `tF`.`FahrzNameLang`                AS `FahrzNameLang`,
       `tF`.`FahrzNameAnzeige`             AS `FahrzNameAnzeige`,
       `tF`.`FahrzBesatzung`               AS `FahrzBesatzung`,
       `tEF`.`EinsaFahrzKameradenRefID`    AS `EinsaFahrzKameradenRefID`,
       `tEF`.`EinsaFahrzFahrzeugRefID`     AS `EinsaFahrzFahrzeugRefID`,
       `tK`.`KamerID`                      AS `KamerID`,
       `tK`.`KamerNameAnzeige`             AS `KamerNameAnzeige`,
       `tK`.`KamerNameNach`                AS `KamerNameNach`,
       `tK`.`KamerNameVor`                 AS `KamerNameVor`,
       `tKF`.`KamerFunktID`                AS `KamerFunktID`,
       `tKF`.`KamerFunktNameAnzeige`       AS `KamerFunktNameAnzeige`,
       `tKF`.`KamerFunktNameLang`          AS `KamerFunktNameLang`,
       `tKF`.`KamerFunktSortierung`        AS `KamerFunktSortierung`,
       `tH`.`HilfsName`                    AS `HilfsName`,
       `tEH`.`EinsaHilfsAnzahl`            AS `EinsaHilfsAnzahl`,
       `tHK`.`HilfsKategName`              AS `HilfsKategName`,
       `tEAF`.`EinsaAndereFwNamen`         AS `EinsaAndereFwNamen`,
       `tEAF`.`EinsaAndereFwKameraden`     AS `EinsaAndereFwKameraden`,
       `tEAF`.`EinsaAndereFwEinsatzmittel` AS `EinsaAndereFwEinsatzmittel`
from (((((((((((((`EinsatzVerwaltungNdw`.`tblEinsatzVerarbeitung` `tEV` left join `EinsatzVerwaltungNdw`.`tblEinsatzEingang` `tEE`
                  on ((`tEE`.`EinsaEingaID` = `tEV`.`EinsaVerarEingaRefID`))) left join `EinsatzVerwaltungNdw`.`tblOrtschaften` `tO2`
                 on ((`tEE`.`EinsaEingaOrtRefID` = `tO2`.`OrtID`))) left join `EinsatzVerwaltungGrund`.`tblEinsatzEingangOrginalKopie` `tEEOK`
                on ((`tEE`.`EinsaEingaOrginalRefID` = `tEEOK`.`EinsaOrginKopieID`))) left join `EinsatzVerwaltungNdw`.`tblEinsatzFahrzeuge` `tEF`
               on ((`tEF`.`EinsaFahrzVerarRefID` = `tEV`.`EinsaVerarFahrzRefID`))) left join `EinsatzVerwaltungNdw`.`tblFahrzeuge` `tF`
              on ((`tEF`.`EinsaFahrzFahrzeugRefID` = `tF`.`FahrzID`))) left join `EinsatzVerwaltungNdw`.`tblEinsatzPersonal` `tEP`
             on ((`tEF`.`EinsaFahrzKameradenRefID` = `tEP`.`EinsaPersoID`))) left join `EinsatzVerwaltungNdw`.`tblKameraden` `tK`
            on ((`tEP`.`EinsaPersoKameradenRefID` = `tK`.`KamerID`))) left join `EinsatzVerwaltungNdw`.`tblKameradenFunktionen` `tKF`
           on ((`tEP`.`EinsaPersoFunktionRefID` = `tKF`.`KamerFunktID`))) left join `EinsatzVerwaltungNdw`.`tblEinsatzArten` `tEA`
          on ((`tEV`.`EinsaVerarKategRefID` = `tEA`.`EinsaArtID`))) left join `EinsatzVerwaltungNdw`.`tblEinsatzHilfmittel` `tEH`
         on ((`tEV`.`EinsaVerarHilfsRefID` = `tEH`.`EinsaHilfsVerarRefID`))) left join `EinsatzVerwaltungNdw`.`tblHilfsmittel` `tH`
        on ((`tEH`.`EinsaHilfsHilfsRefID` = `tH`.`HilfsID`))) left join `EinsatzVerwaltungNdw`.`tblHilfsmittelKategorien` `tHK`
       on ((`tHK`.`HilfsKategID` = `tH`.`HilfsKategRefID`))) left join `EinsatzVerwaltungNdw`.`tblEinsatzAndereFw` `tEAF`
      on ((`tEV`.`EinsaVerarAndereFwRefID` = `tEAF`.`EinsaAndereFwVerarRefID`)))
where (isnull(`tEE`.`EinsaEingaGeloescht`) and (`tEV`.`EinsaVerarEingaRefID` is not null) and
       (`tEV`.`EinsaVerarFahrzRefID` is not null) and (`tEV`.`EinsaVerarKategRefID` is not null) and
       (`tEV`.`EinsaVerarHilfsRefID` is not null) and (`tEV`.`EinsaVerarAndereFwRefID` is not null) and
       (`tEEOK`.`EinsaOrginKopieMessage` is not null) and (`tEEOK`.`EinsaOrginKopieTimeStamp` is not null))


# select
#     # tblEinsatzVerarbeitung
#     tEV.EinsaVerarID,
#     tEV.EinsaVerarBemerkung,
#     tEV.EinsaVerarEintragDatum,
#
#     # tblEinsatzEingangOrginalKopie
#     tEEOK.EinsaOrginKopieTimeStamp,
#     tEEOK.EinsaOrginKopieMessage,
#     tEEOK.EinsaOrginKopieAddress,
#
#     # tblEinsatzArten
#     tEA.EinsaArtNameKurz,
#     tEA.EinsaArtNameLang,
#
#     # tblEinsatzEingang
#     tEE.EinsaEingaDatumAnfang,
#     tEE.EinsaEingaDatumEnde,
#
#     tEE.EinsaEingaEintrag,
#     tEE.EinsaEingaNummerFw,
#     tEE.EinsaEingaNummerLst,
#     tEE.EinsaEingaNummerPol,
#     tEE.EinsaEingaOrginalRefID,
#     tEE.EinsaEingaStichwortKurz,
#     tEE.EinsaEingaStichwortLang,
#     tEE.EinsaEingaSonstigeInfos,
#
#     # Ort und Postleitzahl, Straße
#     tO2.OrtName,
#     tO2.OrtPostleitzahl,
#     tEE.EinsaEingaStrasseNr,
#
#     tEE.EinsaEingaText,
#     tEE.EinsaEingaZusatz,
#     tEE.EinsaEingaBemerkung,
#
#     # tblEinsatzFahrzeuge (tblFahrzeuge)
#     tF.FahrzNameKurz,
#     tF.FahrzNameLang,
#     tF.FahrzNameAnzeige,
#     tF.FahrzBesatzung,
#
#     # tblEinsatzFahrzeuge
#     tEF.EinsaFahrzKameradenRefID,
#     tEF.EinsaFahrzFahrzeugRefID,
#
#     # (tblKameraden)
#     tK.KamerID,
#     tK.KamerNameAnzeige,
#     tK.KamerNameNach,
#     tK.KamerNameVor,
#
#     # (tblKameradenFunktionen)
#     tKF.KamerFunktID,
#     tKF.KamerFunktNameAnzeige,
#     tKF.KamerFunktNameLang,
#     tKF.KamerFunktSortierung,
#
#     # tblEinsatzHilfmittel (tblHilfsmittel)
#     tH.HilfsName,
#     tEH.EinsaHilfsAnzahl,
#
#     # (tblHilfsmittelKategorien)
#     tHK.HilfsKategName,
#
#     # tblEinsatzAndereFw
#     tEAF.EinsaAndereFwNamen,
#     tEAF.EinsaAndereFwKameraden,
#     tEAF.EinsaAndereFwEinsatzmittel
#
# from tblEinsatzVerarbeitung tEV
#          left join tblEinsatzEingang tEE on tEE.EinsaEingaID = tEV.EinsaVerarEingaRefID
#
#          left join tblOrtschaften tO2 on tEE.EinsaEingaOrtRefID = tO2.OrtID
#
#          left join EinsatzVerwaltungGrund.tblEinsatzEingangOrginalKopie tEEOK on tEE.EinsaEingaOrginalRefID =  tEEOK.EinsaOrginKopieID
#
#          left join tblEinsatzFahrzeuge tEF on tEF.EinsaFahrzVerarRefID = tEV.EinsaVerarFahrzRefID
#          left join tblFahrzeuge tF on tEF.EinsaFahrzFahrzeugRefID = tF.FahrzID
#          left join tblEinsatzPersonal tEP on tEF.EinsaFahrzKameradenRefID = tEP.EinsaPersoID
#          left join tblKameraden tK on tEP.EinsaPersoKameradenRefID = tK.KamerID
#          left join tblKameradenFunktionen tKF on tEP.EinsaPersoFunktionRefID = tKF.KamerFunktID
#
#          left join tblEinsatzArten tEA on tEV.EinsaVerarKategRefID = tEA.EinsaArtID
#
#          left join tblEinsatzHilfmittel tEH on tEV.EinsaVerarHilfsRefID = tEH.EinsaHilfsVerarRefID
#          left join tblHilfsmittel tH on tEH.EinsaHilfsHilfsRefID = tH.HilfsID
#          left join tblHilfsmittelKategorien tHK on tHK.HilfsKategID = tH.HilfsKategRefID
#
#          left join tblEinsatzAndereFw tEAF on tEV.EinsaVerarAndereFwRefID = tEAF.EinsaAndereFwVerarRefID
#
# where tEE.EinsaEingaGeloescht is null and EinsaVerarID = 2
